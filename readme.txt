NRL Imports Plugin Readme
=========================

This plugins imports MLS listings for use with Nesbitt Real Estate Listings. The Imports can be set up in a different WP install
on the same server as the site(s) running NRL Listings.

When you activate the Nesbitt Imports plugin for the first time, it will attempt to import settings from the old
version of the plugin, if it exists on the site.

Then you need to visit the NRL Imports / Setup tab to complete installation. Complete the steps in order, starting from the top. 
Steps include:
* Create necessary tables (do this first)
* Retrieve list of data fields from MRIS
* Retrieve Lookup field codes and values from MRIS
* Import plugin settings loaded several csv located in the setup/ diretory
  (These files are included in the repository)

If there are any errors during installation, you can check the log in logs/log-install.txt in the plugin directory.

Use the WordPress Importer to import the Article Templates from the .xml file found in the plugin setup directory.
You will need these to use the Post Generator in the NRL Listings plugin.

Then you will need to import some listings so that you have some data to work with. You can run that manually several times
by clicking the Import Listings button on the Tools tab of the NRL Listings admin menu.
Or if you can set up a local cron job, run this command: wget -O /dev/null http://nesbittrealty.com/index.php?nrl_cron_job=1
(modify the url to point to the WordPress directory on your local test server)
You do not need to continually run the cron job and get a complete set of listings. You just need enough for some testing.

