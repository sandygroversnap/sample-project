<?php
/*
  Plugin Name: NRL Imports
  Description: Imports MRIS listing data and sets it up in the database
  Version: 1.5.1
  Author: Ken Carlson
  Author URI: http://kencarlsonconsulting.com
 */

//do not allow direct access
if ( strpos(strtolower($_SERVER['SCRIPT_NAME']), strtolower(basename(__FILE__))) ) {
    header('HTTP/1.0 403 Forbidden');
    exit('Forbidden');
}

/* * ******************
 * Global constants
 * ****************** */

define('NIMP_VERSION', '1.5.1');
define('NIMP_BUILD', '27');  // Used to force load of latest .js files
define('NIMP_TABLE_VERSION', '5'); // Version of nrl custom tables
define('NIMP_FILE', __FILE__); // For use in other files
define('NIMP_PATH', plugin_dir_path(__FILE__));
define('NIMP_URL', plugin_dir_url(__FILE__));
define('NIMP_PLUGIN_NAME', 'Nesbitt Imports');
define('NIMP_TEXT_DOMAIN','nrl_plugin');


// Image server host and path (used for rewriting image urls)
define('BRIGHT_IMAGE_HOST','http://bright-media.brightmls.com/bright/images/');


// Constants used by Common functions in both plugins
// These must appear in both plugins, and MUST BE THE SAME
if ( ! defined('NRL_CONTENT_DIR') ) {
    define('NRL_CONTENT_DIR','NRL');        // Directory under wp-content for photos
}
if ( ! defined('NRL_IMAGE_HOST') ) {
    define('NRL_IMAGE_HOST', 'http://photos.nesbittrealty.com/');
}

define('NIMP_POLYGON_FILE', NIMP_PATH . 'setup/polygons.txt');

// Names for WP options for this plugin
define('NIMP_OPTION_NAME', 'nimp_options');     // Name of the options for the imports plugin

define('NIMP_LISTINGS_OPTION_NAME', 'nrl_listings'); // Used to store list of MRIS ListingKey values to process

define('NIMP_MAINT_OPTION_NAME', 'nrl_maintenance'); // Used to store polygon areas defined in the admin area
define('NIMP_HISTORY_OPTION_NAME', 'nrl_history_listings'); // Options used to store IDs of history records (only on the back end)

define('NIMP_SHORTCODE_OPTION_NAME', 'nrl_shortcode_fields');     // Used to store shortcode fields

// No longer needed
//define('NIMP_OPTION_LOOKUP', 'nrl_lookups');     // Lookup table for MRIS database Lookup cols
//define('NIMP_OPTIONS_NAME', 'nrl_options');     // Name of the main options table entry (used on front and back end)
//define('NIMP_DATABASE_OPTION_NAME', 'nrl_database'); // Used to store options related to database maintenance on the main site

define('NIMP_MLS_OPTIONS_NAME', 'nrl_mls_options'); // Options used for MLS import (only on the back end)
// The following are used for .csv files imported for setup
define('NIMP_FIELDS_OPTION_NAME', 'nrl_field_settings');     // Used to store field display options
define('NIMP_SEARCH_OPTION_NAME', 'nrl_search_fields');     // Used to store search fields
define('NIMP_PROP_TYPE_OPTION_NAME', 'nrl_prop_type_text');     // Used to store prop type display text

define('NIMP_POLYGON_OPTION_NAME', 'nrl_polygons'); // Used to store polygon areas defined in the admin area

// From old version of the plugin...

define('NIMP_OUR_AGENCY','Nesbitt');

// Logs
define('NIMP_INSTALL_LOG', NIMP_PATH . 'logs/log-install.txt');
define('NIMP_IMPORT_LOG', NIMP_PATH . 'logs/log-import');
define('NIMP_IMPORT_SUMMARY_LOG', NIMP_PATH . 'logs/log-import-summary');
define('NIMP_MAINT_LOG', NIMP_PATH . 'logs/log-maintenance.txt');
define('NIMP_CUSTOM_LOG', NIMP_PATH . 'logs/log-custom.txt');
define('NIMP_ERROR_LOG', NIMP_PATH . 'logs/log-error.txt');
define('NIMP_DEBUG_LOG', NIMP_PATH . 'logs/log-debug.txt');

// Turn on some debug messages?
define( 'NIMP_DEBUG', true );

// Set the time limit for imports and maintenance tasks run by the cron job
// If necessary, the PHP max execution time will be increased in NIMP_MLS_Util::run_cron()
$time_limit = ini_get('max_execution_time');
if ( ! ini_get('safe_mode') && $time_limit < 90 ) {
    // If PHP is in safe mode, we can't use ini_set to change max_execution time
    $time_limit = 90;
}
define('NIMP_IMPORT_TIME_LIMIT', $time_limit-30);

// Set Dev site and Dev user constants
define('NIMP_DEV_USER_ID', 2);
define('NIMP_DEV_USER_ID2', 2640);
if ( strpos(NIMP_URL, 'dev.') !== false || strpos(NIMP_URL, 'localhost') !== false ) {
    define('NIMP_DEV_SITE', true);
} else {
    define('NIMP_DEV_SITE', false);
}

/* * ******************
 * Includes
 * ****************** */
require_once NIMP_PATH . 'library/class-common.php';
require_once NIMP_PATH . 'library/class-nimp-util.php';
require_once NIMP_PATH . 'library/class-nimp-admin.php';

require_once NIMP_PATH . 'library/class-MLS.php';
require_once NIMP_PATH . 'library/class-MLS-util.php';

// The following are needed only for admin, not for imports
if ( is_admin() ) {
    require_once NIMP_PATH . 'library/class-custom-posts.php';      // Required to edit Article Templates
    require_once NIMP_PATH . 'library/class-fields.php';
}

//require_once NIMP_PATH . 'library/class-bright-mls.php';

// Initialize plugin settings and hooks
NIMP_Util::setup();
