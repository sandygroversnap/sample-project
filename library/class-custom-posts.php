<?php
/**
 * Nesbitt Imports Plugin
 * Needed by Nesbitt Real Estate Listings *if* display site needs to create custom Article Templates
 *
 * Functions used to create Article Templates for the Post Generator
 * (The actual post generation takes place in the nrl-listings plugin on the main site
 *
 * @author Ken Carlson <kencarlsonconsulting.com>
 */

if ( ! class_exists('NRL_Custom_Posts') ) {

    add_action( 'admin_init', 'NRL_Custom_Posts::init_meta_box' );

    class NRL_Custom_Posts {
    //    static $options;
        static $listing, $listing_url, $listing_type, $post_meta;

        static function init_meta_box() {
            // Add meta box to the custom post type edit page
            // Remove unnessesary meta boxes:
    //        remove_meta_box('linktargetdiv', 'link', 'normal');
    //        remove_meta_box('linkxfndiv', 'link', 'normal');
    //        remove_meta_box('linkadvanceddiv', 'link', 'normal');
    //        remove_meta_box('postexcerpt', 'post', 'normal');
    //        remove_meta_box('trackbacksdiv', 'post', 'normal');
    //        remove_meta_box('postcustom', 'post', 'normal');
    //        remove_meta_box('commentstatusdiv', 'post', 'normal');
    //        remove_meta_box('commentsdiv', 'post', 'normal');
    //        remove_meta_box('revisionsdiv', 'post', 'normal');
    //        remove_meta_box('postdivrich', 'post', 'normal');
    //        remove_meta_box('authordiv', 'post', 'normal');
    //        remove_meta_box('sqpt-meta-tags', 'post', 'normal');
    //        remove_meta_box('slugdiv', 'post', 'normal');

            // Add our own meta box:
            add_meta_box( 'nrl-meta', 'Article Template Data', array('NRL_Custom_Posts','meta_box_display'), 'nrl_articles' );
            add_action( 'save_post', 'NRL_Custom_Posts::meta_box_save' );
            add_meta_box( 'nrl-meta-tags', 'Available Template Tags', array('NRL_Custom_Posts','meta_box_tags_display'), 'nrl_articles', 'side' );
        }

        static function meta_box_display($post) {
            // Displays the meta box for the Article Template edit page

            wp_nonce_field('nrl_meta_box', 'nrl_meta_box_nonce');
            // Get the post meta
    //        $meta_data = get_post_meta($post->ID, 'nrl_meta', true);
            $meta_data = NRL_Common::get_article_meta_data( $post->ID );
            if ( empty($meta_data) ) {
                // Set initial default values
                $meta_data = array(
                    'title' => '',
                    'priority' => '10',
                    'seed'  => 'true',
                    'list_type' => 'sales',
                    'random' => array(),
                );
            }

            $priority = ( ! empty($meta_data['priority']) ? $meta_data['priority'] : '10');
            if ( empty($meta_data['seed']) || $meta_data['seed'] == 'true' ) {
                $seed_checked = ' checked="checked"';
            } else {
                $seed_checked = '';
            }
            $location_type = ( ! empty($meta_data['post_location_type']) ? $meta_data['post_location_type'] : '');
            $select_options = array(
                'County'    => 'County',
                'CityName'  => 'City',
                'Subdivision'   => 'Subdivision',
                'PostalCode'    => 'Zip',
            );
            if ( empty($meta_data['change_location'])) {
                $meta_data['change_location'] = 'true';
            }
            $location_change = ( $meta_data['change_location'] == 'true' ? ' checked="checked"' : '' );
    ?>
    <div id="nrl_meta_box">
        <div>
        <p>Enter the title for generated articles. The name of the template, e.g. "Featured Property" goes in the WordPress title above.</p>
        <label for='nrl_title'>Article Title</label>
        <input type="text" name="nrl_meta[title]" id="nrl_title" size="60" value="<?php echo $meta_data['title']; ?>">
        <br/>
        </div>
    </div>
    <div>
        <p><b>Seed Listing for this article</b></p>
        <label><input type="checkbox" id="nrl-has-seed" name="nrl_meta[seed]" value="true" <?php echo $seed_checked; ?>> This article uses a seed listing</label>
    <p><b>Selection Criteria for Seed Listing:</b></p>
            <div class="nrl_option_item">
            <label for="nrl_list_type">Listing type</label>
            <select id="nrl_list_type" onchange="changePrice(this.value);" name="nrl_meta[list_type]">
                <option value="sales"<?php if ($meta_data['list_type']=='sales') echo " selected='selected'"; ?>>For Sale</option>
                <option value="rentals"<?php if ($meta_data['list_type']=='rentals') echo " selected='selected'"; ?>>Rentals</option>
            </select>
            </div>
            <div class="nrl_option_item">
            <label for="nrl_location_type">Listing location type</label>
            <select id="nrl_location_type"  name="nrl_meta[post_location_type]">
                <option value="">Any</option>
    <?php   foreach ( $select_options as $skey => $name ) {
                echo "<option value='$skey'" . ( $skey == $location_type ? " selected='selected'" : '' ) . ">$name</option>\n";

            }
        ?>
            </select>
            <label for="nrl_location_value"> Name</label>
            <input type="text" id="nrl_location_value" name="nrl_meta[post_location_value]" size="50" value="<?php
               if ( ! empty($meta_data['post_location_value'])) echo $meta_data['post_location_value']; ?>" />
            </div>

            <?php

            $property_type  = NRL_Common::get_prop_types();
            $ownership      = array(
                'Any'               => '',
                'Condo'             => 'Condo',
                'Coop'              => 'Coop',
                'Fee Simple'        => 'Fee Simple',
                'Ground Rent'       => 'Ground Rent',
                'Rental Apartment'  => 'Rental Apartment',
            );
            $garage         = array(
                'Any'   => '',
                '1+'    => '1',
                '2+'    => '2',
                '3+'    => '3',
                '4+'    => '4',
            );
            $style = array(
                'Any' => 'Any',
                'Arts & Crafts' => 'Arts & Crafts',
                'Art Deco' => 'Art Deco',
                'Bungalow' => 'Bungalow',
                'Cape Cod' => 'Cape Cod',
                'Colonial' => 'Colonial',
                'Contemporary' => 'Contemporary',
                'Cottage' => 'Cottage',
                'Craftsman' => 'Craftsman',
                'Farm House' => 'Farm House',
                'Federal' => 'Federal',
                'French Provincial' => 'French Provincial',
                'Georgian' => 'Georgian',
                'International' => 'International',
                'Manor' => 'Manor',
                'Other' => 'Other',
                'Rambler' => 'Rambler',
                'Spanish' => 'Spanish',
                'Split Foyer' => 'Split Foyer',
                'Split Level' => 'Split Level',
                'Traditional' => 'Traditional',
                'Transitional' => 'Transitional',
                'Tudor' => 'Tudor',
                'Victorian' => 'Victorian',
                'Villa' => 'Villa',
            );


            $size_tags      = array(
                'Any'=>'Any',
                '1-600' => 'Tiny',
                '601-700' => 'Smaller',
                '701-1100' => 'Small',
                '1101-1500' => 'Modest',
                '1501-2000' => 'Mid-sized',
                '2001-2500' => 'Large',
                '2501-3000' => 'Big',
                '3001-4000' => 'Very Large',
                '4001-99999999' => 'Sprawling',
            );

            if ( ! empty($meta_data['OwnershipInterest']) && $meta_data['OwnershipInterest'] == 'Fee Simple' ) {
                $price_tags     = array(
                    'Any'=>'Any',
                    '0-300000'  => 'Affordable',
                    '300001-750000'  => 'Mid-Market',
                    '750001-1100000' => 'Premium',
                    '1100001-1500000' => 'Luxury',
                    '1500001-999999999' => 'Super Luxury',
                );
            } else {
                $price_tags     = array(
                    'Any'=>'Any',
                    "0-200000" 				=> "Budget",
                    "200001-300000" 		=> "Affordable",
                    "300001-400000" 		=> "Nice",
                    "400001-500000" 		=> "Premium",
                    "500001-600000" 		=> "Luxury",
                    "600001-99999999999" 	=> "Super Luxury",
                );
            }

            $polygons = get_option( NIMP_POLYGON_OPTION_NAME );
            ?>

            <div class="nrl_option_item">
                <div class="nrl_search_field">
                    <label for="area">Area</label>
                    <select name="nrl_meta[Area]" id="area" class="nrl_seed_field">
                        <option value="Any">Any</option>
                        <?php foreach ($polygons as $key => $value){
                            ?>
                            <option value="<?php echo $key;?>" <?php //if ($meta_data['Area']==$key){echo 'selected';}
                            echo self::show_selected( $meta_data, 'Area', $key ); ?>><?php echo $value['name'];?></option>
                            <?php
                        }?>
                    </select>
                </div>
            </div>
            <div class="nrl_option_item">
                <div class="nrl_search_field">
                    <label for="type">Type</label>
                    <select name="nrl_meta[PropertyType]" id="type" class="nrl_seed_field">
                        <?php foreach ($property_type as $key => $value){
                            ?>
                            <option value="<?php echo $value;?>" <?php
//                               if ( ! empty($meta_data['PropertyType']) && $meta_data['PropertyType']==$value){echo 'selected';}
                               echo self::show_selected( $meta_data, 'PropertyType', $value );?>><?php echo $key;?></option>
                            <?php
                        }?>
                    </select>
                </div>
            </div>
            <div class="nrl_option_item">
                <div class="nrl_search_field">
                    <label for="style">Style</label>
                    <select name="nrl_meta[Style]" id="style" class="nrl_seed_field">
                        <?php foreach ($style as $key => $value){
                            ?>
                            <option value="<?php echo $value;?>" <?php
//                            if ($meta_data['Style']==$value){echo "selected";}
                            echo self::show_selected( $meta_data, 'Style', $value );?>><?php echo $key;?></option>
                            <?php
                        }?>
                    </select>
                </div>
            </div>
            <div class="nrl_option_item">
                <div class="nrl_search_field">
                    <label for="beds_min">Beds(min)</label>
                    <input type="text" name="nrl_meta[Beds_min]" id="beds_min" value="<?php echo self::show_if_not_empty($meta_data, 'Beds_min');  ?>" class="nrl_seed_field">
                </div>
            </div>
            <div class="nrl_option_item">

                <div class="nrl_search_field">
                    <label for="beds_max">Beds(max)</label>
                    <input type="text" name="nrl_meta[Beds_max]" id="beds_max" value="<?php echo self::show_if_not_empty($meta_data, 'Beds_max');?>" class="nrl_seed_field">
                </div>
            </div>
            <div class="nrl_option_item">
                <div class="nrl_search_field">
                    <label for="ownership">Ownership Scheme</label>
                    <select name="nrl_meta[Ownership]" onchange="changePrice(this.value);" id="ownership" class="nrl_seed_field">
                        <?php foreach ($ownership as $key => $value){
                            ?>
                            <option value="<?php echo $value;?>" <?php //if ($meta_data['Ownership']==$value){echo "selected";}
                            echo self::show_selected( $meta_data, 'Ownership', $value ); ?>><?php echo $key;?></option>
                            <?php
                        }?>
                    </select>
                </div>
            </div>
            <div class="nrl_option_item">
                <div class="nrl_search_field">
                    <label for="size">Size</label>
                    <select name="nrl_meta[AboveGradeAreaFinished]" id="size" class="nrl_seed_field">
                        <?php foreach ($size_tags as $key => $value){
                            ?>
                            <option value="<?php echo $key;?>" <?php //if ($meta_data['AboveGradeAreaFinished']==$key){echo "selected";}
                            echo self::show_selected( $meta_data, 'AboveGradeAreaFinished', $key ); ?>><?php echo $value;?></option>
                            <?php
                        }?>
                    </select>
                </div>
            </div>
            <div class="nrl_option_item">
                <div class="nrl_search_field">
                    <label for="price">Price</label>
                    <select name="nrl_meta[ListPrice]" id="price" class="nrl_seed_field" multiple>
                        <?php foreach ($price_tags as $key => $value){
                            ?>
                            <option value="<?php echo $key;?>" <?php //if ($meta_data['ListPrice']==$key){echo "selected";}
                            echo self::show_selected( $meta_data, 'AboveGradeAreaFinished', $key ); ?>><?php echo $value;?></option>
                            <?php
                        }?>
                    </select>
                </div>
            </div>
            <div class="nrl_option_item">
                <div class="nrl_search_field">
                    <label for="garages">Garages</label>
                    <select name="nrl_meta[Garage]" id="garages" class="nrl_seed_field">
                        <?php foreach ($garage as $key => $value){
                            ?>
                            <option value="<?php echo $value;?>" <?php //if($meta_data['Garage']==$value){echo "selected";}
                            echo self::show_selected( $meta_data, 'Garage', $value ); ?>><?php echo $key;?></option>
                            <?php
                        }?>
                    </select>
                </div>
            </div>
            <div class="nrl_option_item">
                <div class="nrl_search_field">
                    <label for="basement">Basement</label>
                    <?php if ( ! isset($meta_data['Basement'])) { $meta_data['Basement'] = ''; } ?>
                    <select id="basement" name="nrl_meta[Basement]" class="nrl_seed_field">
                        <option value=""<?php if ($meta_data['Basement']==''){echo "selected";}?>>Any</option>
                        <option value="1"<?php if ($meta_data['Basement']=='1'){echo "selected";}?>>Yes</option>
                        <option value="0"<?php if ($meta_data['Basement']=='0'){echo "selected";}?>>No</option>
                    </select>
                </div>
            </div>
            <div class="nrl_option_item">
                <div class="nrl_search_field">
                    <label for="bath_min">Baths(min)</label>
                    <input type="text" name="nrl_meta[Bath_min]" id="bath_min" value="<?php echo self::show_if_not_empty($meta_data, 'Bath_min');?>" >
                </div>
            </div>
            <div class="nrl_option_item">
                <div class="nrl_search_field">
                    <label for="bath_max">Baths(max)</label>
                    <input type="text" name="nrl_meta[Bath_max]" id="bath_max" value="<?php echo self::show_if_not_empty($meta_data, 'Bath_max');?>" >
                </div>
            </div>
            <div class="nrl_option_item">
                <input type="checkbox" name="nrl_meta[change_location]" value="true"<?php echo $location_change; ?> /> Allow Agents to change location settings for this template
            </div>
    <?php   $prop_types = NRL_Common::get_prop_types();
            $price_ranges = array(
                'Any'       => '',
                'to100000'  => 'Under $100,000'
            );
            for ( $cnt=1; $cnt<=19; $cnt++) {
                $min = $cnt * 100000;
                $max = ($cnt+1) * 100000;
                $key = $min . 'to' . $max;
                $val = '$' . number_format($min) . ' to $' . number_format($max);
               $price_ranges[$key] = $val;
            }
            $price_ranges['2000000to'] = "Over $2,000,000";
            $temp = 1;
    ?>
            <div class="nrl_option_item">

            </div>

    <div class="nrl_random">
            <h3>Random text lists for this article</h3>
    <div id='tabbed-nav' data-role='z-tabs' data-options='{"theme": "silver", "orientation": "vertical", "animation": "none" }'>
      <ul>
    <?php   // Display the tab titles
            foreach ( $meta_data['random'] as $slug => $list ) {
                echo "<li><a>$slug</a></li>";
            }
            echo "<li><a>New Random List</a></li>"; ?>
      </ul>
      <div>
    <?php   // Display the tab contents
            foreach ( $meta_data['random'] as $slug => $values ) {
                echo "<div>";
                echo "<p><b>Random Text for $slug &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tag:</b> {Random list=$slug <i>count=1 wrap=''</i>}</p>";
                $checked = ( $values['repeat'] == 'true' ? ' checked' : '' );
                $checked_no_repeat = ( ! empty($values['no_repeat']) && $values['no_repeat'] == 'true' ? ' checked' : '' ); ?>
          <p>
              <input type='checkbox' id='nrl_meta_random_<?php echo $slug; ?>_repeat' name="nrl_meta[random][<?php echo $slug; ?>][repeat]" value='true'<?php
                    echo $checked; ?> />
              <label for='nrl_meta_random_<?php echo $slug; ?>_repeat'>Repeat same value every time this tag is used in the template</label>
          </p>
            <p>
                <input type='checkbox' id='nrl_meta_random_<?php echo $slug; ?>_norepeat' name="nrl_meta[random][<?php echo $slug; ?>][no_repeat]" value='true'<?php
                echo $checked_no_repeat; ?> />
                <label for='nrl_meta_random_<?php echo $slug; ?>_norepeat'>No Repeat same value every time this tag is used in the template</label>
            </p>
    <?php       $cnt = 1;
                foreach ( $values['list'] as $item ) {
    //                $str = str_pad($cnt++, 2, " ", STR_PAD_LEFT);
                    $str = ( $cnt<10 ? '&nbsp;' . $cnt++ : $cnt++);
    //                echo "$str "; ?>
    <!--            <input type="text" size="65" name="nrl_meta[random][<?php //echo $slug; ?>][]" value="<?php //echo $item; ?>" /><br/>-->
                <textarea cols="65" rows="2" name="nrl_meta[random][<?php echo $slug; ?>][list][]"><?php echo $item; ?></textarea><br/>
    <?php       }
            for ( $cnt2=0; $cnt2<5; $cnt2++) {
                $str = ( $cnt<10 ? '&nbsp;' . $cnt++ : $cnt++);
    //            echo "$str "; ?>
    <!--            <input type="text" size="65" name="nrl_meta[random][<?php //echo $slug; ?>][]" /><br/>-->
                <textarea cols="65" rows="2" name="nrl_meta[random][<?php echo $slug; ?>][list][]"></textarea><br/><?php   }
            if ( NIMP_DEV_USER ) { ?>
                <input type="checkbox" name="nrl_meta[random][<?php echo $slug; ?>][delete]" id="nrl_meta[random][<?php echo $slug; ?>][delete]" value="true" />
                <label for="nrl_meta[random][<?php echo $slug; ?>][delete]">Delete this random list</label>
    <?php   }
            echo "</div>";
            }
            ?>
          <div><p><b>Add New Random List</b></p>
                <label for="nrl_random_new_name">Name for new random list check</label>
                <input type="text" name="nrl_meta[random][new_name][name]" id="nrl_random_new_name" />
                <span class="nrl_help"><a title="click for help" onclick="nrl_toggleVisibility('nrl_random_new_title');">help</a></span>
                <div class="nrl_tip" id="nrl_random_new_title">Enter a title for the new list. You can use only letters, numbers, dash, and underscore (no spaces)</div>
                <br/><br/>
    <?php   for ( $cnt=0; $cnt<10; $cnt++) { ?>
    <!--            <input type="text" size="60" name="nrl_meta[random][new_name][list][]" /><br/>-->
                <textarea cols="65" rows="2" name="nrl_meta[random][new_name][list][]"></textarea><br/>
    <?php   } ?>
          </div>
      </div>
    </div></div>
    </div>
    <?php
            return;
        }   // end function meta_box_display()

        static function show_selected( $meta_data, $field, $value ) {
            // Return ' selected' if the $meta_data field is not empty and = $value
            if ( ! empty($meta_data[$field] ) && $meta_data[$field] == $value ) {
                return ' selected';
            } else {
                return '';


            }
        }

        static function show_if_not_empty( $meta_data, $field ) {
            //return ( ! empty($meta_data[$field]) ? $meta_data[$field] : '' );
            if( !empty($meta_data[$field]) || $meta_data[$field] == 0 || $meta_data[$field] == '0' )
            {
              $out = $meta_data[$field];
            }
            else
            {
              $out = '';
            }

            return $out;
        }


        static function meta_box_tags_display($post) {

            if ( empty(self::$post_meta) ) {
                self::$post_meta = NRL_Common::get_article_meta_data( $post->ID );
            }

            if ( ! empty(self::$post_meta['seed']) && self::$post_meta['seed']=='false' ) {
                return;
            }
    //        $tag_list = array( 'County', 'City', 'Subdivision', 'Zip', 'Property_Type', 'Remarks', 'Property_Summary', 'Suggestions count=1', 'Factoids count=1 source=Subdivision',
    //                'Factoid_Source', 'Factoid_Count');
    //        $tag_list2 = array( 'City', 'Subdivision', 'Zip', 'Property_Type', 'Remarks', 'Property_Summary', 'Suggestions count=1', 'Factoids count=1 source=Subdivision',
    //                'Factoid_Source', 'Factoid_Count');
            $tag_list = array(  'County <i>formatted=true caps=false</i>',
                                'City',
                                'Subdivision',
                                'Zip',
                                'Address',
                                'Beds',
                                'Baths_Full',
                                'Baths_Half',
                                'Living_Area',
                                'Living_Area_p5',
                                'Living_Area_m5',
                                'Size_Adjective <i> caps=false</i>',
                                'Price',
                                'Price_p5',
                                'Price_m5',
                                'Price_Adjective  <i> caps=false</i>',
                                'Rebate',
                                'Property_Type <i>formatted=true plural=true caps=false</i> ',
                                'Property_Style',
                                'Property_Style <i>plural=false caps=false</i>',
                                'Remarks',
                                'DOM',
                                'Property_Summary',
                                'Age_Adjective <i>caps=false</i>',
                                'Photo <i>random=false</i>',
                                'Rebate',
                                'Community_Fee_Includes <i>caps=false</i>',
                                'Transportation',
                                'Address_Link',
                                'Subdivision_Link',
                                'Zip_Link',
                                'City_Link',
                                'Agent',
                                'Agent_Pic',
                                'Agent_Bio',
                                'Agent_Link <i>show=name</i> <i>caps=true</i>',
                                'Agent_Random_Text',
                                'Agent_Random_Pic',
                                'MLS','null');
            $tag_list2 = array( 'Median_Age', 'Median_Year_Built', 'Average_DOM', 'Common_Heating <i>caps=false</i>', 'Common_Cooling <i>caps=false</i>', 'Common_Hot_Water <i>caps=false</i>',
            'Common_Style', 'Common_Exterior <i>caps=false</i>','Common_Roof <i>caps=false</i>',  'Average_Living_Area', 'Basements <i>caps=true</i> <i>text=true</i>', 'Garages <i>caps=true </i> <i>text=true</i>', 'Price_High', 'Price_Low', 'Beds_High',
            'Beds_Low', 'Elementary_School <i>link=true</i>', 'Middle_School <i>link=true</i>', 'High_School <i>link=true</i>', 'Most_Expensive ', 'Least_Expensive', 'Similar_Price <i>count=3 source=Subdivision format=bullets range=number</i>',
                'Suggestions <i>count=1</i>','For_Rent','Sold', 'Factoids <i>count=1 source=Subdivision</i>', 'Factoid_Source', 'Factoid_Count');
    //        echo "<b>Available Tags, shown with defaults:</b>\n<ul>";
    ?>
    <div class="nrl_meta_box">
        <p><i>Optional parameters are shown in italics with defaults.</i></p>
        <h3>Data from seed listing</h3>
        <ul>
    <?php  foreach ( $tag_list as $tag ) {
                echo '<li>{' . $tag . '}</li>' . "\n";
            } ?>
        </ul>
        <h3>Data from listings in area</h3>
        <ul>
    <?php  foreach ( $tag_list2 as $tag ) {
                echo '<li>{' . $tag . '}</li>' . "\n";
            } ?>
        </ul>
    <?php  if ( ! empty(self::$post_meta['random']) ) { ?>
        <h3>Random Lists from this template</h3>
        <ul>
    <?php      foreach ( self::$post_meta['random'] as $slug => $list ) {
                    $tag_list2[] = 'Random list=' . $slug;
                    echo '<li>{Random list=' . $slug . " <i>count=1 wrap=''</i>}</li>" . "\n";
                } ?>
        </ul>
    <?php  } ?>

        <p><i>Possible values for wrap are: p, div, span, ul, or ol. Items within ul or ol tags will automatically be enclosed in li tags.</i></p>
        <p><span class="nrl_help"><a title="click for help" onclick="nrl_toggleVisibility('nrl_tags_help');">help</a></span></p>
        <div class="nrl_tip" id="nrl_tags_help">
            The Factoids tag has two optional parameters<br/>
            1. <b>count</b> tells how many factoids to display. It has several options
            <ul>
                <li>a number (count=3)</li>
                <li>a random range of numbers (count=1-4)</li>
                <li>roll of dice (count=2d4)</li>
            </ul>
            2. <b>source</b> gives the factoid source type. It should be one of the following:
            <ul>
                <li>Subdivision</li>
                <li>Zip</li>
                <li>Polygon</li>
                <li>Random (default) (randomly choose one of the first three) </li>
            </ul>
            Factoid_Source and Factoid_Count are defined only if the article includes a Factoids tag.
        </div>
    </div>
    <?php
        }   // end function meta_box_tags_display()

        static function meta_box_save($post_id) {
            // Save the data from the meta box

            // Security checks
            if ( ! self::security_check($post_id) )
                return;


            // See if we need to add a new random list
            $new_meta = NRL_Common::get_post_var('nrl_meta', true);
            $temp = 1;

            // See if there are any unchecked checkboxes
            if ( empty($new_meta['seed'])) {
                $new_meta['seed'] = 'false';
            }
            if ( empty($new_meta['change_location']) ) {
                $new_meta['change_location'] = 'false';
            }

    //        $meta_data = array();
            if ( ! empty($new_meta['random']['new_name']['name'])) {
                $temp = 1;
    //            $slug = NIMP_Util::make_slug($meta_data['random']['new_name']['name']);
                $name = str_replace(' ', '-', $new_meta['random']['new_name']['name']);
                $name =  preg_replace('/[^a-zA-Z0-9-_]/', '', $name);
    //            $new_meta['random'][$name] = $new_meta['random']['new_name']['list'];
                $new_meta['random'][$name] = $new_meta['random']['new_name'];
            }
            unset($new_meta['random']['new_name']);
            $temp = 1;

            // Update existing lists
            foreach ( $new_meta['random'] as $name => $values ) {
                if ( ! empty($values['delete']) ) {
                    unset( $new_meta['random'][$name] );
                } else {
                    $temp = 1;
                    foreach ( $values['list'] as $key => $item ) {
                        $text = trim($item);
                        if ( empty($text) ) {
                            unset($new_meta['random'][$name]['list'][$key]);
                        } else {
                            // Remove smart quotes
                            $new_meta['random'][$name]['list'][$key] = str_replace('&#34;', '"', $item);
                        }
                    }
                }
                if ( ! isset($values['repeat']) ) {
                    $new_meta['random'][$name]['repeat'] = 'false';
                }
            }

            // Update the post meta
            update_post_meta($post_id, 'nrl_meta', $new_meta);

        }

            static function security_check($post_id) {
                //  Verify this came from our edit screen
                // Check if our nonce is set.
                $nrl_nonce = NRL_Common::get_post_var('nrl_meta_box_nonce');
    //            if (!isset($_POST['nrl_meta_box_nonce'])) {
                if ( empty($nrl_nonce) ) {
                    return false;
                }

                // Verify that the nonce is valid.
                if ( ! wp_verify_nonce($nrl_nonce, 'nrl_meta_box') ) {
                    return false;
                }

                // If this is an autosave, our form has not been submitted, so we don't want to do anything.
                if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) {
                    return false;
                }

                // Check the user's permissions.
                $post_type = NRL_Common::get_post_var('post_type');
                if ( ! empty($post_type) && 'page' == $post_type ) {
                    if ( ! current_user_can('edit_page', $post_id)) {
                        return false;
                    }
                } else {
                    if ( ! current_user_can('edit_post', $post_id)) {
                        return false;
                    }
                }
                return true;
            }   // end security_check()

// ***** Moved to NRL_Common
//        static function get_meta_data( $id ) {
//            // Get meta data for article template with post ID $id
//            $meta_data = get_post_meta( $id, 'nrl_meta', true );
//            $temp = 1;
//            if ( empty($meta_data) ) {
//                $meta_data = array(
//                    'list_type' => 'sales',
//                    'title'     => '',
//                    'random'    => array()
//                );
//            }
//            // LEGACY support
//            if ( empty($meta_data['list_type']) ) {
//                $meta_data['list_type'] = 'sales';
//            }
//    //        if ( empty($meta_data['title']) ) {
//    //            $meta_data['title'] = '';
//    //        }
//            // LEGACY: See if we need to change the array structure
//    //        if ( empty($meta_data['random']) ) {
//    //            $meta_data['random'] = array();
//    //        }
//            foreach ( $meta_data['random'] as $slug => $values ) {
//                $new_random = array();
//                if ( ! empty($values) && ! isset($values['list']) ) {
//                    $new_random['list'] = $meta_data['random'][$slug];
//                    $meta_data['random'][$slug] = $new_random;
//                }
//                if ( empty($meta_data['random'][$slug]['repeat']) ) {
//                    $meta_data['random'][$slug]['repeat'] = 'false';
//                }
//                $temp = 1;
//            }
//
//            // LEGACY 9/13/17
//            if ( empty($meta_data['post_location_type']) && isset($meta_data['post_select_type']) ) {
//                $meta_data['post_location_type'] = $meta_data['post_select_type'];
//                if ( empty($meta_data['post_location_value']) && isset($meta_data['post_select_value']) ) {
//                    $meta_data['post_location_value'] = $meta_data['post_select_value'];
//                    unset( $meta_data['post_select_value'] );
//                }
//                $meta_data['post_select_type'] = '';
//            }
//
//            return $meta_data;
//        }


    }   // end class NRL_Custom_Posts
}
