<?php
/**
 * Installation functions for NRL Imports plugin
 * These functions are needed only for plugin installation or updating
 * @author Ken Carlson <kencarlsonconsulting.com>
 */
class NIMP_Install {
    static $log='';
    
    static function setup() {
//        self::$log = "Installation on " + date('Y-m-d') + "\n";
    }
        
    static function create_tables( $group=1) {
        // Create/update all the tables for the plugin
//        ob_start();
        $result = true;
        if ( $group == 1 ) {
            $result = ( self::create_settings_table() === false ? false : $result );
            $result = ( self::create_fields_table('sales') === false ? false : $result );
            $result = ( self::create_fields_table('rentals') === false ? false : $result );
            $result = ( self::create_cities_table() === false ? false : $result );            
        } else {
            $result = ( self::create_properties_table('sales') === false ? false : $result );
            $result = ( self::create_properties_table('rentals') === false ? false : $result );
            $result = ( self::create_media_table() === false ? false : $result );
            $result = ( self::create_lookup_table() === false ? false : $result );
        }

//        $errors = ob_get_clean();
        return $result;
    }
    
    static function check_tables( $group=1) {
        // Checks for the existence of required tables
        // The tables need to be set up in two groups, becaue the properties tables require the field tables to create
        // Returns true or false
        global $wpdb;
       
        if ( $group == 1 ) {
            $table_names = array(
                $wpdb->nrl_settings,
                $wpdb->nrl_fields_sales,
                $wpdb->nrl_fields_rentals,
                $wpdb->nrl_cities
            );            
        } else {
            $table_names = array(
                $wpdb->nrl_properties_sales,
                $wpdb->nrl_properties_rentals,
                $wpdb->nrl_lookup,
            );            
        }

        $result = true;
        foreach ( $table_names as $table ) {
            if ( $wpdb->get_var("SHOW TABLES LIKE '$table'") != $table ) {
                $result = false;
            }
        }
        return $result;
    }
    
    static function create_fields_table( $type='sales' ) {
        // Create or update the structure of the database table        
        // For backward compatibility...
        global $wpdb;
//        if ( empty($table_name) ) {
            $table_name = "{$wpdb->prefix}nrl_fields_$type"; // $wpdb->{nrl_fields . "_$type"};
//        }
        // KPC *** Should some of the columns be Integers?
       
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );  // Loads dbDelta()
        
//        $table_name = $wpdb->prefix . 'nrl_fields';
        $sql = "CREATE TABLE $table_name (\n";
//        $sql .= 'id INT(11) AUTO_INCREMENT,' . "\n";
        $sql .= 'SystemName varchar(50),' . "\n";
        $sql .= 'LongName varchar(50),' . "\n";
        $sql .= 'MaximumLength varchar(50),' . "\n";
        $sql .= 'DataType varchar(50),' . "\n";
        $sql .= 'Precision1 varchar(50),' . "\n";   // Precision is a MySQL reserve word
        $sql .= 'Interpretation varchar(50),' . "\n";
        $sql .= 'UseSeparator varchar(50),' . "\n";
        $sql .= 'EditMaskID varchar(50),' . "\n";
        $sql .= 'LookupName varchar(50),' . "\n";
        $sql .= 'MaxSelect varchar(50),' . "\n";
        $sql .= "Import char(1) default '1'," . "\n";           // Import this field?
        $sql .= "Section varchar(50) default 'Other'," . "\n";  // Section for display in detailed listing
        $sql .= 'PRIMARY KEY  (SystemName) )';

        $result = dbDelta( $sql );  // Returns an array of the results of the db operations
        if ( false === $result ) {
            // Error creating table
            self::$log .= "Error creating Firlds table\n";
            $temp = 1;
        } else {
//            echo "+ Fields table created/updated<br/>\n";
            self::$log .= "+ Fields table created/updated<br/>\n";
        }
        return $result;
    }
    
    static function create_properties_table( $type='sales' ) {
        // Create or update structure of nrl_properties table
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );  // Loads dbDelta()
        global $wpdb;
        $table_name = "{$wpdb->prefix}nrl_properties_$type"; //wpdb->{nrl_properties . "_$type"};
        
        // Temporarily disable cron jobs so that the table is not modified while it is being updated
        self::disable_cron_jobs();
        $fields_table = "{$wpdb->prefix}nrl_fields_$type"; //$wpdb->{nrl_fields . "_$type"};
        $query = "SELECT * FROM $fields_table";
        $field_list = $wpdb->get_results( $query, ARRAY_A );
        $key_field = 'ListingKey';
        $sql = self::create_property_table_sql( $table_name, $field_list, $key_field );
        $result = dbDelta( $sql );  // Returns array of the result of the update operations
        self::enable_cron_jobs();
        if ( false === $result ) {
            self::$log .= "Error creating Properties table\n";
        } else {
//        echo str_replace("\n","<br/>\n", print_r($result,true));
            self::$log .= "+ Properties table created/updated<br/>\n";            
        }
        self::enable_cron_jobs();
        return $result;
    }
    
    static function create_property_table_sql( $table_name, $field_list, $key_field, $field_prefix = "" ) {
        // Based on code from PHRETS Wiki
        
        $sql_query = "CREATE TABLE $table_name (\n";
        $sql_query .= "id int(11) AUTO_INCREMENT,\n";
//        $sql_query .= "PostID bigint(20),\n";             // ID of WP Post for this listing
//        $sql_query .= "OldPhotos boolean default '0',\n";   // Does this listing have photos in the old directory structure?
//        $sql_query .= "OldId int(11) default NULL,\n";        // id for old listing table (used to build old photo urls
        $sql_query .= "RewriteURLs BOOLEAN DEFAULT 0 COMMENT 'Rewrite URLs to point to our own server',\n";

        foreach ($field_list as $field) {
            
            $sql_make = self::create_field_sql( $field, $field_prefix );

//            $field['SystemName'] = "{$field_prefix}{$field['SystemName']}";
//
//            $cleaned_comment = addslashes($field['LongName']);
//
//            $sql_make = "{$field['SystemName']} ";
//
//            if ($field['Interpretation'] == "LookupMulti") {
//                $sql_make .= "text";
//            }
//            elseif ($field['Interpretation'] == "Lookup") {
//                $sql_make .= "tinytext";  // was varchar(255)
//            }
//            elseif ($field['DataType'] == "Int" || $field['DataType'] == "Small" || $field['DataType'] == "Tiny") {
//                $sql_make .= "int({$field['MaximumLength']})";
//            }
//            elseif ($field['DataType'] == "Long") {
//                    $sql_make .= "bigint({$field['MaximumLength']})";
//            }
//            elseif ($field['DataType'] == "DateTime") {
//                    $sql_make .= "datetime default '0000-00-00 00:00:00' not null";
//            }
//            elseif ($field['DataType'] == "Character" && $field['MaximumLength'] <= 255) {
//                    $sql_make .= "varchar({$field['MaximumLength']})";
//            }
//            elseif ($field['DataType'] == "Character" && $field['MaximumLength'] > 255) {
//                    $sql_make .= "text";
//            }
//            elseif ($field['DataType'] == "Decimal") {
////                    $pre_point = ($field['MaximumLength'] - $field['Precision']);
//                    $post_point = !empty($field['Precision1']) ? $field['Precision1'] : 0;
//                    $sql_make .= "decimal({$field['MaximumLength']},{$post_point})";
//            }
//            elseif ($field['DataType'] == "Boolean") {
//                    $sql_make .= "char(1)";
//            }
//            elseif ($field['DataType'] == "Date") {
//                    $sql_make .= "date default '0000-00-00' not null";
//            }
//            elseif ($field['DataType'] == "Time") {
//                    $sql_make .= "time default '00:00:00' not null";
//            }
////            elseif ($field['DataType'] == 'Lookup')  {
////                
////            }
//            else {
//                    $sql_make .= "varchar(255)";
//            }
//
//            $sql_make .= " COMMENT '{$cleaned_comment}'";
//            $sql_make .= ",\n";
            $sql_query .= $sql_make;
            $temp = 1;
        }

        $sql_query .= "PRIMARY KEY  ({$field_prefix}id),\n";
        $sql_query .= "UNIQUE KEY KEY_FIELD ({$field_prefix}{$key_field}) )";
        return $sql_query;
    }    // end function create_property_table_sql

    static function create_media_table( $field_prefix='' ) {
        
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );  // Loads dbDelta()
        global $wpdb;
        $table_name = "{$wpdb->prefix}nrl_media"; 
        
        // Temporarily disable cron jobs so that the table is not modified while it is being updated
        self::disable_cron_jobs();
        
        // This is different, because the Media resource field names are not stored in a table
        $media_import_fields = NIMP_Util::csv_file_get_assoc( NIMP_PATH . "setup/media_import_fields.csv" );

        // Build the sql to create the table
        $sql_query = "CREATE TABLE $table_name (\n";
        $sql_query .= "id int(11) AUTO_INCREMENT,\n";
//        $sql_query .= "BrightMediaHost text COMMENT 'Host for Bright MLS media server',\n";
        $sql_query .= "RewriteURLs BOOLEAN DEFAULT 0 COMMENT 'Rewrite URLs to point to our own server',\n";
        
        foreach ( $media_import_fields as $field ) {
            if ( $field['Import']=='X' ) {
                $sql_query .= self::create_field_sql( $field, $field_prefix ); 
            }
        }        
        $sql_query .= "PRIMARY KEY  ({$field_prefix}id),\n";
        $sql_query .= "KEY MEDIA_KEY ({$field_prefix}MediaKey),\n";
        $sql_query .= "KEY RESOURCE_RECORD_KEY ({$field_prefix}ResourceRecordKey) )";
        
        $result = dbDelta( $sql_query );  // Returns array of the result of the update operations
        if ( false === $result ) {
            self::$log .= "Error creating Media table\n";
        } else {
//        echo str_replace("\n","<br/>\n", print_r($result,true));
            self::$log .= "+ Media table created/updated<br/>\n";            
        }
        self::enable_cron_jobs();
        return $result;         
    }
    
    static function create_field_sql( $field, $field_prefix = "" ) {
        // Create and return the sql line to create $field

        $field['SystemName'] = "{$field_prefix}{$field['SystemName']}";
        $sql_make = "{$field['SystemName']} ";

        if ($field['Interpretation'] == "LookupMulti") {
            $sql_make .= "text";
        }
        elseif ($field['Interpretation'] == "Lookup") {
            $sql_make .= "tinytext";  // was varchar(255)
        }
        elseif ($field['DataType'] == "Int" || $field['DataType'] == "Small" || $field['DataType'] == "Tiny") {
            $sql_make .= "int({$field['MaximumLength']})";
        }
        elseif ($field['DataType'] == "Long") {
            $sql_make .= "bigint({$field['MaximumLength']})";
        }
        elseif ($field['DataType'] == "DateTime") {
            $sql_make .= "datetime default '0000-00-00 00:00:00' not null";
        }
        elseif ($field['DataType'] == "Character" && $field['MaximumLength'] <= 255) {
            $sql_make .= "varchar({$field['MaximumLength']})";
        }
        elseif ($field['DataType'] == "Character" && $field['MaximumLength'] > 255) {
            $sql_make .= "text";
        }
        elseif ($field['DataType'] == "Decimal") {
//                    $pre_point = ($field['MaximumLength'] - $field['Precision']);
            $post_point = !empty($field['Precision1']) ? $field['Precision1'] : 0;
            $sql_make .= "decimal({$field['MaximumLength']},{$post_point})";
        }
        elseif ($field['DataType'] == "Boolean") {
            $sql_make .= "char(1)";
        }
        elseif ($field['DataType'] == "Date") {
            $sql_make .= "date default '0000-00-00' not null";
        }
        elseif ($field['DataType'] == "Time") {
            $sql_make .= "time default '00:00:00' not null";
        }
//            elseif ($field['DataType'] == 'Lookup')  {
//                
//            }
        else {
            $sql_make .= "varchar(255)";
        }

        if ( ! empty($field['LongName']) ) {
            $cleaned_comment = addslashes($field['LongName']);
            $sql_make .= " COMMENT '{$cleaned_comment}'";
        }
        $sql_make .= ",\n";
    
        return $sql_make;
    }
    
    static function create_lookup_table() {
        // Create lookup table from array
        global $wpdb;
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );  // Loads dbDelta()

        // Create the table
        $table_name = $wpdb->nrl_lookup;
        $sql = "CREATE TABLE $table_name (\n";
//        $sql .= 'id INT(11) AUTO_INCREMENT,' . "\n";
        $sql .= 'lookup_name varchar(50),' . "\n";
//        $sql .= 'code INT(11),' . "\n";
        $sql .= 'code varchar(50),' . "\n";
        $sql .= 'value varchar(50),' . "\n";
//        $sql .= 'PRIMARY KEY  (id),' . "\n";
        $sql .= 'PRIMARY KEY  (code),' . "\n";
        $sql .= 'KEY LOOKUP  (lookup_name) )';
//        $sql .= 'INDEX sort  (lookup_name,code) )';
        
//        $result = true;
        $result = dbDelta( $sql );  // Returns array of the result of the update operations
        if ( false === $result ) {
            self::$log .= "Error creating Lookup table\n";
        } else {
            self::$log .= "+ Lookup table created/updated<br/>\n";
        }
        $temp = 1;
        return $result;
    }
    
    static function create_settings_table() {
        // Create or update the structure of the nrl settings table        
        
        global $wpdb;
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );  // Loads dbDelta()
        
        $table_name = $wpdb->prefix . 'nrl_settings';
        $sql = "CREATE TABLE $table_name (\n";
//        $sql .= 'id INT(11) AUTO_INCREMENT,' . "\n";
        $sql .= 'option_name varchar(50),' . "\n";
        $sql .= 'option_value longtext,' . "\n";
//        $sql .= "PRIMARY KEY  (id),\n";
//        $sql .= "UNIQUE KEY OptionName (option_name) )";
        $sql .= "PRIMARY KEY (option_name) )";

        $result = dbDelta( $sql );  // Returns an array of the results of the db operations
        if ( false === $result ) {
            self::$log .= "Error creating Settings table\n";
        } else {
            self::$log .= "+ Settings table created/updated<br/>\n";
        }
        return $result;
    }   // end function create_options_table()    
    
    
    static function create_cities_table() {
        // Creates table used for county, city, zip
        // SalesNew and RentalNew are used by the main site to add county/city location pages
        global $wpdb;
        
        $table_name = $wpdb->nrl_cities;
//        if ( $wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name ) {
            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );  // Loads dbDelta()
            $sql = "CREATE TABLE `$table_name` (\n";
            $sql .= 'id INT(11) AUTO_INCREMENT,' . "\n";
            $sql .= 'County varchar(50),' . "\n";
            $sql .= 'City varchar(50),' . "\n";
//            $sql .= 'Subdivision varchar(50),' . "\n";
            $sql .= 'Zips varchar(200),' . "\n";
//            $sql .= 'Count INT(7),' . "\n";
            $sql .= 'Sales BOOLEAN DEFAULT 0,' . "\n";        // This city has For Sale listings
//            $sql .= 'SalesNew BOOLEAN DEFAULT 0,' . "\n";     // This is a newly added For Sale listing city
            $sql .= 'Rental BOOLEAN DEFAULT 0,' . "\n";       // This city has For Rental listings
//            $sql .= 'RentalNew BOOLEAN DEFAULT 0,' . "\n";    // This is a newly added Rental listing city
            $sql .= 'LastMod DATETIME DEFAULT CURRENT_TIMESTAMP,' . "\n";
            $sql .= 'PRIMARY KEY  (id),'. "\n";
            $sql .= 'KEY COUNTY  (County),'. "\n";
            $sql .= 'KEY CITY  (City) )';
            $result = dbDelta( $sql );  // Returns an array of the results of the db operations
            if ( false === $result ) {
                self::$log .= "Error creating Cities table\n";
            } else {
                self::$log .= "+ Cities table created/updated<br/>\n";
            }          

        return $result;        
    }  // end function create_cities_table()
    

    
    static function create_articles_table() {
        // Creates table used to track generated articles and listing IDs
        global $wpdb;
        
        $table_name = $wpdb->nrl_articles;
        if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );  // Loads dbDelta()
            $sql = "CREATE TABLE `$table_name` (\n";
            $sql .= 'id INT(11) AUTO_INCREMENT,' . "\n";
            $sql .= 'PostID bigint(20),' . "\n";
            $sql .= 'ListingKey varchar(15),' . "\n";
            $sql .= 'PRIMARY KEY  (id),'. "\n";        
            $sql .= 'KEY ListingKey (ListingKey),'. "\n";
            $sql .= 'KEY PostID (PostID) )';
            $result = dbDelta( $sql );  // Returns an array of the results of the db operations
            if ( false === $result ) {
                // Error creating table
                self::$log .= "Error creating Articles table\n";
                $temp = 1;
            }          
        } else {
            $result = false;
        }
        return $result;        
    }  // end function create_articles_table()    
    

    static function check_settings() {
        // Check to be sure that the settings exist
        // If they are missing, import them or use the defaults
        global $wpdb;
        $table_name = $wpdb->nrl_settings;        
        if ( $wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name ) {
            self::create_settings_table();
        }
//        $nrl_settings = $wpdb->get_results( "SELECT option_name FROM $wpdb->nrl_settings", ARRAY_A );
        
        $nrl_settings = $wpdb->get_col( "SELECT option_name FROM $table_name" );
        $temp = 1;
        
        // See if the settings table is empty
//        $count = $wpdb->get_results( "SELECT COUNT(*) FROM $wpdb->nrl_settings", ARRAY_A );
//        $temp = 1;
//        if ( empty( $count[0]['COUNT(*)'] || $count[0]['COUNT(*)'] == 0) ) {
        if ( empty( $nrl_settings ) )  {
            // Try to import mls settings from the old version of the plugin (or set defaults)
            $result = false;
            $result = NIMP_Install::import_nrl_settings();
            if ( ! $result ) {
                $temp = 1;
            }
        } 
        
        // Check for specific settings that might need to be initialized
        if ( empty($nrl_settings['import_status_sales']) ) {
            $defaults = self::get_defaults('import_status');
            NRL_Common::update_nrl_setting( 'import_status_sales', $defaults );
        }
        if ( empty($nrl_settings['import_status_rentals']) ) {
            $defaults = self::get_defaults('import_status');
            NRL_Common::update_nrl_setting( 'import_status_rentals', $defaults );
        }            
                
        $temp = 1;
    }
    
    static function get_defaults( $type ) {
        
        switch ( $type ) {
            case 'import_status':
                $default = array(
                    'initial_import'    => true, 
                    'initial_import_phase'  => 0,           // 0 = needs initialize, 1 or 2 = phase
                    'new_pass'          => true,
                    'added'             => 0,                // Properties added in this pass
                    'updated'           => 0,                // Properties updated in this pass
                    'deleted'           => 0,                // Properties deleted in this pass
                    'offset'            => 0,                // offset into my list of properties
                    'remaining'         => 0,                // Properties remaining in this pass
                    'photos_added'      => 0,                // Photos added in this pass
                    'photos_deleted'    => 0,                // Photos deleted in this pass            
                    'error_cnt'         => 0,                // Error count for this pass                    
                    'prev_start_time' => '',
                    'new_start_time'  => '',
                    'pass_start_datetime'   => '',
                );
                break;
            default:
                $default = array();
             
        }
        return $default;
    }
    
    // ***** Upgrade Code *****
    static function import_nrl_settings() {
        // Code to create settings table and populate it from MLS Options
        
//        $settings_fields = array( 'field_formats', 'import_fields', 'rental_zips', 'import_status');
//        $mls_options = NIMP_MLS_Util::get_options();
        // Try to get the old MLS options
        $mls_options = get_option( NIMP_MLS_OPTIONS_NAME );
        if ( ! empty($mls_options) ) {
            $result = true;
//            $field_formats = $mls_options['field_formats'];
//            $import_fields = $mls_options['import_fields'];
            $new_rental_zips = $mls_options['rental_zips'];

//            $new_import_status = array(
//                'initial_import'  => $mls_options['initial_import'],
//                'new_pass'        => $mls_options['new_pass'],
//                'prev_start_time' => $mls_options['prev_start_time'],
//                'new_start_time'  => $mls_options['new_start_time'],
//                'offset'          => $mls_options['offset'],                // offset into my list of properties
//                'remaining'       => $mls_options['remaining'],               // Properties remaining in this pass
//                'added'           => $mls_options['added'],                   // Properties added in this pass
//                'updated'         => $mls_options['updated'],                 // Properties updated in this pass
//                'deleted'         => $mls_options['deleted'],                 // Properties deleted in this pass
//                'rentals_added'   => $mls_options['rentals_added'],           // Rental listings added in this pass
//                'photos_added'    => $mls_options['photos_added'],            // Photos added in this pass            
//                'photos_deleted'  => $mls_options['photos_deleted'],          // Photos deleted in this pass            
//                'error_cnt'       => $mls_options['error_cnt'],               // Error count for this pass
//            );

//            NRL_Common::update_nrl_setting( 'import_fields', $import_fields );
//            NRL_Common::update_nrl_setting( 'field_formats', $field_formats );
            
            $rental_zips = NRL_Common::get_nrl_setting('rental_zips');
            if ( empty( $rental_zips ) ) {
                NRL_Common::update_nrl_setting( 'rental_zips', $new_rental_zips );
            }
            
//            $import_status = NRL_Common::get_nrl_setting('import_status');
//            if ( empty( $import_status ) ) {
//                NRL_Common::update_nrl_setting( 'import_status', $new_import_status );
//            }

            $nrl_options = get_option('nrl_options');
            $factoid_text = NRL_Common::get_nrl_setting('factoid_text');
            if ( empty( $factoid_text ) && ! empty($nrl_options['factoid_text']) ) {
                NRL_Common::update_nrl_setting( 'factoid_text', $nrl_options['factoid_text'] );
            }
            
            // See if the import plugin options are empty
            $nimp_options = NIMP_Util::get_options();
            if ( empty($nimp_options) ) {
                // Merge defaults into $mls_options in case any options are missing
                $defaults = NIMP_Util::set_defaults();
                $mls_options = array_merge( $defaults, $mls_options );

                // Set import plugin WP options based on the old options
                $new_options = array(
                    // Settings
                    'import_limit'  => $mls_options['import_limit'],        // How many properties to attempt to import in one run
                    'picture_limit' => $mls_options['picture_limit'],
                    'allow_cron'    => $mls_options['allow_cron'],
//                    'suspend_imports'   => ( empty($mls_options['suspend_imports']) || $mls_options['suspend_imports'] === true ? true : false),
                    'suspend_imports'   => false,
                    'suspend_imports_after_pass'    => false,               // Suspend imports when the current import pass is finished
                    'notification_email'    => $nrl_options['notification_email'],
                    'build_version'         => $nrl_options['build_version'],
                    'prop_table_version'    => '3',

                    // MRIS login info            
                    'url'   =>  'http://csrets.mris.com:6103/platinum/login',
                    'user'  =>  '3082558',
                    'pwd'   =>  '855177',
                    'agent'         =>  'RETS Test/1.0',
                    'unique'        =>  'ListingKey',

                );
                NIMP_Util::update_options( $new_options, 'NIMP_Install::import_nrl_settings' );
            }
        } else {
            // The nimp options will be set to the defaults the first time they are loaded
            $result = false;
        }

        // Get WP Options that need to be global settings
        $polygons = NRL_Common::get_nrl_setting('polygons');
        if ( empty( $polygons ) ) {
            $polygons = get_option( NIMP_POLYGON_OPTION_NAME );
            if ( empty( $polygons ) ) {
                $polygons = array();
            }
            NRL_Common::update_nrl_setting( 'polygons', $polygons );
        }
        
        return $result;
    } // end function import_nrl_settings()

    static function enable_cron_jobs() {
        $options = NIMP_Util::get_options();
        $options['allow_cron'] = true;
        NIMP_Util::update_options( $options, 'MLS_UTIL::enable_cron_jobs()' );
    }
    
    static function disable_cron_jobs() {
        $options = NIMP_Util::get_options();
        $options['allow_cron'] = false;
        NIMP_Util::update_options( $options, 'MLS_UTIL::disable_cron_jobs()' );        
    }
    
    // *** OLD CODE ***
     
    // *** This is no longer used! ***    
// This is now handled through NIMP_Admin::ajax_setup()    
    static function install_plugin_OLD() {
        // Create the tables and import the settings to install the plugin
        // Called by NIMP_Util::activation()
//        self::setup();
//        
        // Set PHP maximum execution time if necessary
        $max_time = ini_get('max_execution_time');
        if ( $max_time < 300 ) {
            ini_set( 'max_execution_time', 300 );
        }
        
        ob_start();
$log = date('Y-m-d H:i:s') . " NRL Begin installation...\n";
NRL_Common::write_log( $log, NIMP_INSTALL_LOG );
            
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );  // Loads dbDelta()
        $mris = new NIMP_MLS();
        // Get the names of the MRIS data fields
        self::create_fields_table();
        $msg = $mris->import_fields();
NRL_Common::write_log( $msg, NIMP_INSTALL_LOG );
//        self::$log .= $msg;

//if ( 1 == 2 ) {        
        // Create lookup and propterties tables
        self::create_lookup_table();
        $mris->import_mls_lookups( 'property' );
        self::create_properties_table();
        self::create_settings_table();
        
        // Import settings from csv files
        $result = NIMP_Util::csv_file_get_assoc( NIMP_PATH . 'setup/' . NIMP_FIELDS_OPTION_NAME .".csv", true);
        if ( ! $result ) {
            self::$log .= "Failed to import options csv file\n";
        }
        $result = NIMP_Util::csv_file_get_assoc( NIMP_PATH . 'setup/' . NIMP_SEARCH_OPTION_NAME .".csv", true); //NIMP_MLS_Util::update_search_fields_csv();
        if ( ! $result ) {
            self::$log .= "Failed to import search csv file\n";
        }
        $result = NIMP_Util::csv_file_get_assoc( NIMP_PATH . 'setup/' . NIMP_PROP_TYPE_OPTION_NAME .".csv", true, true);
        if ( ! $result ) {
            self::$log .= "Failed to import property type csv file\n";
        }

        // Create auxillary tables
        self::create_cities_table();
        self::create_articles_table(); 
// Write log messages from table creation and csv import
NRL_Common::write_log( self::$log, NIMP_INSTALL_LOG );        
//}        
        // Add any output to the log *** FOR TESTING ***
        $text = ob_get_clean();
        if ( ! empty( $text ) ) {
//            self::$log .= "Output text: $text \n";
NRL_Common::write_log( "Output text: $text \n", NIMP_INSTALL_LOG );        
        } else {
NRL_Common::write_log( date('Y-m-d H:i:s') . "Installation complete. \n", NIMP_INSTALL_LOG );        
        }
        
        // Write install log
        if ( ! empty(self::$log) ) {
//            self::$log = date('Y-m-d') . " NRL Installation log:\n" . self::$log . "\n";
//            NRL_Common::write_log( self::$log, NIMP_INSTALL_LOG );
        }
        
    }   // end function install_plugin() *** OLD ***
    
    
}
