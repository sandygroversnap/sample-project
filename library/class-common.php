<?php
/**
 * Nesbitt Real Estate Listings AND Nesbitt Imports
 * Common functions used by BOTH of the above plugins
 * 
 * 
 *  ********** Do not change this file without approval from the lead developer **********
 * 
 *  ********** Do not change this file without syncing it with both plugins **********
 * 
 * 
 * @author Ken Carlson <kencarlsonconsulting.com>
 **/

// This file might be included by both plugins, so don't declare it twice!
if ( ! class_exists('NRL_Common') ) {
    
    define('NRL_PHOTO_SCHEME', 'http' );
    
    add_action( 'init', array('NRL_Common','create_post_type') );
    
    class NRL_Common {
        // Functions common to both plugins
        static $options;

        static function create_post_type() {
            // Create custom post type for article templates
            // For now, only show ui if the Imports plugin is active
            $show_ui =  class_exists( 'NIMP_Util');
            register_post_type( 'nrl_articles',
                array(
                    'labels' => array(
                        'name' => __( 'Article Templates' ),
                        'singular_name' => __( 'Article' ),
                //		'menu_name'          => _x( 'Templates', 'admin menu'),
                        'name_admin_bar'     => _x( 'Article Template', 'add new on admin bar' ),
                        'add_new'            => _x( 'Add New', 'template' ),
                        'add_new_item'       => __( 'Add New Article Template' ),
                        'new_item'           => __( 'New Article Template' ),
                        'edit_item'          => __( 'Edit Article Template' ),
                        'view_item'          => __( 'View Article Template' ),
                        'all_items'          => __( 'All Article Templates' ),
                        'search_items'       => __( 'Search Article Templates' ),
                        'not_found'          => __( 'No Article Templates found.' ),
                        'not_found_in_trash' => __( 'No Article Templates found in Trash.' )
                        ),
                    'public'        => true,
                    'has_archive'   => false,
                    'taxonomies'    => array('category', 'post_tag'),
                    'exclude_from_search' => true,
                    'publicly_queryable' => true,
                    'show_ui'       => $show_ui,
                    'query_var'     => true,
    //                'register_meta_box_cb' => array('NIMP_Posts','init_meta_box'),
    //                'supports' => false,
                    'menu_position' => 5,
                )
            );

        }   // end function create_post_type

        static function get_nrl_setting( $name='', $use_array=true ) {
            // Get the nrl_settings table setting $name 
            // If $use_array is true, return an empty array if the setting is empty or missing
            if ( empty($name) ) {
                return;
            }
          
            // Use the Imports site db if defined
            global $wpdb, $wpdb_nrl;
            $my_wpdb = ( ! empty($wpdb_nrl) ? $wpdb_nrl : $wpdb );
            $query = "SELECT option_value FROM $my_wpdb->nrl_settings WHERE option_name='$name'";
            $option = $my_wpdb->get_results( $query, ARRAY_N );

            if ( ! empty($option[0][0]) ) {
                $result = unserialize($option[0][0]);
            } else {
                $result = ( $use_array ? array() : '' );
            }
            return $result;
        }

        static function update_nrl_setting( $name , $value ) {
            // Update the nrl_settings table setting $name 
            if ( empty($name) ) {
                return;
            }
            
            // Use the Imports site db if defined
            global $wpdb, $wpdb_nrl;
            $my_wpdb = ( ! empty($wpdb_nrl) ? $wpdb_nrl : $wpdb );
            
            $fields = array(
                'option_name'   => $name,
                'option_value'  => serialize( $value ),
            );
            $result = $my_wpdb->replace( $my_wpdb->nrl_settings, $fields );
            if ( $result === false ) {
                $temp = 1;
            }
            return $result;
        }
        
        static function get_article_types( $field, $index_key='' ) {
            // Returns a list of the names of the posts in the nrl_articles custom post type
            // $field is the post field to return, $index_key is the array key

            // Use the imports plugin database to get article types
            global $wpdb, $wpdb_nrl;
            if ( ! empty($wpdb_nrl) ) {
                // Use the imports database to get Article Template data
                $wpdb_save = $wpdb;
                $wpdb = $wpdb_nrl;
            }            

            // Query for your custom type
            $post_type_query  = new WP_Query(
                array (  
                    'post_type'      => 'nrl_articles',
                    'post_status'    => 'publish',
                    'posts_per_page' => -1, 
                    'orderby' => $field,
                    'order'   => 'ASC',            )  
            );   
            // We need the array of posts
            $posts_array = $post_type_query->posts;   
            // Create a list with the names of the article types
            if ( empty($index_key) ) {
                // Create an array with a numeric key
                $post_title_array = wp_list_pluck( $posts_array, $field);
            } else {
                // Create an array with the field $key as the key
                $post_title_array = wp_list_pluck( $posts_array, $field, $index_key );
            }

            // Restore $wpdb for this site
            if ( ! empty($wpdb_nrl) ) {
                $wpdb = $wpdb_save;
            }
            return $post_title_array;
        }
        
        static function get_article_data( $id, $type='' ) {
            // Returns data of the specified $type for the Article Template $id
            
            global $wpdb, $wpdb_nrl;
            if ( ! empty($wpdb_nrl) ) {
                // Use the imports database to get Article Template data
                $wpdb_save = $wpdb;
                $wpdb = $wpdb_nrl;
            }
            
            switch ( $type ) {
                case 'post':
                    $data = get_post( (int) $id );
                    break;
                case 'cats':
                    $data = get_the_category( $id );
                    break;
                case 'tags':
                    $data = wp_get_post_tags( $id );
                    break;
                case 'meta':
                    $data = self::get_article_meta_data( $id );
                    break;
            }
            // Restore $wpdb if it has been changed
            if ( ! empty($wpdb_nrl) ) {
                $wpdb = $wpdb_save;
            }
            
            return $data;
        }
    
        static function get_article_meta_data( $id ) {
            // Get meta data for article template with post ID $id
            $meta_data = get_post_meta( $id, 'nrl_meta', true );
            $temp = 1;
            if ( empty($meta_data) ) {
                $meta_data = array(
                    'list_type' => 'sales',
                    'title'     => '',
                    'random'    => array()
                );
            }
            
// For backwards compatibility 12-29-18            
            if ( ! empty($meta_data['post_location_type']) && $meta_data['post_location_type'] == 'CityName' ) {
                $meta_data['post_location_type'] = 'City';
            }   
            
            // LEGACY support
//            if ( empty($meta_data['list_type']) ) {
//                $meta_data['list_type'] = 'sales';
//            }

            foreach ( $meta_data['random'] as $slug => $values ) {
                $new_random = array();
                if ( ! empty($values) && ! isset($values['list']) ) {
                    $new_random['list'] = $meta_data['random'][$slug];
                    $meta_data['random'][$slug] = $new_random;
                }
                if ( empty($meta_data['random'][$slug]['repeat']) ) {
                    $meta_data['random'][$slug]['repeat'] = 'false';
                }
                $temp = 1;
            }

            // LEGACY 9/13/17
//            if ( empty($meta_data['post_location_type']) && isset($meta_data['post_select_type']) ) {
//                $meta_data['post_location_type'] = $meta_data['post_select_type'];
//                if ( empty($meta_data['post_location_value']) && isset($meta_data['post_select_value']) ) {
//                    $meta_data['post_location_value'] = $meta_data['post_select_value'];
//                    unset( $meta_data['post_select_value'] );
//                }
//                $meta_data['post_select_type'] = '';
//            }

            return $meta_data;
        }
        
        static function get_article_author( $user_id ) {
            // Get the user object for $user_id from the imports database
            global $wpdb, $wpdb_nrl;
            if ( ! empty($wpdb_nrl) ) {
                // Use the imports database to get Article Template data
                $wpdb_save = $wpdb;
                $wpdb = $wpdb_nrl;
            }            
            $user = get_user_by( 'ID', $author_id );
            // Restore $wpdb if it has been changed
            if ( ! empty($wpdb_nrl) ) {
                $wpdb = $wpdb_save;
            }            
            return $user;
        }
    
        static function format_listing_page_url( $listing, $type, $site_url='', $path_parts=array() ) {
            // Format the url for the listing page for $listing
            if ( empty($path_parts) ) {
                $path_parts = self::get_listing_path_parts( $listing );
            }
            if ( empty($site_url) ) {
                $site_url = home_url();
            }
//            if ( $listing['ForSale'] == 1 ) {
            if ( $type == 'sales' ) {
                $url = $site_url . '/buyers';
            } else {
                $url = $site_url . '/renters';
            }
            foreach ( $path_parts as $key => $part ) {
                $url .= '/' . self::encode_path_part($key, $part);
            }
            $url .= '/' . self::encode_path_part( 'address', $listing['FullStreetAddress']  . '_mls-' . $listing['ListingId'] );
            return $url;
        }

        static function get_listing_path_parts( $listing ) {
            // Return the parts of the listing page url in an array
            // This is used during import to make sure the county/city/zip pages exist
            $path_parts = array(
                'region'    => 'Northern Virginia',
                'county'    => self::format_county_name($listing['County']),
                'city'      => $listing['City'],
    //            'zip'       => $listing['PostalCode'],
    //                'address'   => $listing['FullStreetAddress'] . '_' . $listing['ListingId']
            );
            return $path_parts;
        }

        static function encode_path_part( $loc, $part ) {
            // URL encoude the path part for location type $loc
    //        $path = urlencode( str_replace(array(',',' '), array('','_'),$part) );
            $path = strtolower( urlencode( str_replace( array(',',' ','/'), array('','-','-'), $part ) ) );
            if ( $loc == 'zip' ) {
                $path .= '_';
            }
            return $path;
        }        
        
        static function format_county_name( $county, $caps='true' ) {
            // $caps tells whether or not to captitalize the word "the" in front of "City of.."
            if ( strpos($county,'County')===false && strpos($county,'City')===false ) {
                return $county . ' County';
            } else if ( substr( $county, -5 ) == ' City' ) {
                $article = ( $caps == 'true' ? 'The' : 'the' );
                return "$article City of " . substr( $county, 0, strlen($county)-5 );
            } else {    
                return $county;
            }
        }
        
//    static function format_property_url($listing) {
//        // Creates the url to the detailed listing for $listing
//        
//        // TEMP: For now, only use the new virtual listings for ForSale listings
//        if ( $listing['ForSale'] == '1' ) {
//            $url = NRL_Listings_Page::format_listing_page_url($listing);
//        } else {
//            $url = self::format_property_virtual_url($listing);
//        }
//        return $url;
//        
//        // Since we are switching to all virtual listing pages, we should always create this type of url
//        $url = NRL_Listings_Page::format_listing_page_url($listing);
//        return $url;
//    }        
    
        static function get_photos($id) {
            // Returns an array of file names for the photos for property $id
            $path = self::get_photo_path($id);
            $pad_id = str_pad($id, 2, "0", STR_PAD_LEFT); // If single digit, left pad with zero
            $pattern = "{$path}image-{$pad_id}-*.*";
            $photos = glob($pattern);
            return($photos);
        }
        
    static function get_photo_path($id) {
        // Directory structure, based on id, is like this: NRL/0010000/10000/
        // Photos for 100 properties are in a directory
        
        if ( empty($id) ) {
            return false;
        }
        if ( empty(self::$options) && class_exists( 'NRL_Util') ) {
            self::$options = NRL_Util::get_options();
        }
//        $pad_id = '000' . $id;
        $str_id = (string) $id;
        if ( $id < 100 ) {
            $p_dir = '00000';
        } else {
            $p_dir = substr($str_id, 0, strlen($id)-2) . '00';
            $p_dir = str_pad( $p_dir, 5, "0", STR_PAD_LEFT);
            $temp = 1;
        }
//        $grp_dir = str_pad(10000*((int)($id/10000)), 6, "0", STR_PAD_LEFT);
        $grp_dir = str_pad(10000*((int)($id/10000)), 7, "0", STR_PAD_LEFT);
        // See if we need to get photos from the imports site
        if ( ! empty(self::$options['remote_db']) && self::$options['remote_db'] == 'true' && ! empty(self::$options['imports_path']) ) {
            $path = self::$options['imports_path'];
        } else {
            $path = ABSPATH;
        }
        
        $photo_dir_2 = $path . 'wp-content/' . NRL_CONTENT_DIR . '/' . $grp_dir . '/' . $p_dir . '/'; 
        
        return $photo_dir_2;
    }

        static function search_val_encode($val){
            $val = str_replace( ' ', '_', $val );
            // Encoded slashes are not accepted by the server, so double encode them
            $val = str_replace('/', '%2F', $val);
            $val = urlencode($val);
            return $val;
        }

        static function search_val_decode($val) {
            $val = str_replace( '+', ' ', $val );
            $val = str_replace( '_', ' ', $val );
            // Fix double encoded slash when setting up the search, here
    //        $val = str_replace('%252F', '%2F', $val);
            return urldecode($val);
        }

    static function get_post_var( $var_name, $array=false ) {
        // Checks for $var_name in $_POST and returns the sanitized value, if any
        if ( filter_has_var(INPUT_POST, $var_name) ) {
            if ( $array ) {
                $value = filter_input(INPUT_POST, $var_name, FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);
            } else {
                $value = filter_input(INPUT_POST, $var_name, FILTER_SANITIZE_STRING);
            }
        } else {
             $value = '';
        }
        return $value;        
    } 

        static function write_log($message, $logfile, $mode='a') {
          // Append to the log file
          if ( $fd = @fopen($logfile, $mode) ) {
    //        $result = fputcsv($fd, array($date, $remote_addr, $request_uri, $message));
            $result = fwrite($fd, $message);
            fclose($fd);

            if( $result > 0 ) {
                $response = array('status' => true);  
            } else {
                // Can't write to the file
                $response = array('status' => false, 'message' => 'Unable to write to '.$logfile.'!');
            }
          } else {
              // Can't open the file
              $response = array('status' => false, 'message' => 'Unable to open file '.$logfile.'!');
          }
          if ( ! $response['status'] && defined('NRL_DEBUG_LOG') && $logfile !== NRL_DEBUG_LOG ) {  // Avoid an infinite loop!
                if ( defined( 'NRL_DEBUG' ) ) {
                    $log = date('Y-m-d H:i:s') . " Error writing log: {$response['message']}\n";
                    self::write_log( $log, NRL_DEBUG_LOG );                
                }          
          }
          return $response;
        }    

        static function error_log($message) {
            // Add the date/time and write $message to error log
            $msg = date('Y-m-d H:i:s') . ' ' . $message . "\n";
            self::write_log($msg, NRL_ERROR_LOG);
            }
        
        static function name_case($string) {
            $word_splitters = array(' ', '-', "O'", "L'", "D'", 'St.', 'Mc', '#', '.', '/');
            $lowercase_exceptions = array('the', 'van', 'den', 'von', 'und', 'der', 'de', 'da', 'of', 'and', "l'", "d'");
            $uppercase_exceptions = array('III', 'IV', 'VI', 'VII', 'VIII', 'IX', 'NW', 'NE', 'SW', 'SE', 'DC');

            $string = strtolower($string);
            foreach ($word_splitters as $delimiter) { 
                $words = explode($delimiter, $string); 
                $newwords = array(); 
                foreach ($words as $word) { 
                    if (in_array(strtoupper($word), $uppercase_exceptions)) {
                        $word = strtoupper($word);
                    } else if (!in_array($word, $lowercase_exceptions)) {
                        $word = ucfirst($word); 
                    }
                    $newwords[] = $word;
                }

        //		if (in_array(strtolower($delimiter), $lowercase_exceptions))
        //			$delimiter = strtolower($delimiter);

                $string = join($delimiter, $newwords); 
            } 
            return $string; 
        }
        
        static function elapsed_time_string($mytime) {
            // $mytime is in seconds
            // Return an array of days, hours, minutes
            $result = array();
            $result['days'] = (int)($mytime / (24*3600));
            $result['hours'] = (int) (($mytime - $result['days']*24*3600) / 3600);
            $result['minutes'] = (int)(($mytime - $result['days']*24*3600 - $result['hours']*3600) / 60);
            return($result);
        }        

        static function get_prop_types() {
            // Returns an array of prop types
            $types = array(
                'Any' => '',
                'Attach/Row Hse' => 'Attach/Row Hse',
                'Back-to-Back'  => 'Back-to-Back',
                'Bed & Breakfast' => 'Bed & Breakfast',
                'Detached' => 'Detached',
                'Double Wide' => 'Double Wide',
                'Duplex' => 'Duplex', 
                'Dwelling w/Rental' => 'Dwelling w/Rental',
                'Garage/Park Space' => 'Garage/Park Space',
                'Garden 1-4 Floors' => 'Garden 1-4 Floors',
                'Hi-Rise 9+ Floors' => 'Hi-Rise 9+ Floors',
                'House of Worship' => 'House of Worship',
                'Mid-Rise 5-8 Floors' => 'Mid-Rise 5-8 Floors',
                'Mobile' => 'Mobile',
                'Multi-Family' => 'Multi-Family',
                'Other' => 'Other',
                'Over Storefront' => 'Over Storefront',
                'Patio Home' => 'Patio Home',
                'Penthouse' => 'Penthouse',
                'Quad' => 'Quad',
                'Rooming House' => 'Rooming House',
                'Semi-Detached' => 'Semi-Detached',
                'Townhouse' => 'Townhouse', 
                'Vacation Home' => 'Vacation Home',
                'Vacation Rental' => 'Vacation Rental',
                'Triplex' => 'Triplex',                  
            );
            return $types;
        }
        
        static function get_request_uri() {
            // Get request uri from $_SERVER['REQUEST_URI']
            // Workaround because filter_input returns NULL on $_SERVER variables on some systems
            // Ref: https://github.com/xwp/stream/issues/254
            if ( filter_has_var( INPUT_SERVER, 'REQUEST_URI' ) ) {
                $url_path  = filter_input( INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL );
            } else {
                if (isset($_SERVER['REQUEST_URI'])) {
                    $url_path  = filter_var( $_SERVER['REQUEST_URI'], FILTER_SANITIZE_URL, FILTER_NULL_ON_FAILURE );
                } else {
                    $url_path = false;
                }
            }        
            return $url_path;
        }

        static function get_page_uri() {
            // Returns the complete page uri
            $url_path = self::get_request_uri();
            if ( $url_path ) {
                $host = filter_var($_SERVER['HTTP_HOST'], FILTER_SANITIZE_URL, FILTER_NULL_ON_FAILURE);
            }
            $scheme = filter_input(INPUT_SERVER, 'REQUEST_SCHEME', FILTER_SANITIZE_URL);
            if ( ! empty($scheme) ) {
                $path = $scheme. "://" . $host . $url_path;
            } else {
                $path = "http://" . $host . $url_path;
            }
            return $path;
        }
        

    } // end class NRL_Common
}  // end if class_exists
