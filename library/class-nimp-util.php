<?php
/**
 * Utility functions for NRL Imports plugin
 * Used on both the front and back end
 * @author Ken Carlson (http://kencarlsonconsulting.com)
 */


        
class NIMP_Util {
    static $options, $options_defaults;
    static $admin_notices = array();
//    static $activation = false;

    static function setup() {
        // Come here when the plugin is run
//date_default_timezone_set('America/New_York');
//$temp = strftime("%H:%M");  // date("H:i",  microtime(true));
//$time = explode(':', date("H:i"));
        
//    self::setup_db_access();
    self::$options = self::get_options();

// For adding featured image to listing posts

        // Activation hook
        register_activation_hook( NIMP_FILE, array('NIMP_Util','activation') );

//        add_action('wp_head',array('NIMP_MLS_Util','wpautop_fix'));  // This is handled instead in init_actions() below
        add_action('init', array('NIMP_Util', 'setup_db_access'));
//        add_action( 'init', array('NIMP_Posts','create_post_type') );
        
        // See if we are running a cron job
//        if ( filter_has_var(INPUT_GET, 'nrl_imports') ) {
//            // This is a cron job
//            add_action('init', array('NIMP_MLS_Util', 'run_cron_job'), 99);
//        }
        
        // load plugin textdomain for languages
//        add_action('plugins_loaded', array('NIMP_Util', 'init_languages'),1);
//        add_shortcode('nrl-summary-listing', array('NIMP_Util','process_summary_listing'), 1);        
//        add_shortcode('nrl-detail-listing', array('NIMP_Util','process_detail_listing'), 1);
        // Is the nrl-recent-listings shortcode still used anywhere?
//        add_shortcode('nrl-listings', array('NIMP_Display','process_listings'), 1);
//        add_shortcode('nrl_listings', array('NIMP_Display','process_listings'), 1);
////        add_shortcode('nrl-recent-listings', array('NIMP_Util','process_recent_listings'), 1);
//        add_shortcode('nrl-recent-listings', array('NIMP_Display','process_listings'), 1);
//        add_shortcode('nrl-zip-page', array('NIMP_Listings_Page','process_zip_page'), 1);
//        add_shortcode('NRL', array('NIMP_Display','process_listing_page'), 1);        
//        add_shortcode('nrl_search', array('NIMP_Display','process_search_form'), 1);     
//        add_shortcode('nrl_favorites', array('NIMP_Display','process_favorites'), 1);     
//        add_shortcode('nrl-favorites', array('NIMP_Display','process_favorites'), 1);     
//        if ( ! defined('DSIDXPRESS_OPTION_NAME')) {
//            add_shortcode('idx-listings', array('NIMP_Display','process_listings'), 1);        
//        } 
//        add_shortcode('nrl-saved-searches', array('NIMP_Display','process_saved_searches'), 1); 
//        add_shortcode('nrl_saved_searches', array('NIMP_Display','process_saved_searches'), 1); 
////        add_shortcode('nrl_parent_subpages', array('NIMP_Util','process_list_parent_subpages'), 1); 
//        
//        // Shortcode used to show suggestions on listing pages 
//        add_shortcode('nrl_listing_suggestions', array('NIMP_Display','process_listing_suggestions'), 1); 
//        
//        add_action( 'widgets_init', array('NIMP_Util','register_custom_sidebars') );        
//        add_action('login_head', array('NIMP_Util', 'custom_login_logo'));
////        NIMP_Display::setup();

        if ( is_admin() ) {
            // Set up admin actions
            add_action('admin_enqueue_scripts', array('NIMP_Util','enqueue_admin_scripts'));
            // Add scripts for Zozo tabs in the admin area
            add_action( 'admin_enqueue_scripts', array('NIMP_Util','enqueue_tabs_scripts') );

            add_action('admin_menu', array('NIMP_Admin', 'register_options_page'));

            add_action('admin_init', array('NIMP_Admin', 'initialize_options'));
//            add_action('admin_init', array('NIMP_Util', 'shortcode_button_init'));
            add_action('admin_head', array('NIMP_Util', 'header_custom_admin') );
            add_action('admin_notices', array('NIMP_Util', 'admin_notices'));

            add_action('wp_ajax_nimp_setup', array('NIMP_Admin', 'ajax_setup') );
            add_action('wp_ajax_nimp_tools', array('NIMP_Admin', 'ajax_tools') );
            add_action('wp_ajax_nimp_debug', array('NIMP_Admin', 'ajax_debug') );
            if ( class_exists("NIMP_Bright") ) {
                add_action('wp_ajax_nimp_convert', array('NIMP_Bright', 'ajax_convert') );
            }
            add_action('wp_ajax_save_polygon', array('NIMP_Admin', 'ajax_polygon') );
            add_action('wp_ajax_delete_polygon', array('NIMP_Admin', 'ajax_polygon') );
            
// Not needed for the imports site..
//            add_action('wp_ajax_nrl_shortcode_builder', array('NIMP_Admin', 'ajax_shortcode_builder') );        
//            add_action('wp_ajax_nrl_generate_post', array('NIMP_Posts', 'ajax_generate_post') );
            
            // Code for testing generation of listing page
//            add_action('wp_ajax_nrl_generate_listing_page', array('NIMP_Util', 'ajax_generate_listing_page') );        

            // AJAX actions happen in the admin context, so even though this is for the front end, these goes here
//            add_action('wp_ajax_nrl_manage_searches', array('NIMP_Display', 'ajax_manage_searches') );        
//            add_action('wp_ajax_nrl_delete_searches', array('NIMP_Display', 'ajax_delete_searches') );        
//            add_action('wp_ajax_nrl_toggle_favorite', array('NIMP_Display', 'ajax_toggle_favorite') );        

//            add_action('admin_enqueue_scripts', array('NIMP_Util', 'enqueue_admin_scripts'));

//            add_action('admin_footer', array('NIMP_Util', 'admin_footer'));

            // adds "Settings" link to the plugin action page
            add_filter('plugin_action_links', array('NIMP_Util', 'plugin_action_links'), 10, 2);
            
            // Code for adding Agent Author to user profile
//            add_action( 'show_user_profile', array('NIMP_Admin', 'my_extra_user_fields')  );
//            add_action( 'edit_user_profile', array('NIMP_Admin','my_extra_user_fields') );
//            add_action( 'personal_options_update', array('NIMP_Admin','save_my_extra_user_fields') );
//            add_action( 'edit_user_profile_update', array('NIMP_Admin','save_my_extra_user_fields') );
            
            // Add footer scripts for edit pages
//            add_action('admin_footer-post.php', array('NIMP_Util', 'footer_scripts_admin'));
            // Add action to duplicte article templates
            add_action( 'admin_action_nrl_duplicate_post_as_draft', array('NIMP_Admin', 'duplicate_post_as_draft') );            
            add_filter( 'post_row_actions', array('NIMP_Admin','duplicate_post_link'), 10, 2 );            
           
            // Add an action for use in publishing or trashing draft generated posts
//            add_action( 'admin_action_nrl_admin_action', array('NIMP_Posts', 'nrl_admin_action' ) );
            
            // When we delete a post, if it is a generted post we need to remove it from the table of generated articles
//            add_action( 'before_delete_post', array('NIMP_Util', 'delete_post') );
            
// KPC TEST
            add_filter('views_edit-post', array('NIMP_Admin','edit_post_views'));
        } else {
            // Check for cron job
            add_action('init', array('NIMP_Util', 'init_check_cron'), 12 ); // Do this after init actions with default priority
            // Set up front end actions
//            add_action('wp_enqueue_scripts', array('NIMP_Util','enqueue_scripts'));
//            add_action('wp_head', array('NIMP_Util', 'header_custom') ); 
//            add_action( 'wp_head', array( 'NIMP_Display', 'add_meta_tags'), 2 );            
//            add_action('wp_footer', array('NIMP_Display', 'footer_scripts'));
            
        }

        date_default_timezone_set('America/New_York');
         // Add sitemaps to Better WordPress Sitemaps plugin
        add_filter( 'bwp_gxs_external_sitemaps', array('NIMP_Util', 'add_external_sitemaps') ); 

    }   // end setup()
    
    static function activation() {
        // Run when the plugin is activated
        global $wpdb;
        
        ob_start();
        // Register custom tables, since that won't have run yet when we get here
        self::setup_db_access();

        // Create the settings table if necessary
        require_once NIMP_PATH . 'library/class-nimp-install.php';
        NIMP_Install::check_settings();

        // Make sure that the photo dir exists
        $photo_dir = get_home_path() . 'wp-content/' . NRL_CONTENT_DIR . '/';
        if ( ! file_exists($photo_dir) ) {
            $temp = 1;
            if ( ! mkdir($photo_dir, 0755) ) {
                die('Failed to create folder...');
            }
        }

        // See if the table structures need to be updated
        self::$options = self::get_options();
        $defaults = NIMP_Util::set_defaults();
        if ( count(self::$options) < count($defaults) ) {
            self::$options = array_merge( $defaults, self::$options );
            self::update_options( self::$options, 'NIMP_Util::activation() set defaults');
        }
        if ( empty(self::$options['table_version']) || self::$options['table_version'] !== NIMP_TABLE_VERSION ) {
            require_once NIMP_PATH . 'library/class-nimp-install.php';
//            NIMP_Install::check_tables();
            NIMP_Install::create_tables();
            self::$options['table_version'] = NIMP_TABLE_VERSION;
            self::update_options( self::$options, 'NIMP_Util::activation() update table structures');
        }
        
        // See if anything needs to be updated
        $temp = 1;
        if ( empty( self::$options['build_version']) || self::$options['build_version'] < NIMP_BUILD ) {
            // See if anything needs to be updated...
            
            // Version specific updates
//            if ( self::$options['build_version'] < 22 ) {
//            
               
//            }
            
            // Update the build number in Options
            self::$options['build_version'] = NIMP_BUILD;
            self::update_options( self::$options );
        }
        
        // There should not be any output unless there was an error...
        $text = ob_get_clean();
        if ( ! empty($text) ) {
            NIMP_Util::error_log( "Error on activation: $text\n" );
        }
        
    }   // end function activation()
    
    
    static function setup_db_access() {
        // This is for the imports site -- always use $wpdb
        global $wpdb; // $wpdb_nrl;
//        $wpdb_nrl = $wpdb;
        
        // Register custom table in globals
        $wpdb->nrl_settings = "{$wpdb->prefix}nrl_settings";
        $wpdb->nrl_fields_sales = "{$wpdb->prefix}nrl_fields_sales";
        $wpdb->nrl_fields_rentals = "{$wpdb->prefix}nrl_fields_rentals";
        $wpdb->nrl_lookup = "{$wpdb->prefix}nrl_lookup_mls";
        $wpdb->nrl_properties_sales = "{$wpdb->prefix}nrl_properties_sales";
        $wpdb->nrl_properties_rentals = "{$wpdb->prefix}nrl_properties_rentals";
        $wpdb->nrl_cities = "{$wpdb->prefix}nrl_cities";
        $wpdb->nrl_media = "{$wpdb->prefix}nrl_media";
        
        // This table is for the display plugin
//        $wpdb->nrl_articles = "{$wpdb->prefix}nrl_articles";
        
        // Add another init action
        if ( ! defined('NIMP_DEV_USER') ) {
            $user_ID = get_current_user_id();
            $temp = 1;
            if ( ($user_ID == NIMP_DEV_USER_ID2) || ( strpos(NIMP_URL, 'dev.') !== false && $user_ID == NIMP_DEV_USER_ID ) || 
//                    strpos(NIMP_URL,'localhost') !==false || strpos(NIMP_URL,'test.') !==false ) {
//                    strpos(NIMP_URL,'test.') !==false ) {
                ( strpos(NIMP_URL, 'imports.') !== false && $user_ID == 2 ) || strpos(NIMP_URL,'test.') !==false ) {
//                ( strpos(NIMP_URL, 'imports.') !== false ) || strpos(NIMP_URL,'test.') !==false ) {
                    define('NIMP_DEV_USER', true);
//                require_once NIMP_PATH . 'library/class-test.php';
            } else {
                define('NIMP_DEV_USER', false);
//                define('NIMP_DEV_USER', true);
            }
        } else {
            $temp = NIMP_DEV_USER;
            $temp = 1;
        }
            
    } 

    static function init_check_cron() {
        
        // See if we are running a cron job
        $url_path = NRL_Common::get_request_uri();  //self::get_request_uri();
        if ( strpos( $url_path, '/nrl_cron937/imports') !== false ) {
            // This function will exit the script:
            NIMP_MLS_Util::run_cron_job();
        }
        return;
    }
    
    static function init_actions() {
        
        // See if we are running a cron job
//        $url_path = NRL_Common::get_request_uri();  //self::get_request_uri();
//        if ( strpos( $url_path, '/nrl_cron937/imports') !== false ) {
//            // This function will exit the script:
//            NIMP_MLS_Util::run_cron_job();
//        }
        
        // init actions for the front end

        NIMP_Display::setup();
        
        // Set up virtual page sections
        add_rewrite_rule( NIMP_LISTINGS_DIR . '/(.*)/*(.*)$', 'index.php?pagename=' . NIMP_LISTINGS_DIR, 'top' );  
        add_rewrite_rule( NIMP_RENTALS_DIR . '/(.*)/*(.*)$', 'index.php?pagename=' . NIMP_RENTALS_DIR, 'top' );
        
        // Property Management and Sales local sections have a different template page for subdivisions
        add_rewrite_rule( NIMP_PROP_MANAGE_DIR . '/([^/]*)/*$', 'index.php?pagename=' . NIMP_PROP_MANAGE_DIR, 'top' );          
        add_rewrite_rule( NIMP_PROP_MANAGE_DIR . '/([^/]+)/([^/]+)/*$', 'index.php?pagename=' . NIMP_PROP_MANAGE_DIR . '/city', 'top' );
        add_rewrite_rule( NIMP_PROP_MANAGE_DIR . '/([^/]+)/([^/]+)/([^/]+)/*$', 'index.php?pagename=' . NIMP_PROP_MANAGE_DIR . '/city/subdivision', 'top' );

        add_rewrite_rule( NIMP_PROP_SELLERS_DIR . '/([^/]*)/*$', 'index.php?pagename=' . NIMP_PROP_SELLERS_DIR, 'top' );  
        add_rewrite_rule( NIMP_PROP_SELLERS_DIR . '/([^/]+)/([^/]+)/*$', 'index.php?pagename=' . NIMP_PROP_SELLERS_DIR . '/city', 'top' );  
        add_rewrite_rule( NIMP_PROP_SELLERS_DIR . '/([^/]+)/([^/]+)/([^/]+)/*$', 'index.php?pagename=' . NIMP_PROP_SELLERS_DIR . '/city/subdivision', 'top' );  
        
        // Rewrite rules for virtual For Sale listing pages
//        add_rewrite_rule( 'buyers/northern-virginia/(.*)/summary/*$', 'index.php?pagename=templates/listing-summary-template', 'top' );  
        add_rewrite_rule( 'buyers/[Nn]orthern-[Vv]irginia/(.*)_[a-zA-Z]{2}[0-9]+/*$', 'index.php?pagename=templates/listing-full-template', 'top' );
        add_rewrite_rule( 'buyers/[Nn]orthern-[Vv]irginia/(.*)/(.*)/[0-9]{5}_/*$', 'index.php?pagename=templates/listing-zip-template', 'top' );  
        add_rewrite_rule( 'buyers/[Nn]orthern-[Vv]irginia/(.*)/gallery/*$', 'index.php?pagename=templates/listing-gallery-template', 'top' );   
//        add_rewrite_rule( 'buyers/northern-virginia/(.*)/full/*$', 'index.php?pagename=templates/listing-full-template', 'top' );  
        
        // Rewrite rules for virtual rental listing pages
        add_rewrite_rule( 'renters/[Nn]orthern-[Vv]irginia/(.*)_[a-zA-Z]{2}[0-9]+/*$', 'index.php?pagename=templates/listing-full-template', 'top' );
        add_rewrite_rule( 'renters/[Nn]orthern-[Vv]irginia/(.*)/(.*)/[0-9]{5}_/*$', 'index.php?pagename=templates/listing-zip-template', 'top' );         
        
        
        global $wp_rewrite;
        $wp_rewrite->flush_rules( false );
        // Later, do this only on activation?

        if ( ! NIMP_DEV_SITE ) {
            // Add custom RSS feed
            add_feed('properties', array('NIMP_Display','custom_rss_feed'));            
        }
        
        // See if this is a listings page
        $url = NRL_Common::get_page_uri();
        if ( stripos($url, '/buyers/northern-virginia') || stripos($url, '/renters/northern-virginia') ) {
            // Add js for the maps to work
//            wp_enqueue_script('nrl_maps', plugins_url('/nrl-maps.js', __FILE__), array('jquery'), NIMP_BUILD);
            // Load Google Maps API
//            add_action('wp_footer', array('NIMP_Display', 'load_map_script'));
            
            // Don't let WP muck with the html
            remove_filter( 'the_content', 'wpautop' );
            remove_filter( 'the_excerpt', 'wpautop' );
            // Tell WP to allow the data attribute
//            add_filter('wp_kses_allowed_html', array('NIMP_Util','kses_filter_allowed_html' ), 25, 2);
//            self::allow_data_attributes();
            error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
        }

    }   // end function init_actions()

    static function register_custom_sidebars() {
        // Register custom sidebars used for the listing pages
        register_sidebar(
            array (
                'name' => __( 'Listing Page Sidebar', NIMP_TEXT_DOMAIN ),
                'id' => 'nrl-listing-page-side-bar',
                'description' => __( 'Custom Sidebar for Listing Pages', NIMP_TEXT_DOMAIN ),
                'before_widget' => '<div class="widget-content">',
                'after_widget' => "</div>",
                'before_title' => '<h3 class="widget-title">',
                'after_title' => '</h3>',
            )
        );        
        register_sidebar(
            array (
                'name' => __( 'Listing Page Bottom', NIMP_TEXT_DOMAIN ),
                'id' => 'nrl-listing-page-bottom',
                'description' => __( 'Content for the bottom of Listing Pages', NIMP_TEXT_DOMAIN ),
                'before_widget' => '<div class="widget-content">',
                'after_widget' => "</div>",
                'before_title' => '<h3 class="widget-title">',
                'after_title' => '</h3>',
            )
        ); 
    }
    
    static function init_languages() {
        if (function_exists('load_plugin_textdomain')) {
            load_plugin_textdomain(NIMP_TEXT_DOMAIN, false, NIMP_PATH . '/languages');
        }
    }
    
    static function filter_depreciated_trigger_error( $val ) {
        return false;
    }
    

    static function add_external_sitemaps() {
        // Add custom sitemaps to Better WordPress Google XML Sitemaps plugin sitemap index
        // Source: http://betterwp.net/wordpress-plugins/google-xml-sitemaps/
        $fname_base = get_home_path() . "nrl_sitemap_";
        $files = glob( $fname_base . '*.xml' );
//        $files = array(
//            'nrl_sitemap_sales.xml',
//            'nrl_sitemap_local.xml',
//        );
        $sitemaps = array();
        $result = true;
        foreach ( $files as $file_path ) {
//            $file_path = ABSPATH . $file_name;
            if ( file_exists( $file_path ) ) {
                $path_parts = explode( '/', $file_path );
                $file_name = $path_parts[count($path_parts)-1];
                $mod_date = date ("Y-m-d", filemtime( $file_path ));
                $sitemaps[] = array(
                        'location' => home_url( $file_name ),
                        'lastmod'  => $mod_date
                    );
            } else {
                $result = false;
            }            
        }
        return $sitemaps;
    }    
    
    static function shortcode_button_init() {
        // Set up the shortcode builder button for the edit screen
        // Make sure that the user has permission
        if ( current_user_can('edit_posts') && current_user_can('edit_pages') && get_user_option('rich_editing') == 'true') {
            //Add a callback to regiser our tinymce plugin   
            add_filter("mce_external_plugins", array('NIMP_Util', 'register_tinymce_plugin')); 

            // Add a callback to add our button to the TinyMCE toolbar
            add_filter('mce_buttons', array('NIMP_Util', 'add_tinymce_button'));
        }
        error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);

       
         // Add sitemaps to Better WordPress Sitemaps plugin
        add_filter( 'bwp_gxs_external_sitemaps', array('NIMP_Util', 'add_external_sitemaps') ); 
    }
    
    static function register_tinymce_plugin($plugin_array) {
//        $plugin_array['nrl_button'] = NIMP_URL . 'library/shortcode_button.js';
        $plugin_array['nrlshortcode'] = plugins_url( '/shortcode_button2.js', __FILE__ );
        return $plugin_array;        
    }
    
    static function add_tinymce_button($buttons) {
        $temp = 1;
        //Add the button ID to the $button array
//        $buttons[] = "NRL Shortcode";
        array_push( $buttons, "|", "nrlshortcode" );
        return $buttons;        
    }

    static function header_custom_admin() {
        // Header actions for admin 
        self::header_custom();
        // See if we are on an edit page or post screen..
        if ( filter_has_var(INPUT_SERVER, 'REQUEST_URI') ) {
            $request_uri = filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL);
        // Workaround because filter_input returns NULL on $_SERVER variables on some systems
        // Ref: https://github.com/xwp/stream/issues/254
        } else if ( isset($_SERVER['REQUEST_URI']) ) {
            $request_uri = filter_var($_SERVER['REQUEST_URI'], FILTER_SANITIZE_URL, FILTER_NULL_ON_FAILURE);
        } else {
            // Can't find uri -- Create a fake uri to always load nrlDefaults
            $request_uri = 'something/post-new.php';
        }
        if ( strpos($request_uri, 'post-new.php') || strpos($request_uri, 'post.php') ) {
            $search_options = NRL_Common::get_nrl_setting('search_fields'); // get_option(NIMP_SEARCH_OPTION_NAME);
            $defaults = array();
            foreach ( $search_options as $fname => $props ) {
                if ( $props['default'] !== '' ) {
                    $defaults[$fname] = $props['default'];
                }
            }
?>
<script>
   var nrlDefaults = <?php echo json_encode($defaults, JSON_UNESCAPED_SLASHES); ?>
   
   console.log("nrlDefaults has been initialized by NRL Imports.\n");
</script>
<?php   }   // end if strpos...
    }   // end function header_custom_admin()
    
    static function header_custom() {
        $rss_url = home_url() . '/feed/properties/';
        echo '<link rel="alternate" type="application/rss+xml" title="Nesbitt Realty Properties Feed" href="' .$rss_url . '" />' . "\n";
//        if ( empty(self::$options) ) {
//            self::$options = self::get_options();
//        }
//        $ss_height = self::$options['slideshow_height'];
//        echo "<style>#nrl_slideshow {height: $ss_height;}</style>";
//        echo '<script type="text/javascript">nrlMaps=[];</script>' . "\n";

    }

    
    static function custom_login_logo() {
        echo '<style type="text/css">
            h1 a { background-image:url(' . NIMP_URL . '/images/cropped-n-site-icon-1-300x300.png) !important; }
        </style>';
    }

    static function admin_notices() {
        // Displays admin notices, if any, at top of admin screen
        // Called by hook to WP admin_notices
         
//        // The notice will appear the next time the WP 'admin_notices' action occurs
//        self::get_global_options();
        $temp = 1;
        if ( ! empty(self::$admin_notices) ) {
            foreach ( self::$admin_notices as $notice ) { 
?>     <div class="notice notice-warning">
            <p>
                <?php echo $notice; ?>
            </p>
        </div>
<?php       }
        }
    }   // end function admin_notices    
    
    static function set_admin_notice( $msg ) {
        self::$admin_notices[] = $msg;
    }

static function enqueue_scripts() {
    wp_enqueue_style('nrl_styles', plugins_url('/styles_nrl20.css', __FILE__), false, NIMP_BUILD);
//    wp_enqueue_style('nrl_listing_styles', plugins_url('/styles_listing_format.css', __FILE__), false, NIMP_BUILD);
    wp_enqueue_script('nrl_scripts', plugins_url('/scripts7.js', __FILE__), array('jquery'), NIMP_BUILD);
//    wp_enqueue_script('nrl__listing_scripts', plugins_url('/nrl_listing_scripts.js', __FILE__), array('jquery'), NIMP_BUILD);
//    wp_enqueue_script('cycle', plugins_url('/jquery.cycle.lite.js', __FILE__), array('jquery'), NIMP_BUILD);
    wp_enqueue_script('cycle', plugins_url('/jquery.cycle.all.js', __FILE__), array('jquery'), NIMP_BUILD);
    wp_enqueue_script('nrl_maps', plugins_url('/nrl-maps1d.js', __FILE__), array('jquery'), NIMP_BUILD);
    
    //Load js file for AJAX actions
//    wp_enqueue_script('nrl_admin_ajax', plugins_url('/nrl_admin_ajax.js', __FILE__), array('jquery'), NIMP_BUILD);
    wp_localize_script('nimp_ajax', 'NIMP_admin_AJAX', array(
        'ajaxurl' => admin_url('admin-ajax.php'),
        'nonce' => wp_create_nonce('nrl-ajax-nonce'),
//            'postID' => $post->ID, 
        )
    );    
}

static function enqueue_admin_scripts() {
    wp_enqueue_style('nimp_admin_styles', plugins_url('/styles-admin4.css', __FILE__), false, NIMP_BUILD);

// KPC temp for testiong    
//    wp_enqueue_style('nrl_styles', plugins_url('/styles_nrl8.css', __FILE__), false, NIMP_BUILD);
    
    wp_enqueue_script('nimp_admin_scripts', plugins_url('/nimp-scripts-admin1.js', __FILE__), array('jquery','jquery-ui-core','jquery-ui-tabs'), NIMP_BUILD);
    wp_enqueue_script( 'jquery-ui-core' );
    wp_enqueue_script( 'jquery-ui-tabs' );      // For settings tabs    
    // Load theme for jqueary ui tabs
    global $wp_scripts;
    // get registered script object for jquery-ui
    $ui = $wp_scripts->query('jquery-ui-core');
     // tell WordPress to load the Smoothness theme from Google CDN
    $protocol = is_ssl() ? 'https' : 'http';
    $url = "$protocol://ajax.googleapis.com/ajax/libs/jqueryui/{$ui->ver}/themes/smoothness/jquery-ui.min.css";
    wp_enqueue_style('jquery-ui-smoothness', $url, false, null);
    
    //Load js file for admin AJAX actions
//    wp_enqueue_script('nimp_admin_ajax', plugins_url('/nrl_admin_ajax.js', __FILE__), array('jquery'), NIMP_BUILD);
    wp_localize_script('nimp_admin_ajax', 'NIMP_admin_AJAX', array(
        'ajaxurl' => admin_url('admin-ajax.php'),
        'nonce' => wp_create_nonce('nimp-ajax-nonce'),
//            'postID' => $post->ID, 
        )
    );
}

    static function enqueue_tabs_scripts() {
        // Enqueue scripts to support zozo tabs (needed only in the admin area)
        $url = plugin_dir_url( __FILE__ );
        $ind = strpos($url, '/plugins/');
        $url = substr($url, 0, $ind) . '/plugins/ztabs/';
        $temp = 1;
        // Zozo Accordion styles and scripts
        wp_enqueue_style( 'zozo-accordion-styles', $url . 'zozo-accordion-files/css/zozo.accordion.min.css' );
        wp_enqueue_script( 'zozo-accordion-scripts', $url . 'zozo-accordion-files/js/zozo.accordion.min.js' );

        // Zozo Tabs styles and scripts
        wp_enqueue_style( 'zozo-tabs-styles', $url . 'zozo-tabs-files/css/zozo.tabs.min.css' );
        wp_enqueue_style( 'zozo-tabs-flat-styles', $url . 'zozo-tabs-files/css/zozo.tabs.flat.min.css' );
        wp_enqueue_script( 'jquery' );
    //    wp_enqueue_script( 'jquery-easing', ZT_URL . 'zozo-tabs-files/js/jquery.easing.min.js' );  // loaded by WPL 28 theme
        wp_enqueue_script( 'zozo-tabs-scripts', $url . 'zozo-tabs-files/js/zozo.tabs.min.js' );
    }
    
//    static function process_summary_listing_OLD($atts) {
//        // Return summary listing(s)
//
//		// Extract shortcode atts
//		extract( shortcode_atts( array(
//					'count'		 => 5,
////					'redirect'	 => '',
////					'hidden'	 => '',
////					'email_to'	 => '',
//					), $atts ) );
//        ob_start();
//        self::display_listings($count, true);
//        $output = ob_get_clean();
//        return $output;
//    }

//    static function process_detail_listing_NOT_USING($atts) {
//        // Return detail listing(s)
//
//		// Extract shortcode atts
//		extract( shortcode_atts( array(
//					'id'		 => 1,
////					'redirect'	 => '',
////					'hidden'	 => '',
////					'email_to'	 => '',
//					), $atts ) );
////        ob_start();
//        $output = NIMP_Display::format_detail_listing($id);
////        $output = ob_get_clean();
//        return $output;
//    }   // end process_detail_listing()

//    static function process_recent_listings($atts) {
//        // Return recent listings
//		// Extract shortcode atts
//		extract( shortcode_atts( array(
//					'count'	    => 10,
//					'format'    => 'summary',
//					), $atts ) );        
//        
//        return NIMP_Display::get_recent_listings($count, $format);
//    }
    
    static function process_list_parent_subpages($atts) {
        // List subpages of parent page, excluding current page
        $post_id = get_the_ID();
        $parent_id = wp_get_post_parent_id( $post_id );
        $result = '<div class="nrl_parent_subpages">' . "\n" 
                . wp_list_pages(array('child_of'=>$parent_id, 'depth'=>1, 'exclude'=>$post_id, 'title_li'=>'', 'echo'=>false)) . "\n"
                . "</div>\n";        
        return $result;
    }
    
//    static function get_options_old() {
//        if (!isset(self::$options_defaults))
//            self::$options_defaults = self::set_defaults();
//
//        // Load options
//        self::$options = get_option(NIMP_OPTIONS_NAME);
//        if (!self::$options) {
//            // Options table entry does not exist, so create it
//            update_option(NIMP_OPTIONS_NAME, self::$options_defaults);
//            self::$options = get_option(NIMP_OPTIONS_NAME);
//        } else if (count(self::$options) < count(self::$options_defaults)) {
//            // Add missing elements from the default form options array
//            self::$options = array_merge(self::$options_defaults, self::$options);
//        }
//        return self::$options;
//    }

    static function get_options() {
        // Load plugin options
        $options = get_option( NIMP_OPTION_NAME );
        if ( ! $options) {
            // Options table entry does not exist, so create it
            $defaults = self::set_defaults();
            update_option( NIMP_OPTION_NAME, $defaults );
            $options = $defaults;
        } // else if (count($options) < count($defaults)) {
//            // Add missing elements from the default options array
//            $options = array_merge($defaults, $options);
////            update_option(NIMP_OPTIONS_NAME, $options);
//            self::update_options( $options, 'NIMP_Util::get_options() add new defaults' );
//        }
        
//        // See if the table structures need to be updated
//        if ( empty($options['table_version']) || $options['table_version'] !== NIMP_TABLE_VERSION ) {
//            require_once NIMP_PATH . 'library/class-nimp-install.php';
//            NIMP_Install::check_tables();
//            $options['table_version'] = NIMP_TABLE_VERSION;
//            self::update_options( $options, 'NIMP_Util::get_options() update table structures');
//        }
         
//        if ( empty($options['factoid_text'])) {
//            $options['factoid_text'] = $defaults['factoid_text'];
//        }

        return $options;
    }
    
    static function update_options( $new_options, $context='' ) {
        // $context gives info about the calling function, for debugging
        // This is left here for backwards compatibility..
        $result = update_option( NIMP_OPTION_NAME, $new_options );
        if ( ! $result ) {
            // Try clearing the option cache so that the option can be updated
            wp_cache_delete ( 'alloptions', 'options' );
            $result = update_option( NIMP_OPTION_NAME, $new_options );
            if ( ! $result ) {
                $msg = 'Failed to update option: ' . NIMP_OPTION_NAME;
                if ( ! empty($context) ) {
                    $msg .= " in $context";
                }
                NIMP_Util::error_log( $msg );                
            }
        }

       return $result;
    }
    
    static function update_wp_option( $option_name, $new_options, $context='' ) {
        // Update the given WP option
        // If there is an error, write a message to the error log including the context info
        // This is a more general version of update_options()
        $result = update_option( $option_name, $new_options );
        if ( ! $result ) {
            // Try clearing the option cache so that the option can be updated
            wp_cache_delete ( 'alloptions', 'options' );
            $result = update_option( $option_name, $new_options );
            if ( ! result ) {
                $msg = 'Failed to update option: ' . $option_name;
                if ( ! empty($context) ) {
                    $msg .= " in $context";
                }
                NIMP_Util::error_log( $msg );                
            }
        }
       return $result;        
    }
    
    static function activation_updates_NOT_USED() {
        // Activation updates that need to be run when the options are first loaded after activation
        
        // Version specific updates
        $temp = 1;
        if ( self::$options['build_version'] < NIMP_BUILD ) {
            // See if anything needs to be updated...
            
            if ( self::$options['build_version'] < 22 ) {
                // Add a role to the Agent Authors
                $args = array(
                    'meta_key'     => 'nrl_agent_author',
                    'meta_value'   => 'true'
                );
                $users = get_users( $args );
                $temp = 1;
                foreach ( $users as $user ) {
                    $user->add_cap('nrl_edit_schedule');
                }                
            }

            // Update the build number in Options
            self::$options['build_version'] = NIMP_BUILD;
//            self::update_options(self::$options);
        }
        self::$activation = false;
        
    }   // end function activation_updates()

	static function plugin_action_links( $links, $file ) {
            //Static so we don't call plugin_basename on every plugin row.
            static $this_plugin;
            if ( ! $this_plugin )
                    $this_plugin = plugin_basename( NIMP_FILE );

            if ( $file == $this_plugin ) {
                    $settings_link = '<a href="plugins.php?page=' . plugin_basename(NIMP_FILE) . '">' . __( 'Settings', NIMP_TEXT_DOMAIN ) . '</a>';
                    array_unshift( $links, $settings_link ); // before other links
            }
            return $links;
	} // end function plugin_action_links    
    
    static function set_defaults() {
        $admin_email = get_option('admin_email');
//        $factoid_text = array(
//            'average_age'   => "The average age of {Property_Type} on the market in {Factoid_Source} is {average_age} years.",
//            'price_range'   => "Properties range from {price_range}.",  // "{price_low} to {price_high}"
//            'bed_counts'    => "Properties in {Factoid_Source} typically have {bed_counts} bedrooms.", 
//                // "{bed_count_low} to {bed_count_high}" or just a single bed count if they are equal
//            'average_living'    => "The average above grade living area in {Factoid_Source} is {average_living}.",
//            'median_built'  => "Many were built in {median_built}.",
//            'popular_style' => "The most popular style of property on the market today is {popular_style}.",
//            'basements'     => "Properties in {Factoid_Source} typically {basements} have basements.",
//            'garages'       => "{Factoid_Source} homes typically {garages} have garages.",
//            'remark_quip'   => "Agents are saying {remark_quip}.", //Remark Quip is the last sentence of a listing. There should be 1 to 4 Remark_Quips from different listings
//            'community_fee_includes'  => "Included in the association fees, residents at {Factoid_Source} enjoy {community_fee_includes}.", // see CommunityFeeIncludes
//            'transportation'    => "Commuters from {Factoid_Source} have the benefit of {transportation}.",  // see Transportation field
//            'suggestion'    => "You might want to compare {Factoid_Source} to {suggestion}.",  //Randomly grab 1 of of the suggestions from the first listing. 
//            'elementary_school' => "Residents here are served by {elementary_school}.",
//            'middle_school' => "Middle school children at {Factoid_Source} attend {middle_school}.",
//            'high_school'   => "{high_school} serves {Factoid_Source}."
//        );        
        self::$options_defaults = array(
//            'setup_complete'        => false,
            'build_version'         => '1',
            'table_version'    => '3',                  // Table structure verison -- this changes if ANY of the table structures are changed
            'import_limit'  => 150,                     // How many properties to attempt to import in one run
            'picture_limit' => 30,
            'allow_cron'    => true,
            'suspend_imports'   => false,
            'suspend_imports_after_pass'    => false,   // Suspend imports when the current import pass is finished
            'notification_email'    => $admin_email,
            
// MRIS login info            
            'url'   =>  'http://csrets.mris.com:6103/platinum/login',
            'user'  =>  '3237119',
            'pwd'   =>  'vap2ubpSuswevaCr6cravpChu',
            'agent' =>  'Bright RETS Application/1.0',
//            'unique'        =>  'ListingKey',
//            'user'  =>  '3082558',
//            'pwd'   =>  '855177',            
//            'initial_import'    => true,
//            'new_pass'          => true,
//            'added'             => 0,
//            'updated'           => 0,
//            'photos_added'      => 0,
//            'remaining'         => 0,
            
//            'rental_zips'       => array(),            
//            'show_empty'    => 'false', // Show empty or zero fields (overrides field settings) -- used for testing
//            'search_results_max'    => 250,     // Max number of properties to retrieve for a search
//            'listings_per_page'     => 25,       // Number of listings per page for search results pages
//			'slideshow_height'		=> '431.5833px',  // For main site
//            'factoid_text'          => $factoid_text,
//            'site_type'             => 'main',   // Main or sub-site?
//            'main_url'              => '',  // URL of main site, used for property listings and searches on sub-sites
//            'remote_db'             => 'false',     // Whether to access the listings table from a different WP install
//            'imports_url'           => '',
//            'imports_path'          => '',
//            'import_fields' => array(),
//            'field_formats' => array(),
        );
        return self::$options_defaults;
    }
    

static function add_spaces_NOT_USED($string) {
    // Add spaces to CamelCase name
    // Allow single digit at end of a word
//    $ccWord = 'NewNASAModule';
    $re = '/(?#! splitCamelCase Rev:20140412)
        # Split camelCase "words". Two global alternatives. Either g1of2:
          (?<=[a-z])      # Position is after a lowercase,
          (?=[A-Z])       # and before an uppercase letter.
        | (?<=[1-9])      # Or after a digit  
          (?=[A-Z])       # and before an uppercase letter.
        | (?<=[A-Z])      # Or g2of2; Position is after uppercase,
          (?=[A-Z][a-z])  # and before upper-then-lower case.
        /x';
    $words = preg_split($re, $string);
//    $count = count($words);
//    for ($i = 0; $i < $count; ++$i) {
//        printf("Word %d of %d = \"%s\"\n",
//            $i + 1, $count, $words[$i]);
//    }    
    return implode(' ', $words);
}
    
//    static function write_log($message, $logfile, $mode='a') {
//      // Append to the log file
//      if ( $fd = @fopen($logfile, $mode) ) {
////        $result = fputcsv($fd, array($date, $remote_addr, $request_uri, $message));
//        $result = fwrite($fd, $message);
//        fclose($fd);
//
//        if( $result > 0 ) {
//            $response = array('status' => true);  
//        } else {
//            // Can't write to the file
//            $response = array('status' => false, 'message' => 'Unable to write to '.$logfile.'!');
//        }
//      } else {
//          // Can't open the file
//          $response = array('status' => false, 'message' => 'Unable to open file '.$logfile.'!');
//      }
//      if ( ! $response['status'] && $logfile !== NIMP_DEBUG_LOG ) {  // Avoid an infinite loop!
//            if ( defined( 'NIMP_DEBUG' ) ) {
//                $log = date('Y-m-d H:i:s') . " Error writing log: {$response['message']}\n";
//                self::write_log( $log, NIMP_DEBUG_LOG );                
//            }          
//      }
//      return $response;
//    }    
//    
//    static function error_log($message) {
//        // Add the date/time and write $message to error log
//        $msg = date('Y-m-d H:i:s') . ' ' . $message . "\n";
//        self::write_log($msg, NIMP_ERROR_LOG);
//    }

//    static function get_photo_path_old($id) {
//        // Directory structure, based on id, is like this: NRL/100/
//        // Photos for 100 properties are in a directory
//        
//        if ( empty($id) ) {
//            return false;
//        }
//        $pad_id = '000' . $id;
////        $photo_dir = get_home_path() . 'wp-content/' . NIMP_CONTENT_DIR . '/' . (substr($pad_id, -3, 1) + 1) . '00/';
//        // KPC Problem: this grabs only a single digit to form the photo directory!
//        $photo_dir = ABSPATH . 'wp-content/' . NIMP_CONTENT_DIR . '/' . (substr($pad_id, -3, 1) ) . '00/';
//
//        return $photo_dir;
//    }    
    
//    static function get_photo_path($id) {
//        // Directory structure, based on id, is like this: NRL/0010000/10000/
//        // Photos for 100 properties are in a directory
//        
//        if ( empty($id) ) {
//            return false;
//        }
//        $pad_id = '000' . $id;
////        $photo_dir = get_home_path() . 'wp-content/' . NIMP_CONTENT_DIR . '/' . (substr($pad_id, -3, 1) + 1) . '00/';
//        // KPC Problem: this grabs only a single digit to form the photo directory!
////        if ( self::$options['remote_db'] == 'true' && ! empty(self::$options['imports_path']) ) {
////            $path = self::$options['imports_path'];
////        } else {
////            $path = ABSPATH;
////        }
////        $photo_dir = $path . 'wp-content/' . NIMP_CONTENT_DIR . '/' . (substr($pad_id, -3, 1) ) . '00/';
//        
//        // KPC Here's how it should have been done:
//        $str_id = (string) $id;
//        if ( $id < 100 ) {
//            $p_dir = '00000';
//        } else {
//            $p_dir = substr($str_id, 0, strlen($id)-2) . '00';
//            $p_dir = str_pad( $p_dir, 5, "0", STR_PAD_LEFT);
//            $temp = 1;
//        }
////        $grp_dir = str_pad(10000*((int)($id/10000)), 6, "0", STR_PAD_LEFT);
//        $grp_dir = str_pad(10000*((int)($id/10000)), 7, "0", STR_PAD_LEFT);
//        // See if we need to get photos from the imports site
//        if ( self::$options['remote_db'] == 'true' && ! empty(self::$options['imports_path']) ) {
//            $path = self::$options['imports_path'];
//        } else {
//            $path = ABSPATH;
//        }        
//        
//        $photo_dir_2 = $path . 'wp-content/' . NIMP_CONTENT_DIR . '/' . $grp_dir . '/' . $p_dir . '/'; 
//        
//        return $photo_dir_2;
//    }    

//    static function get_photos_old($id) {
//        // Returns an array of file names for the photos for property $id
//        $path = self::get_photo_path_old($id);
//        $pad_id = str_pad($id, 2, "0", STR_PAD_LEFT); // If single digit, left pad with zero
//        $pattern = "{$path}image-{$pad_id}*.*";
//        $photos = glob($pattern);   
//        return($photos);
//    }    
    
//    static function get_photos($id) {
//        // Returns an array of file names for the photos for property $id
//        $path = self::get_photo_path($id);
//        $pad_id = str_pad($id, 2, "0", STR_PAD_LEFT); // If single digit, left pad with zero
//        $pattern = "{$path}image-{$pad_id}-*.*";
//        $photos = glob($pattern);
//        
//        // KPC TEMP for transition
////        if ( empty($photos) ) {
////            $path = self::get_photo_path_old($id);
////            $pattern = "{$path}image-{$pad_id}-*.*";
////            $photos = glob($pattern);            
////        }
//        
//        return($photos);
//    }
    
    static function get_photo_url($path) {
        // Build the photo url from the file path
        $ind = strpos($path,'wp-content');
//        $fname = substr($path,$ind-1);
        if ( self::$options['remote_db'] == 'true' && ! empty(self::$options['imports_url']) ) {
            $base = self::$options['imports_url'];
        } else {
            $base = home_url() . '/';
        }
        
        $url = $base . substr($path, $ind);
        return($url);
        
    }
    
    static function get_photo_urls($photos, $cnt=999) {
        // Returns an array of urls for up to $cnt $photos
        // If there are no photos, returnt he url of a place holder image
        // Called by display functions
        $photo_urls = array();
        if ( ! empty($photos) ) {
            // Use the downloaded photos
            foreach ( $photos as $photo ) {
                $photo_urls[] = NIMP_Util::get_photo_url($photo);
                if ( --$cnt <= 0 ) break;
            }
        } else if ( ! empty($list_picture_url) ) {
            // Use the url for the large size ListPicture
            $photo_urls[] = $list_picture_url;
        } else {
            // Display placeholder photo
            $photo_urls[] = NIMP_URL . 'images/no-photo-available.png';
        }        
        return $photo_urls;
    }
    
//    static function get_post_schedule_options() {
//        // Get the post schedule options
//            $post_schedule = get_option(NIMP_POST_GENERATOR_OPTION_NAME, array());
//            
//            // Update (TEMP)
//            foreach ( $post_schedule as $user => $schedule_data ) {
//                // For backwards compatibility...
//                if ( ! empty($schedule_data) && ! isset($schedule_data['enabled'])) {
//                    $schedule = array();
//                    foreach ( $schedule_data as $item ) {
//                        $schedule[] = $item;
//                    }
//                    $post_schedule[$user] = array(
//                        'enabled'   => 'true',
//                        'schedule' => $schedule
//                        );
//                }
//                if ( ! isset($schedule_data['days'])) {
//                    $post_schedule[$user]['days'] = array('1','1','1','1','1','1','1');
//                }
//                
//                // More backwards compatibility 11-15-17 (Change article id to array
//                if ( ! empty($schedule_data['schedule'][0]) && ! is_array($schedule_data['schedule'][0]['article_id']) ) {
//                    foreach ( $schedule_data['schedule'] as $key => $item ) {
//                        if ( ! is_array($item['article_id']) ) {
//                            $post_schedule[$user]['schedule'][$key]['article_id'] = array($item['article_id']);
//                        }
//                    }
//                }
//            }   // end outer foreach
//        return $post_schedule;
//    }
    
// XXX ******** Do I need to create thumbnails for featured images??? 8/25-18    
    
     static function set_featured_image($post_id, $url) {
        // Add a featured image to the post
        if ( self::url_is_image($url)) {
            update_post_meta( $post_id, '_thumbnail_ext_url', esc_url($url) );
            if ( ! get_post_meta( $post_id, '_thumbnail_id', TRUE ) ) {
                update_post_meta($post_id, '_thumbnail_id', 'by_url' );
            }
        } elseif ( get_post_meta( $post_id, '_thumbnail_ext_url', TRUE ) ) {
            delete_post_meta( $post_id, '_thumbnail_ext_url' );
            if ( get_post_meta( $post_id, '_thumbnail_id', TRUE ) === 'by_url' ) {
                delete_post_meta( $post_id, '_thumbnail_id' );
            }
        }
        return;
    }
    
// *** See thumbnail functions in at end of file    
    
     static function url_is_image( $url ) {
        if ( ! filter_var( $url, FILTER_VALIDATE_URL ) ) {
            return FALSE;
        }
        $ext = array( 'jpeg', 'jpg', 'gif', 'png' );
        $info = (array) pathinfo( parse_url( $url, PHP_URL_PATH ) );
        return isset( $info['extension'] )
            && in_array( strtolower( $info['extension'] ), $ext, TRUE );
    }    
    
//    static function elapsed_time_string($mytime) {
//        // $mytime is in seconds
//        // Return an array of days, hours, minutes
//        $result = array();
//        $result['days'] = (int)($mytime / (24*3600));
//        $result['hours'] = (int) (($mytime - $result['days']*24*3600) / 3600);
//        $result['minutes'] = (int)(($mytime - $result['days']*24*3600 - $result['hours']*3600) / 60);
//        return($result);
//    }
    
// Moved to class NRL_Common    
    static function search_val_encode($val){
// Used ONLY in NIMP_MLS_Util::sitemap_generate_urls_local() for /local/ virtual page urls
//        $val = str_replace( '-', '~', strtolower($val) );
        $val = str_replace( ' ', '_', $val );
        $val = urlencode($val);
        return $val;      //str_replace( ' ', '_', $val );
    }

//    static function search_val_decode($val) {  
//// *** NOT USED! ***
//        $val = str_replace( '+', ' ', $val );
//        $val = str_replace( '_', ' ', $val );
//        return urldecode($val);
////        return str_replace( '~', '-', $val );
//    }
    
    static function create_adwords_csv() {
        // Create a csv file for import to Google Adwords
        // For efficiency, the csv file is written 500 lines at a time
        
        $line_cnt = 500;
//        $header = "Keyword state,Keyword,Status,Max. CPC,Clicks,Impressions,CTR,Avg. CPC,Cost,Avg. position,Conversions,Cost / conv.,Conv. rate,All conv.,View-through conv.,Labels\n";
//        $header = "Campaign,Ad group,Keyword status,Keyword,Status,Max. CPC,Policy details,Final URL,Clicks,Impr.,CTR,Avg. CPC,Cost\n";
        $header = "Ad group ID,Keyword status,Keyword,Status,Max. CPC,Policy details,Final URL,Clicks,Impr.,CTR,Avg. CPC,Cost\n";
        $data = array();
        for ( $cnt=0; $cnt<=11; $cnt++ ) {
            $data[] = "";
        }
//        $data[0] = "NRL - visit site targe";   // Campaign
//        $data[1] = "NRL - Address";     // Ad group
        $data[0] = "47796482140";   // Ad group ID = NRL - Address
        $keyword_index = 2;
        $url_index = 6;
        
        $fname = NIMP_PATH . "adwords-keywords-" . date('Y-m-d') . ".csv";
        $output = $header;
        // Get listings
        global $wpdb_nrl;
        $my_query = "SELECT * FROM $wpdb_nrl->nrl_properties WHERE CityName='Alexandria' LIMIT 10";
        $listings = $wpdb_nrl->get_results( $my_query, ARRAY_A );
        if ( ! empty($listings) ) {
            $temp = 1;
            if ($fd = @fopen($fname, "w")) {
                foreach ( $listings as $listing ) {
                    $address = $listing['FullStreetAddress'] . ' ' . $listing['CityName'] . ' VA ' . $listing['PostalCode'];
                    // Remove any double quotes from the address, because it will mess up the csv file
                    $address = str_replace( '"', '', $address );
                    $data[$keyword_index] = '"' . $address . '"';
                    $data[$url_index] = '"' . NIMP_Display::format_property_url($listing) . '"';
                    $output .= implode(',', $data) . "\n";
                    
// **** KPC Need to check the value of $line_cnt 
                }
                $result = fwrite($fd, $output);            
                fclose($fd);
            } else {
                $result = false;
            }            
            
        }
    }    

    static function utility() {
        // Used to run whatever utility code I put in here
        
        // Update properties table structure
        require_once NIMP_PATH . 'library/class-install.php';
        
        $result = NIMP_Install::create_properties_table();
        echo "Update properties table result: ";
        print_r($result);
        return;
        
//        NIMP_MLS_Util::create_sitemap('local');
        NIMP_MLS_Util::create_sitemap('sales');
        return;
        
        NIMP_MLS_Util::get_cities_list();
        return;  
        
        global $wpdb;
        $result = $wpdb->query('DROP TABLE IF EXISTS ' . $wpdb->nrl_cities );
        NIMP_MLS_Util::create_cities_table();
        echo "Cities table deleted and re-created.<br/>\n";
        return;
        
        global $wpdb;
        $query = "SELECT * FROM $wpdb->nrl_cities ORDER BY County,City,Subdivision";
        $records =  $wpdb->get_results( $query, ARRAY_A );
        $temp = 1;
        echo "Subdivision names with 4 characters or less\n";
        foreach ( $records as $record ) {
//            echo "{$record['County']} --- {$record['City']} --- {$record['Subdivision']}<br/>\n";
            // Creating a .csv file, so be sure value does not contain a comma
            $subdiv = trim($record['Subdivision']);
            if ( strlen($subdiv) < 5 || ctype_digit($subdiv) ) {
                echo $subdiv . "<br/>\n";
            }
        }
        
        return;
        
        NIMP_MLS_Util::create_cities_table();
        return;
      


        // Get the first 20 records in the database
        global $wpdb;
        // See if this is a new city/subdivision
        $query = 'SELECT id,County,CityName,Subdivision FROM ' . $wpdb->nrl_properties . ' ORDER BY id LIMIT 50 ';
        $result =  $wpdb->get_results( $query, ARRAY_A );
        $temp = 1;
        foreach ( $result as $row ) {
            echo $row['id'] . '  ' . $row['County'] . ' -- ' . $row['CityName'] . ' -- ' . $row['Subdivision'] . "<br/>\n";
        }
        return;
        
        NIMP_MLS_Util::get_cities_list();
        return;
        
        NIMP_MLS_Util::create_cities_table();
        return;
        
//        $mls_options = NIMP_MLS_Util::get_options();
//        if ( ! isset($mls_options['rental_zips']) ) {
//            $mls_options['rental_zips'] = array('22041','22043','22101'.'22102','22103','22106','22107','22108','22109','22150','22152','22180','22182','22183','22185','22314','22309','22310','22308','22307','22303',
//            '22304','22312','22202','22201','22203','22205','22209');
//            $result = NIMP_MLS_Util::update_options( $mls_options, 'NIMP_Util::validata_basic()' );
//            echo '++Updated MLS Options rental zips<br/>';
//        } else {
//            echo '++MLS rental zips already present<br/>';
//        }
//
//        return;
//        
//        $mls_options = NIMP_MLS_Util::get_options();
//        $test = strlen($mls_options['rental_zips'][3]);
//        echo "Length of " . $mls_options['rental_zips'][3] . " is $test<br/>";
//        print_r($mls_options['rental_zips']);
//        return;
//        
//        $mls_options = NIMP_MLS_Util::get_options();
//        $mls_options['rental_zips'] = '';
//        $result = NIMP_MLS_Util::update_options( $mls_options, 'NIMP_Util::validata_basic()' );
//        echo '++Updated MLS Options<br/>';
//        return;
        
        // Setup maintenance function
        $defaults = array();
        $maint_options = get_option('nrl_maintenance',$defaults);
        $maint_options['first_run'] = true;
        $result = update_option('nrl_maintenance',$maint_options);
        if ( ! $result ) {
            echo "Error updating maintenance options";
        }
        echo "++ Maintanence first_run set to true\n";
        return;
        
        // Code to fix dev site photos in wrong directories
        echo "<br/>Moving photos to correct directory...  <br/>";
        $photo_cnt = 0;
        for ( $grp=12; $grp<=13; $grp++) {
            $old_path = self::get_photo_path($grp*100+1);
            echo "<br/><br/>Processing $old_path...";
            $grp_1 = $grp - 10;
            // Create ids for photos wrongly placed
//            for ($pre=1; $pre<=20; $pre++) {        // was <=20
                for ($post=0; $post<100; $post++) {
//                    $id = $pre . $grp . str_pad($post, 2, "0", STR_PAD_LEFT);
                    $id = $grp . $grp_1 . str_pad($post, 2, "0", STR_PAD_LEFT);
                    echo "$id ";
//                    $pattern = "{$old_path}image-????*-*.*";
                    $pattern = "{$old_path}image-{$id}-*.*";
                    $photos = glob($pattern);
                    $new_path = self::get_photo_path($id);
                    if ( count($photos) ) {
                        NIMP_MLS_Util::create_photo_path($new_path);
                    }                    
//                    echo count($photos) . ' photos found<br/>';
                    foreach ( $photos as $photo ) {
                        $ind = strpos($photo, 'image-');
                        $new_name = $new_path . substr($photo,$ind);
                        if ( ! rename($photo,$new_name) ) {
                            // Error moving photo
                            $temp = 1;
                    //                    echo "<img src='$photo' />";
                        }
                        $photo_cnt++;
                    }
                }
//            }
            

//            break;
        }
        echo "<br/>$photo_cnt Photos moved<br/>";
        return;        
        
        $options = self::get_options();
        $options['search_results_max'] = 250;
        $options['listings_per_page'] = 25;
        self::update_options($options);
        return;
        
        NIMP_MLS_Util::update_csv_field_settings();
        return;
        
        
        $mls_options = get_option(NIMP_MLS_OPTIONS_NAME);
        $field_settings = array();
        foreach ( $mls_options['field_formats'] as $key => $val ) {
            $field_settings[$key] = array(
                'show'  =>  1,
                'empty' =>  1,
                'format'    => $val,
                'suffix'    => '',
            );            
        }
        // Sort field array alphabetically by key
        ksort($field_settings);
        update_option( NIMP_FIELDS_OPTION_NAME, $field_settings );
        return;

    }
  
// MOVED to NRL_Common..    
//    static function get_request_uri() {
//        // Get request uri
//        // Workaround because filter_input returns NULL on $_SERVER variables on some systems
//        // Ref: https://github.com/xwp/stream/issues/254
//        if ( filter_has_var(INPUT_SERVER, 'REQUEST_URI') ) {
//            $url_path  = filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL);
//        } else {
//            if (isset($_SERVER['REQUEST_URI']))
//                $url_path  = filter_var($_SERVER['REQUEST_URI'], FILTER_SANITIZE_URL, FILTER_NULL_ON_FAILURE);
//            else
//                $url_path = false;
//        }        
//        return $url_path;
//    }
//
//    static function get_page_uri() {
//        // Returns the complete page uri
//        // Get request uri
//        // Workaround because filter_input returns NULL on $_SERVER variables on some systems
//        // Ref: https://github.com/xwp/stream/issues/254
////        if ( filter_has_var(INPUT_SERVER, 'REQUEST_URI') ) {
////            $url_path  = filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL);
////        } else {
////            if (isset($_SERVER['REQUEST_URI']))
////                $url_path  = filter_var($_SERVER['REQUEST_URI'], FILTER_SANITIZE_URL, FILTER_NULL_ON_FAILURE);
////            else
////                $url_path = false;
////        }
//        $url_path = self::get_request_uri();
//        if ( $url_path ) {
//            $host = filter_var($_SERVER['HTTP_HOST'], FILTER_SANITIZE_URL, FILTER_NULL_ON_FAILURE);
//        }
//        $scheme = filter_input(INPUT_SERVER, 'REQUEST_SCHEME', FILTER_SANITIZE_URL);
//        if ( ! empty($scheme) ) {
//            $path = $scheme. "://" . $host . $url_path;
//        } else {
//            $path = "http://" . $host . $url_path;
//        }
//        return $path;
//    }
    
    static function excerpt( $text, $length=60 ) {
        // Form an excerpt from $text of max $length in characters
        // Respect word boudaries
        
        // KPC rewrite to use preg_match. Words might be separated by commas w/ no space
        
        if ( strlen($text) > $length) {
            $text = substr($text, 0, $length);
            $ind = strrpos($text, ' ');
            $text = substr($text, 0, $ind);
            if ( substr( $text, -1 ) == ',' ) {
                $text = substr($text, 0, strlen($text)-1);
            }
        }
        return $text;
    }    
    
    static function make_slug($str)	{
        // Creates a WordPress like slug from $str
		if($str !== mb_convert_encoding( mb_convert_encoding($str, 'UTF-32', 'UTF-8'), 'UTF-8', 'UTF-32') )
			$str = mb_convert_encoding($str, 'UTF-8', mb_detect_encoding($str));
		$str = htmlentities($str, ENT_NOQUOTES, 'UTF-8');
		$str = preg_replace('`&([a-z]{1,2})(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig);`i', '\\1', $str);
		$str = html_entity_decode($str, ENT_NOQUOTES, 'UTF-8');
		$str = preg_replace(array('`[^a-z0-9]`i','`[-]+`'), '-', $str);
		$str = strtolower( trim($str, '-') );
		$str = substr($str, 0, 100);
		return $str;
	}
    
    static function make_excerpt($text, $charlength=250) {
        // makes an excerpt from $text with a max length of $charlength, respecting word boundaries
        if ( mb_strlen( $text ) > $charlength ) {
            $subex = mb_substr( $text, 0, $charlength - 5 );
            $exwords = explode( ' ', $subex );
            $excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
            if ( $excut < 0 ) {
                $result =  mb_substr( $subex, 0, $excut );
            } else {
                $result = $subex;
            }
            $result .= '...';
        } else {
            $result =  $text;
        }
        return $result;        
    }
    
    static function disable_wpautop( $content ) {
      remove_filter( 'the_content', 'wpautop' );
      return $content;
    }    

    static function get_random_recent_listing( $limit='20' ) {
        // Get a random recent listing
        global $wpdb_nrl;
        $query = "SELECT * FROM $wpdb_nrl->nrl_properties WHERE ForSale='1' ORDER BY ListDate DESC LIMIT $limit";
        $results = $wpdb_nrl->get_results( $query, ARRAY_A );
        if ( empty( $results ) ) {
            return '';
        }
        $rand = mt_rand(0,count($results)-1);
        $listing = $results[$rand];
        return $listing;
    }

//    static function ajax_generate_listing_page() {
//        // Generate a listing page based randomly selected recent listing
//        // This function is used to test the generation of listing pages--Normally this will happen during imports
//        // Returns an array( 'success' => true/false, 'ID' => generated post id, 'msg' => error message
//        if ( ! $user_ID = get_current_user_id() ) {
//            echo "No user logged in";
//            die();
//        }
//
//        // Get a random recent listing
//        $listing = NIMP_Util::get_random_recent_listing();
//        
//        $id = NIMP_MLS_Util::create_listing_page($listing);
//        
//        if ( ! empty($id) ) {
//            $response['success'] = true;
//            $response['msg'] = "Listing page created";                
//            $response['id'] = $id;
//            $response['url'] = home_url() . "/wp-admin/post.php?post=$id&action=edit";            
//        } else {
//            $response['success'] = false;
//            $response['msg'] = "Error creating listing page";                
//        }
//        
//        echo json_encode($response, JSON_UNESCAPED_SLASHES);
//        die();
//    }   // end function ajax_generate_listing_page()    
    
    static function kses_filter_allowed_html($allowed, $context){
        // Add to the list of allowed html for the kses filter
        // Thie prevents WP from removing the data attribute from listing posts
        // ref: http://mawaha.com/allowing-data-attributes-in-wordpress-posts/
        // ref: https://wordpress.stackexchange.com/questions/255754/wp-insert-post-is-automatically-modifying-my-post-content
 
        if (is_array($context)) {
            return $allowed;
        }
        $temp = $allowed['div'];
        if ($context === 'post') {
            $allowed['div']['data-mapinfo'] = true;
            $allowed['div']['style'] = true;
            $allowed['a']['data-toggle'] = true;
            $allowed['a']['title'] = true;
            $allowed['a']['data-content'] = true;
            $allowed['a']['data-placement'] = true;
            $allowed['script'] = true;
            $allowed['style'] = true;
        }
        $temp2 = $allowed['div'];
        return $allowed;
    }    
    
    static function allow_data_attributes() {
        // Allow data attribute in div tags
        // ref: https://cynng.wordpress.com/2013/05/28/adding-html-attribute-exceptions-to-wordpress-kses/
        global $allowedposttags, $allowedtags;
//        $temp1 = $allowedposttags['div'];
//        $temp2 = $allowedtags['div'];
        $allowedposttags['div']['data-mapinfo'] = true;
        $allowedtags['div']['data-mapinfo'] = true;
    }
  
// Moved to NRL_Common    
//    static function get_post_var( $var_name, $array=false ) {
//        // Checks for $var_name in $_POST and returns the value, if any
////        $flags = ( $var_name === 'awf_filters' ? 'FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY' : 'FILTER_SANITIZE_STRING');
//        if ( filter_has_var(INPUT_POST, $var_name) ) {
//            if ( $array ) {
//                $value = filter_input(INPUT_POST, $var_name, FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);
//            } else {
//                $value = filter_input(INPUT_POST, $var_name, FILTER_SANITIZE_STRING);
//            }
//        } else {
//             $value = '';
//        }
//        return $value;        
//    } 
    
    static function check_memory_usage() {
        // Check memory usage, and write to log if it is over a certain limit
        
        // *** NOT FINISHED ***
        
        
        
        // Is the cron job running?
        $fname = ABSPATH . 'wp-content/NRL/lock';
        $fp = fopen( $fname, 'r+' );
        if( ! flock($fp, LOCK_EX | LOCK_NB) ) {
            $cron_job = true;
        } else {
            $cron_job = false;
        }
        fclose($fp);
        
        
        
    }
    
    
    static function thumbnail_url_field( $html ) {

   $img_url = get_post_meta(get_the_id(), '_thumbnail_ext_url',true);
    $html .= '<div>';
//    $html .= '<p>' . __( 'This is the Featured Image that was inserted during post creation', 'txtdomain' ) . '</p>';
    
        $html .= '<p><img style="max-width:150px;height:auto;" src="' 
            . $img_url . '"></p>';
        $html .= '<p>' . __( '', 'txtdomain' ) . '</p>';
    
    $html .= '</div>';
    return $html;
}

    static function thumbnail_external_replace( $html, $post_id ) {
        // This function is called by the post_thumbnail_html filter
        $url =  get_post_meta( $post_id, '_thumbnail_ext_url', TRUE );
        if ( empty( $url ) || ! NIMP_Util::url_is_image( $url ) ) {
            return $html;
        }
        $alt = get_post_field( 'post_title', $post_id ) . ' ' .  __( 'thumbnail', 'txtdomain' );
        $attr = array( 'alt' => $alt );
        $attr = apply_filters( 'wp_get_attachment_image_attributes', $attr, 10, 3 );
        $attr = array_map( 'esc_attr', $attr );
        $html = sprintf( '<img src="%s"', esc_url($url) );
        foreach ( $attr as $name => $value ) {
            $html .= " $name=" . '"' . $value . '"';
        }
        $html .= ' />';
        return $html;
    }
  
    static function error_log( $message ) {
        // Add the date/time and write $message to error log
        $msg = date('Y-m-d H:i:s') . ' ' . $message . "\n";
        NRL_Common::write_log( $msg, NIMP_ERROR_LOG ) ;
    }
    
    static function csv_file_create( $fname, $data, $add_key=false, $overwrite=true ) {
        // Creates a CSV file based on data
        // $data is an array of arrays (an array of values for each line)
        // If $add_key is true the inner array key is added as the first column
        // If the file already exists, it will be overwritten depending on the value of $overwrite
        // Returns true of false
        
        $mode = ( $overwrite ? 'w' : 'x' );
        $result = true;
        if ( ($handle = fopen( $fname, $mode ) ) !== FALSE ) {
            foreach ( $data as $key => $line ) {
                if ( ! is_array($line) ) {
                    // fputcsv expects each line to be in an array
                    $line = array( $line);
                }
                if ( $add_key ) {
                    $line = array_merge( array($key), $line );
                }
                if ( false === fputcsv( $handle, $line ) ) {
                    // Error writing file
                    NIMP_Util::error_log( "Error writing to CSV file $fname" );
                    $result = false;
                    break;
                }
            }
        } else {
            // Can't open file. If $overwrite is false, the file may already exist.
            NIMP_Util::error_log( "Unable to open CSV file $fname for writing" );
            $result = false;
        }
        return $result;
    }

    static function csv_file_create_assoc( $fname, $data, $add_key=false, $overwrite=true ) {
        // Creates a CSV file $fname  based on data in an associative array (an array of arrays)
        // Use the keys of the first array element as the column headers
        // *** This assumes that the inner array keys are the same for all array elements ***
        // $data is an array of associative arrays (an array of values for each line)
        // If $add_key is true the key of the outer array is added as the first column
        // If the file already exists, it will be overwritten depending on the value of $overwrite
        // Returns true of false
        
        $mode = ( $overwrite ? 'w' : 'x' );
        $result = true;
        $first = true;
        if ( ($handle = fopen( $fname, $mode ) ) !== FALSE ) {
            foreach ( $data as $key => $line ) {
                if ( ! is_array($line) ) {
                    // fputcsv expects each line to be in an array
                    $line = array( $line);
                }
                if ( $first ) {
                    $first = false;
                    $heading = array();
                    if ( $add_key ) {
                        $heading[] = 'key';
                    }
                    foreach ( $line as $inner_key => $val ) {
                        $heading[] = $inner_key;
                    }
                    fputcsv( $handle, $heading );
                }
                if ( $add_key ) {
                    $line = array_merge( array($key), $line );
                }
                if ( false === fputcsv( $handle, $line ) ) {
                    // Error writing file
                    NIMP_Util::error_log( "Error writing to CSV file $fname" );
                    $result = false;
                    break;
                }
            }
        } else {
            // Can't open file. If $overwrite is false, the file may already exist.
            NIMP_Util::error_log( "Unable to open CSV file $fname for writing" );
            $result = false;
        }
        return $result;
    }    
    
    static function csv_file_get( $fname, $use_key=false, $header=false ) {
        // Gets the contents of CSV file $fname
        // Returns an array of arrays (one array of values for each line)
        // If $use_key is true, use the first column as the array key
        // If table has a single column, returns a simple array of values
        // Returns false if the file cannot be opened
        
        if ( ($handle = fopen($fname, "r") ) !== FALSE) {
            $data = array();
            while ( ($line = fgetcsv($handle, 1000) ) !== FALSE) {
                $temp = 1;
                if ( $header ) {
                    // Skip the header row
                    $header = false;
                    continue;
                }
                if ( count($line) === 1 ) {
//                    $line = $line[0];
                    $data[] = $line[0];
                } else if ( $use_key ) {
                    $my_key = array_shift( $line );
                    if ( count($line) === 1 ) {
                        $data[$my_key] = $line[0];
                    } else {
                        $data[$my_key] = $line;
                    }
                } else {
                    $data[] = $line;
                }
            }
            fclose($handle);
        } else {
            $data = false;
            NIMP_Util::error_log( "Unable to open CSV file $fname for reading" );
        }
        return $data;
    }
    
    static function csv_file_get_assoc( $fname, $use_key=false ) {
        // Gets the contents of CSV file $fname
        // Returns an array of arrays (one array of values for each line)
        // The inner array is an associative array, using the first row as key names
        // If $use_key is true, use the first field as the key for the outer array

        
        if ( ($handle = fopen($fname, "r") ) !== FALSE) {
            $data = array();
            while ( ($row = fgetcsv($handle, 1000) ) !== FALSE) {
                // Check for the first (= header) row
                if (empty($fields)) {
                    $fields = $row;
                    if ( $use_key ) {
                        array_shift( $fields );  // Don't use the field name for the first column
                    }
                    continue;
                }
                // See if we need to use the first col as the key
                if ( $use_key ) {
                    $my_key = array_shift( $row );
                }
$temp = 1;                
                // Parse the row into the associative array
                foreach  ($row as $k=>$value ) {
                    $results[$fields[$k]] = $value;
                }
                if ( ! empty($my_key) ) {
                    $data[$my_key] = $results;
                } else {
                    $data[] = $results;
                }
            }
            fclose($handle);
        } else {
            $data = false;
            NIMP_Util::error_log( "Unable to open CSV file $fname for reading" );
        }
        return $data;
    }    

    
} // end class NIMP_Util
