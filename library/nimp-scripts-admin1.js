/* 
 * Description: Admin javascript for the NRL Imports plugin
 *
 * @author Ken Carlson
 */

// New function for Imports plugin

function nimpSetup( setupType ) {
    // Use AJAX to run setup functions
//    alert("Setup type " + setupType );
        var data = {
            action: 'nimp_setup',
            nimp_task: setupType,
        };
        console.log('Sending AJAX request to run setup function: ' + setupType);
        
        nrl_status_message( "Working on " + setupType +"..." );
        jQuery.post( ajaxurl, data, function(response) {
            nrl_status_message("");
            response = JSON.parse(response);
            var myStatus = '#nimp_setup_' + setupType + "> span";
            jQuery(myStatus).html(response['message']);
            jQuery("#nrl_setup_details").append(response['details']);
        });
    return false;
}


var brightUpdateCount = 0;
var brightYesNoFields;
var brightConvertCount = 0;

function nimpTools( toolType ) {
    // Use AJAX to process NIMP tools
    
    if ( 'process_db_fields' === toolType ) {
        console.log('Sending AJAX request to update databaase');
        nimp_status_message( "nimp_tools_status", "Preparing Bright MLS database update..." )
        brightConvertCount = 0;
        brightUpdate();
        return;
    }
    
    var data = {
        action: 'nimp_tools',
        nimp_task: toolType
    };
    console.log('Sending AJAX request to run tools: ' + toolType);    
//        nrl_status_message( "Working on " + toolType +"..." );
    nimp_status_message( "nimp_tools_progress_notice", "Working on " + toolType +"..." )
    jQuery.post( ajaxurl, data, function(response) {
//            alert( "JSON: " + response );
        nimp_status_message( "nimp_tools_progress_notice", "" )
        response = JSON.parse(response);
//         alert( "Result: " + response['finished'] + '  Message: ' + response['message'] );
        // See if the process is finished
        if ( response['finished'] !== 'true') {
            jQuery("#nimp_tools_status").html(response['message']);
        } else {
            jQuery("#nimp_tools_log").append(response['message']);
            jQuery("#nimp_tools_status").html('Finished');
        }
    }); 
    
}


function nimpDebug( toolType ) {
    // Use AJAX to process NIMP debug
    
    var nrlSetting = jQuery("#nimp_debug_nrl_setting").val();
    var data = {
        action: 'nimp_debug',
        nimp_task: toolType,
        nrl_setting: nrlSetting
    };
    console.log('Sending AJAX request to run debug: ' + toolType);

//        nrl_status_message( "Working on " + toolType +"..." );
    nimp_status_message( "nimp_debug_progress_notice", "Working on " + toolType +"..." )
    jQuery.post( ajaxurl, data, function(response) {
//            alert( "JSON: " + response );
        nimp_status_message( "nimp_debug_progress_notice", "" )
        response = JSON.parse(response);
//         alert( 'Message: ' + response['message'] );
        jQuery("#nimp_debug_log").html(response['message']);
    }); 
}


function nimpConvert( toolType ) {
    // Use AJAX to process NIMP tools
//    myCount = typeof myCount !== 'undefined' ? myCount : 0;

    brightConvertCount = 0;
    
    // Start the convertion task via AJAX
    console.log('Sending AJAX request to conversion tool: ' + toolType);    
    
    nimpProcessConvert( toolType );
    
    return;
    
//    var data = {
//        action: 'nimp_convert',
//        nimp_task: toolType
//    };
    
    if ( 'process_db_fields' === toolType ) {
        if ( myCount === -1 ) {
            // Initialize the process
// *** Do this only the first time...
            console.log('Sending AJAX request to update databaase fields');
            nimp_status_message( "nimp_tools_status", "Preparing Bright MLS database update..." )
             
            // Get the array of field names

            brightUpdateCount = 0;

            
        } else {
            nimp_status_message( "nimp_tools_status", "Updating Bright MLS database field" + brightUpdatCount + " of " + brightYesNoFields.length );
            // Send the field name to AJAX..
           
        }
        
// *** need to see if brightYesNoFields[brightUpdateCount] is defined        
        
        data.field_name = brightYesNoFields[brightUpdateCount];
//        var data = {
//            action: 'nimp_convert',
//            nimp_task: toolType,
//            field_name: brightYesNoFields[brightUpdateCount]
//        };         
        
//        brightUpdate();
//        return;
    } else {
        console.log('Sending AJAX request to run tools: ' + toolType);    
        
    }
    
}

function nimpProcessConvert( toolType ) {
    // Repeat AJAX command until the current convert task is finished
    // brightConvertCount is used by the server to interate through the task
    
    var newCount = brightConvertCount + 1;
    nimp_status_message( "nimp_tools_progress_notice", "Bright MLS conversion " + toolType + " Step: " + newCount.toString() + '  ' );
    var data = {
        action: 'nimp_convert',
        nimp_task: toolType,
        nimp_count: brightConvertCount
    };    
    jQuery.post( ajaxurl, data, function(response) {
//            alert( "JSON: " + response );
        response = JSON.parse(response);
//         alert( "Result: " + response['finished'] + '  Message: ' + response['message'] );
        // See if the process is finished
        if ( response['finished'] === 'false') {
            jQuery("#nimp_tools_status").html(response['message']);
            jQuery("#nimp_tools_log").append(response['message']);
            brightConvertCount++;
            nimpProcessConvert( toolType );
        } else {
            jQuery("#nimp_tools_log").append(response['message']);
            jQuery("#nimp_tools_status").html('Finished');
            nimp_status_message( "nimp_tools_progress_notice", "" );
        }
    });
}

function brightUpdate() {
    // Update database for Bright MLS
    var data = {
        action: 'nimp_tools',
        nimp_task: 'brightUpdate',
        bright_count: brightUpdateCount
    };

    jQuery.post( ajaxurl, data, function(response) {
        response = JSON.parse(response);
        // See if the process is finished
        jQuery("#nimp_tools_status").html(response['message']);
        if ( response['finished'] !== 'true') {
            brightUpdate();
        } else {
//            jQuery("#nimp_tools_log").append(response['message']);
//            jQuery("#nimp_tools_status").html('Finished');
        }
    }); 
}

// This function is used by both plugins
function nrl_status_message(text) {
//    document.getElementById("nrl_progress_notice").innerHTML = text;
    nrlNotice = document.getElementById("nrl_progress_notice");
    nrlNotice.innerHTML = text + '  ';
    if ( text.length > 0 ) {
        nrlNotice.style.display = 'block';
//        console.log("Make status message visible: " + text);
    } else {
        nrlNotice.style.display = 'none';        
//        console.log("Make status message invisible: " + text);
    }
//    nrl_toggleVisibility("nrl_progress_notice");
}

function nimp_status_message( id, text ) {
    nrlNotice = document.getElementById( id );
    nrlNotice.innerHTML = text;
    if ( text.length > 0 ) {
        nrlNotice.style.display = 'block';
    } else {
        nrlNotice.style.display = 'none';        
    }    
}

function nimp_edit_field_setting( ident ) {
    
//    var fieldName = ident.children[0].innerHTML;
//    alert("Edit field " + fieldName );
    
}