<?php
/**
 * Class NIMP_Fields
 * Used to set field settings used to display fields
 * Saves field settings to nrl_settings
 *
 * @author Ken Carlson <kencarlsonconsulting.com>
 */
class NIMP_Fields {

    static function create_field_settings() {
        // Setup the field setting array if it is missing
        // Returns true on success or false on failure
        
        $field_settings = NRL_Common::get_nrl_setting('field_settings');
        if ( ! empty($field_settings) ) {
//            return false;
        }
        
        $types = array( 'sales', 'rentals');
        $field_settings = array();
        foreach ( $types as $type ) {
            $field_formats = NRL_Common::get_nrl_setting("field_formats_$type");
            if ( empty($field_formats) ) {
                return false;
            }
            $temp = 1;
            foreach ( $field_formats as $fname => $format ) {
                if ( empty($field_settings[$fname]) ) {
                    $field_settings[$fname] = array(
                        'section'   => '',
                        'layout'    => '',
                        'empty'     => '',
                        'label'     => '',
                        'order'     => ''
                    );
                }
            }
        }
        ksort( $field_settings );
        NRL_Common::update_nrl_setting( 'field_settings', $field_settings );
        return true;
    }

    
    
    
    
    
    
}
