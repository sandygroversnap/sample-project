<?php
/**
 * Description: Admin functions for NRL Imports plugin
 *
 * @author Ken Carlson
 */
class NIMP_Admin {

    static $options, $default_tab, $current_tab;
    static $settings_tabs = array();
    static $polygons = array();
    static $poly_map_init = false;
    static $notice;                     // Admin notice
    //
// REMOVE:
//    static $mls_options;
//    static $post_schedule;  // Post generation schedule by editor ID
//    static $article_templates;   // ID => Title    
    
    static function initialize_options() {

        add_settings_section(
                'nimp_factoid_text', // ID used to identify this section and with which to register options
                __('Factoid Text', NIMP_TEXT_DOMAIN), // Title to be displayed on the administration page
                array('NIMP_Admin', 'settings_factoid_text'), // Callback used to render the description of the section
                'factoid_text'    // Page on which to add this section of options.   Used by do_settings_sections()
        );
        
//        add_settings_section(
//                'nimp_settings', // ID used to identify this section and with which to register options
//                __('Basic Settings', NIMP_TEXT_DOMAIN), // Title to be displayed on the administration page
//                array('NIMP_Admin', 'settings_basic'), // Callback used to render the description of the section
//                'basic_settings'    // Page on which to add this section of options.   Used by do_settings_sections()
//        );

        // Import Settings section
        add_settings_section(
                'nimp_import_settings', // ID used to identify this section and with which to register options
                __('Import Settings', NIMP_TEXT_DOMAIN), // Title to be displayed on the administration page
                array('NIMP_Admin', 'settings_import'), // Callback used to render the description of the section
                'import_settings'    // Page on which to add this section of options.   Used by do_settings_sections()
        );
        
//        add_settings_section(
//                'nrl_mls_field_settings', // ID used to identify this section and with which to register options
//                __('MLS Field Settings', NIMP_TEXT_DOMAIN), // Title to be displayed on the administration page
//                array('NIMP_Admin', 'settings_mls_field'), // Callback used to render the description of the section
//                'mls_field_settings'    // Page on which to add this section of options.   Used by do_settings_sections()
//        );
        

        add_settings_section(
                'nimp_field_settings', // ID used to identify this section and with which to register options
                __('Field Settings', NIMP_TEXT_DOMAIN), // Title to be displayed on the administration page
                array('NIMP_Admin', 'settings_nrl_fields'), // Callback used to render the description of the section
                'nimp_field_settings'    // Page on which to add this section of options.   Used by do_settings_sections()
        );
        
        add_settings_section(
                'nimp_polygon_search', // ID used to identify this section and with which to register options
                __('Polygon Search', NIMP_TEXT_DOMAIN), // Title to be displayed on the administration page
                array('NIMP_Admin', 'settings_polygon_search'), // Callback used to render the description of the section
                'polygon_search'    // Page on which to add this section of options.   Used by do_settings_sections()
        );
        
        add_settings_section(
                'nimp_test', // ID used to identify this section and with which to register options
                __('Test', NIMP_TEXT_DOMAIN), // Title to be displayed on the administration page
                array('NIMP_Admin', 'settings_test'), // Callback used to render the description of the section
                'nimp_test'    // Page on which to add this section of options.   Used by do_settings_sections()
        );
        
        add_settings_section(
                'nimp_tools', // ID used to identify this section and with which to register options
                __('Tools', NIMP_TEXT_DOMAIN), // Title to be displayed on the administration page
                array('NIMP_Admin', 'settings_tools'), // Callback used to render the description of the section
                'nimp_tools'    // Page on which to add this section of options.   Used by do_settings_sections()
        );

        add_settings_section(
                'nimp_debug', // ID used to identify this section and with which to register options
                __('Debug', NIMP_TEXT_DOMAIN), // Title to be displayed on the administration page
                array('NIMP_Admin', 'settings_debug'), // Callback used to render the description of the section
                'nimp_debug'    // Page on which to add this section of options.   Used by do_settings_sections()
        );
        
        add_settings_section(
                'nimp_setup', // ID used to identify this section and with which to register options
                __('Setup', NIMP_TEXT_DOMAIN), // Title to be displayed on the administration page
                array('NIMP_Admin', 'settings_setup'), // Callback used to render the description of the section
                'nimp_setup'    // Page on which to add this section of options.   Used by do_settings_sections()
        );
        
        // Define tabs for the MLS settings section
        self::$settings_tabs = array(
            'import_settings'   =>  'Import Settings',
            'nimp_field_settings'      => 'Field Settings',
            'factoid_text'      => 'Factoid Text',
            'polygon_search'    =>  'Polygon Search',
            'nimp_setup'        =>  'Setup',
            'nimp_tools'        => 'Tools',
        );
        
//        // Privileged  users get extra tabs
        if ( NIMP_DEV_USER ) {
            self::$settings_tabs['nimp_debug'] = 'Debug';
        }
        self::$default_tab = 'import_settings';
        
        // Register the settings
        // The first parameter below is the options group.  It is sent to settings_fields()
        // It is also used in check_admin_referer in (??).  If you change it here, you must change it there as well.
        // The second parameter is the name of the options table entry
        register_setting( 'nimp_options', NIMP_OPTION_NAME, array('NIMP_Admin', 'validate_settings') );
    }   // end function initialize_options



    static function register_options_page() {
        
        // Add warning if imports are disabled
//        self::$mls_options = NIMP_MLS_Util::get_options();
        self::$options = NIMP_Util::get_options();
        if ( self::$options['suspend_imports'] ) {
            NIMP_Util::set_admin_notice("WARNING: MRIS Imports are Suspended!");
        }
        
        // Adds admin menu page
        add_menu_page(
                __('Nesbitt Real Estate Listings Settings', NIMP_TEXT_DOMAIN), // The text to the display in the browser when this menu item is active
                __('NRL Imports', NIMP_TEXT_DOMAIN), // The text for this menu item
                'manage_options', // Which type of users can see this menu
                // The slug should point to the main plugin file, not this one!
                'nimp_settings',  // The unique ID - the slug - for this menu item
                array(__CLASS__,'display_options'),   // The function used to render the menu for this page to the screen
                'dashicons-admin-home',  // house icon
                '4.102'  // Position = before Posts (use decimal number to avoid conflicts
        );
    }

    /* ------------------------------------------------------------------------ * 
     * ********** Display functions ********** 
     * ------------------------------------------------------------------------ */


    static function display_options() {
        // This displays the options menu in the admin area for NRL Settings
        // Load the plugin options
        self::$options = NIMP_Util::get_options();
        
// XXX *** This needs to be replaced with import status settings
        
//        self::$mls_options = NIMP_MLS_Util::get_options();
        
//        if ( empty(self::$mls_options['rental_zips']) ) {
//            self::$notice = 'WARNING: Zip code list is empty. No rental listings will be imorted. Add zip codes on the MLR Settings tab.';
//        }
//        $tab = isset( $_GET['tab'] ) ? $_GET['tab'] : self::$default_tab;
        self::$current_tab = filter_has_var(INPUT_GET, 'tab') ? filter_input(INPUT_GET, 'tab', FILTER_SANITIZE_STRING) : self::$default_tab ;
        $temp = 1;
        ?>
        <script type="text/javascript">
        // Set up tabs for options page (selected supports jQuery ui pre-1.9)
            jQuery(function() { 
    //                jQuery( "#kxc-tabs" ).tabs({ active: <?php echo esc_js(self::$current_tab)-1; ?>, selected: <?php echo esc_js(self::$current_tab)-1; ?> });
                var nimpTabs = jQuery( "#nimp-tabs" );
                nimpTabs.tabs();
            });             
        </script>
        <h2><?php _e(NIMP_PLUGIN_NAME . ' Settings', NIMP_TEXT_DOMAIN); ?></h2>
        <?php settings_errors(); ?>
        <div class="wrap">
            <form id="nrl_optionsform" name="optionsform" class="" action="options.php" method="post" enctype="multipart/form-data">
                <?php //wp_nonce_field('nrl_settings-options', 'nimp_settings'); 
                settings_fields( 'nimp_options' );
                if ( ! empty(self::$notice ) ) { ?>
                <div class="nrl_notice"><p><?php echo self::$notice; ?></p></div>
<?php           } ?>                
                <div>
                    <input type="hidden" name="form-changed" id="form-changed" value="0"/>
                    <input type="hidden" id="cur_tab" name="current_tab" value="<?php echo self::$current_tab; ?>"/>
                </div>
                <div id="nimp-tabs">
                <ul id="nimp-tab-list">
                <?php
                // Display the tab labels
                foreach ( self::$settings_tabs as $key => $val ) {
                    echo '<li id="nimp-tab-'.$key .'"';
                    // select the current tab
                    echo '><a href="#nimp-tabs-' . $key . '">' . esc_html($val) . '</a></li> ';
                }
                ?>
                </ul>

                <?php
                // Display the tab contents
//                settings_fields( 'nrl_options' );
                foreach ( self::$settings_tabs as $key => $val ) {
                    echo '<div id="nimp-tabs-' . $key . '">';
                    do_settings_sections( $key );
                    echo ' </div>';
                }
                ?>
                </div>                
            </form>
        </div>
        <?php
        
    }  // end function display_options        
        
    static function settings_basic_NOT_USED() {
		echo "This is the basic settings section"; 
            ?>
        <div class="clear"></div>
            <fieldset class="nrl_settings_group">
<?php   if ( self::$mls_options['initial_import'] ) {
            echo 'Initial import in progress.  ' . self::$mls_options['remaining'] . ' properties remaining.';
        }
    ?>
                <div class="nrl_tab_content">
                    <div class="nrl_tab_content">
<!--                        <div class="nrl_option_item">
                            <label for="nrl_slideshow_height"><?php //_e('Height for slideshow pictures (this is theme dependent)', NIMP_TEXT_DOMAIN); ?></label></div>
                        <div class="nrl_input">
                            <input name="<?php //echo NIMP_OPTION_NAME; ?>[slideshow_height]" id="nrl_slideshow_height" type="text"
                               value="<?php //echo esc_attr(self::$options['slideshow_height']); ?>" size="35" />
                        </div>                            -->
                        
                    </div>
                </div>
                <p class="submit">
                    <input id="submit1" class="button-primary" type="submit" value="<?php esc_attr_e('Save Changes', NIMP_TEXT_DOMAIN); ?>" onclick="document.pressed = this.value" name="submit" />
                </p>                
                </fieldset>
            <?php
    }   // function settings_basic

//    static function settings_test() {
//        echo 'this is a test';
//    }
    
        
        
    static function settings_import() {
        // Display the basic import settings
        
        global $wpdb;
        
if ( empty(self::$options) ) {
    $temp = 1;
}        
        
        ?>
        <div class="clear"></div>
<?php //echo "These will be the MLS settings";
//        if ( ! empty($import_status['initial_import_phase']) ) {
        ?>
        <div class="nrl_status">
            <h4>MLS Import Status</h4>
<?php
         $list_types = array( 'sales'=>$wpdb->nrl_properties_sales, 'rentals'=>$wpdb->nrl_properties_rentals );
         foreach ( $list_types as $type => $table ) {
                 
            if ( $wpdb->get_var("SHOW TABLES LIKE '$table'") == $table ) {                
                $import_status = NRL_Common::get_nrl_setting("import_status_$type");
//                echo ($import_status['initial_import'] ? '** Initial import in progress.' : 'Initial import finished.' );

               // Get record count
                $query = "SELECT COUNT(*) FROM $table";
                $records = $wpdb->get_results( $query, ARRAY_A );
                $record_cnt = $records[0]['COUNT(*)'];                   
?>                
                <p><b>Current import set: <?php echo $type; ?></b><br/>
                <?php echo ($import_status['initial_import'] ? '** Initial import in progress.' : 'Initial import finished.' ); ?><br/>
                Previous Start Time: <?php echo $import_status['prev_start_time']; ?><br/>
                New Start Time: <?php echo $import_status['new_start_time']; ?><br/>
                Properties Imported: <?php echo $import_status['offset']; ?><br/>
                Properties Remaining: <?php echo $import_status['remaining']; ?><br/>
                Estimated time remaining: 
                <?php 
//                if ( $import_status['offset'] < 25 ) {
//                    echo 'unknown';
//                } else {
//                    $time_used = time() - strtotime( $import_status['new_start_time'] );
//                    $time_remaining = ((int) $import_status['remaining']) * $time_used / ((int)$import_status['offset'] );
//                    $times = NRL_Common::elapsed_time_string($time_remaining);
////                    echo "{$times['days']} days, {$times['hours']} hours, {$times['minutes']} minutes";                                    
//                }  
?>
                <br/>Number of records in database: <?php echo $record_cnt; ?></p>
<?php                
            }
        } // end foreach
        $utc_time = date("H:i");
        $local_time = strftime("%H:%M");
                ?>
                <p>UTC Time: <?php echo $utc_time; ?>  Local time: <?php echo $local_time; ?></p>
<?php  
                        // Check for Maintenance task status
                        // 
                        // 
// XXXX This needs to be modified to use nrl_settings 

// XXX TEMP DISABLED:
        if ( 1 === 2 ) {
            $defaults = array(
                'finished' => true,
            );
            $maint_options = get_option( NIMP_MAINT_OPTION_NAME, $defaults );
            $temp = 1;
//                        if ( ! empty($maint_options['next_id']) ) {
                // Maintenance has been run at least once
            if ( $maint_options['finished'] ) {  ?>
            <p>Maintenance Task is finished</p>
<?php                     if ( $maint_options['custom'] ) { ?>
            <p>Custom Maintenance Task has been scheduled</p>
<?php                     } ?>
<?php                  } else { ?>
            <p>Maintenance Task is active<br/>
                <?php 
                if ( isset($maint_options['offset']) && isset($maint_options['rec_count']) ) {
                    echo "Offset: {$maint_options['offset']}, Record count: {$maint_options['rec_count']}"; 
                }
            ?>
            </p>
<?php
            }

        }  // #### END TEMP DISABLED
?>
        </div>
      <?php   
           $rental_zips = NRL_Common::get_nrl_setting('rental_zips');
           ?>
        <fieldset class="nrl_settings_group">
            <div class="nrl_tab_content">
        <p>Zips from which to import rental listings:</p>
        <textarea id="nrl_zip_list" name="<?php echo NIMP_OPTION_NAME; ?>[rental_zips]" rows="10"><?php  
            echo implode("\n", $rental_zips); 
      ?></textarea><br/><br/>
        <div class="nrl_option_item">
            <label for="nrl_notification_email"><?php _e('Email to use for lists of expired/withdrawn listings', NIMP_TEXT_DOMAIN); ?></label></div>
        <div class="nrl_input">
            <input name="<?php echo NIMP_OPTION_NAME; ?>[notification_email]" id="nrl_notification_email" type="text"
               value="<?php echo esc_attr(self::$options['notification_email']); ?>" size="35" />
        </div><br/>       
<?php // if ( NIMP_DEV_USER ) {
            // Allow option to suspend imports
        $suspend_checked = ( self::$options['suspend_imports'] ? 'checked="checked" ' : ''); 
        $cron_checked = ( self::$options['allow_cron'] ? 'checked="checked" ' : ''); 
?>    
        <input type="checkbox" name="<?php echo NIMP_OPTION_NAME; ?>[allow_cron]" value="true" <?php echo $cron_checked; ?>/> Allow cron jobs<br/>
        <input type="checkbox" name="<?php echo NIMP_OPTION_NAME; ?>[suspend_imports]" value="true" <?php echo $suspend_checked; ?>/> Suspend imports

<?php //   } ?>
        <p class="submit">
<!--            <input id="submit-mls" class="button-primary" type="submit" value="import_settings" onclick="document.pressed = this.value" name="submit_button" />-->
            <button type="submit" id="submit-import-settings" class="button-primary" name="submit_button" value="import_settings" >Save Import Settngs</button>
            
        </p><br/><br/>
                            
        <div class="clear"></div>
        </fieldset>
<?php
        }   // function settings_import

    static function settings_nrl_fields() {
        // Display the field settings

        $field_settings = NRL_Common::get_nrl_setting('field_settings');
        if ( empty($field_settings) ) {
            NIMP_Fields::create_field_settings();
            $field_settings = NRL_Common::get_nrl_setting('field_settings');
        }
            ?>
    <fieldset class="nrl_settings_group">
        <div class="nrl_tab_content">
        <div class="clear"></div>
        <div class="nrl_field_settings">
            <p><b>** These field settings are not editable yet! **</b></p>
            <div class="nrl_field_settings_titles clearfix">
                <div class="nrl_field_name"><?php echo 'Field name' ?></div>
                <div><?php echo 'Section';?></div>
                <div><?php echo 'Layout';?></div>
                <div class="field_setting_wide"><?php echo 'Label';?></div>
                <div><?php echo 'Empty';?></div>
                <div><?php echo 'Order';?></div>
            </div>
            <?php
            foreach ( $field_settings as $field => $settings ) {
                ?>
            <div class="clearfix" onclick="nimp_edit_field_setting(this);">
<!--                <button>Edit</button>-->
                <div class="nrl_field_name"><?php echo $field; ?></div>
                <div><?php echo $settings['section'];?></div>
                <div><?php echo $settings['layout'];?></div>
                <div class="field_setting_wide"><?php echo $settings['label'];?></div>
                <div><?php echo $settings['empty'];?></div>
                <div><?php echo $settings['order'];?></div>

            </div>
                <?php
            }
            ?>
        </div>
        </div>
    </fieldset>
    
<?php        
    }        
        
        
    static function settings_setup() {
        // Display the plugin setup status and action buttons
         
        echo "Complete the following tasks in order. They must all be completed successfully (green text) for the plugin to work correctly.<br/>";
        echo "Some of the MLS import functions can take <b>up to 10 minutes</b> to complete. Please be patient. Do not click another button until you see the Response Message appear below.<br/><br/>";
        echo self::import_status();
    }
    
    static function ajax_setup() {
        // Process AJAX requests from the admin NRL Imports Setup options
        // Returns via JSON the result (true/false), message, and any errors output to html

        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        if ( empty($post['nimp_task']) ) {
            die();
        }
        
        require_once NIMP_PATH . 'library/class-nimp-install.php';
        require_once NIMP_PATH . 'library/class-MLS.php';
        require_once NIMP_PATH . 'library/class-MLS-util.php';
        $mris = new NIMP_MLS();
        
        $result = false;
        $details = '';
        ob_start();
        switch ( $post['nimp_task'] ) {
            case 'tables':
                $result = NIMP_Install::create_tables(1);
                if ( $result ) {
                    $details = "+ Tables(1) have been created/updated ** Reload this page **<br/>";
                }
                break;
            case 'tables2':
                $result = NIMP_Install::create_tables(2);
                if ( $result ) {
                    $details = "+ Tables(2) have been created/updated ** Reload this page **<br/>";
                }
                break;
            case 'fields_sales':
                $details = $mris->import_fields('sales') . '<br/>';
                $result = true;
                break;
            case 'fields_rentals':
                $details = $mris->import_fields('rentals') . '<br/>';
                $result = true;
                break;
            case 'fields_media':
                $details = $mris->import_media_fields() . '<br/>';
                $result = true;
                break;
            case 'lookup_property':
                ini_set( 'max_execution_time', 600 );
                $details = $mris->import_mls_lookups( 'property' ) . '<br/>';
                $result = true;
                break;
            case 'lookup_media':
                $details = $mris->import_mls_lookups( 'media' ) . '<br/>';
                $result = true;
                break;
            case 'field_settings':
                $fields = NIMP_Util::csv_file_get_assoc( NIMP_PATH . 'setup/' . NIMP_FIELDS_OPTION_NAME .".csv", true);
                $temp = 1;
                if ( ! empty($fields) ) {
                    NRL_Common::update_nrl_setting( 'field_settings', $fields );
                    $details = '+ Field settings csv file imported<br/>';
                    $result = true;
                } else {
                    $details = ' - Unable to import field settings csv file<br/>';
                }
                break;
            case 'import_rental_zips':
                $result = self::import_setting( 'rental_zips');
                if ( $result !== false ) {
                    $details = "+ Imported/updated $result rental zips<br/>";
                } else {
                    $details = '- Failed to import rental zips<br/>';
                }
                break;
            case 'import_polygons':
                $result = self::import_setting( 'polygons');
                if ( $result !== false ) {
                    $details = "+ Imported/updated $result polygons<br/>";
                } else {
                    $details = '- Failed to import polygons<br/>';
                }
                break;
            case 'import_factoids':
                $result = self::import_setting( 'factoid_text');
                if ( $result !== false ) {
                    $details = "+ Imported/updated $result factoids<br/>";
                } else {
                    $details = '- Failed to import factoids<br/>';
                }
                break;                 
            case 'search_fields':
                $fields = NIMP_Util::csv_file_get_assoc( NIMP_PATH . 'setup/' . NIMP_SEARCH_OPTION_NAME .".csv", true);
                $temp = 1;
                if ( ! empty($fields) ) {
                    NRL_Common::update_nrl_setting( 'search_fields', $fields );
                    $result = true;
                    $details = '+ Search fields csv file imported<br/>';
                } else {
                    $details = ' - Unable to import search fields csv file<br/>';
                }
                break;
            case 'prop_type_text':
                $fields = NIMP_Util::csv_file_get( NIMP_PATH . 'setup/' . NIMP_PROP_TYPE_OPTION_NAME .".csv", true, true);
                if ( ! empty($fields) ) {
                    NRL_Common::update_nrl_setting( 'prop_type_text', $fields );
                    $result = true;
                    $details = '+ Prop type text csv file imported<br/>';
                } else {
                    $details = ' - Unable to import prop type text csv file<br/>';
                }
                break;
        }
        
        if ( ! empty($mris) ) {
            $mris->disconnect();
        }
        $message = self::import_status_messages( $post['nimp_task'] , $result );
        $details = $details . ob_get_clean();
        $response = array( 'result' => ($result ? 'true' : 'false' ), 'message' => $message, 'details' => $details );
        
        echo json_encode( $response, JSON_UNESCAPED_SLASHES );
        die();
    }
    
    static function ajax_tools() {
        // Process AJAX requests from the admin NRL Imports Tools options
        // Returns via JSON whether the job is finished and the message
        
        ob_start();     // Capture any error messages
        $response = array( 
            'finished'=> 'true', 
            'message'=>'' ,
        );
        
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        switch ( $post['nimp_task'] ) {
            case 'imports_sales':
                $mris = new NIMP_MLS;
                $mris->import_mls( 'sales');
                // results will be returned by ob_get_clean()
                break;
            case 'imports_rentals':
                $mris = new NIMP_MLS;
                $mris->import_mls( 'rentals' );
                // results will be returned by ob_get_clean()
                break;
            case 'export_polygons':
                $response['message'] = $post['nimp_task'] . ' not implemented yet<br/>';
                break;
            case 'maintenance':
                $response['message'] = $post['nimp_task'] . ' not implemented yet<br/>';
                break;
            case 'test':
                require_once NIMP_PATH . 'library/class-nimp-test.php';
                NIMP_Test::test();
                // results will be returned by ob_get_clean()
                break;
        }
        $response['message'] .= ob_get_clean();
        
        echo json_encode( $response, JSON_UNESCAPED_SLASHES );
        die();
    }

    static function import_setting( $setting, $merge=true ) {
        // Import an nrl setting from a file
        // If $merge is true (default), merge the imported settings with any existing settings
        // Returns the number of array elements in the setting, or false on failure
        
        $data = file_get_contents( NIMP_PATH . "setup/$setting.txt" );
        if ( ! empty($data) ) {
            $contents = unserialize( $data );
            // See if the setting is already defined            
            $old_setting = NRL_Common::get_nrl_setting( $setting );
            if ( ! empty($old_setting) && ! empty($contents) ) {
                $contents = array_merge( $old_setting, $contents );
                // If this the array uses numeric keys, return only unique elements
                if ( ! empty($contents[0]) ) {
                    $contents = array_unique( $contents );
                }
            }
            NRL_Common::update_nrl_setting( $setting, $contents );
            return count($contents);
        } else {
            return false;
        }
    }

    static function ajax_debug() {
        // Process AJAX requests from the admin NRL Imports Debug options
        // Returns via JSON whether the job is finished and the message
        
        ob_start();     // Capture any error messages
        $response = array( 
            'finished'=> 'true', 
            'message'=>'' ,
        );
        
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        switch ( $post['nimp_task'] ) {
            case 'get_setting':
                $settings = NRL_Common::get_nrl_setting( $post['nrl_setting'] );
                $response['message'] = "Contents of nrl_setting: " . $post['nrl_setting'] . '<br/>';
                if ( is_array($settings) ) {
                    foreach ( $settings as $key => $setting ) {
                        if ( is_array($setting) ) {
                            $response['message'] .= $key . '=> Array(' . count($setting) . ')<br/>';
                        } else {
                            $response['message'] .= $key . ' => ' . $setting . '<br/>';
                        }
                    }
                }
                $response['message'] .= '<br/>';
                // results will be returned by ob_get_clean()
                break;
            case 'get_options':
                $my_options = NIMP_Util::get_options();
                // Continue to the code below...
            case 'get_maint_options':
                if ( empty($my_options) ) {
                    $my_options = get_option( NIMP_MAINT_OPTION_NAME );
                }
                
                // Get the plugin options
//                if ( empty(self::$options)) {
//                    self::$options = NIMP_Util::get_options();
//                }
                $response['message'] = 'Contents of options:<br/>';
                if ( is_array($my_options) ) {
                    foreach ( $my_options as $key => $option ) {
                        if ( is_array($option) ) {
                            $response['message'] .= $key . '=> Array(' . count($option) . ')<br/>';
                        } else {
                            $response['message'] .= $key . ' => ' . $option . '<br/>';
                        }
                    }
                }
                $response['message'] .= '<br/>';                
                break;
            default:
                $response['message'] = 'Unrecognized debug tab task: ' . $post['nimp_task'];
                break;
        }
        
        $response['message'] .= ob_get_clean();
        
        echo json_encode( $response, JSON_UNESCAPED_SLASHES );
        die();
    }
    
    
    static function import_status() {
        // Returns the html to display the import status and action buttons
        
        require_once NIMP_PATH . 'library/class-nimp-install.php';

        global $wpdb;
        
        ob_start();
        // Check status of installation tasks
        $status = array();
        $status['tables1'] = NIMP_Install::check_tables(1);
        $status['tables2'] = NIMP_Install::check_tables(2);
?>   
            <p id="nimp_setup_tables"><button type="button" onclick="nimpSetup('tables')">Create/update tables</button>
                <span class="nrl_import_status"><?php echo self::import_status_messages('tables', $status['tables1']); ?></span></p>
<?php   if ( $status['tables1'] ) {
            $count = $wpdb->get_results( "SELECT COUNT(*) FROM $wpdb->nrl_fields_sales", ARRAY_A );
            $status['fields_sales'] = ( $count[0]['COUNT(*)'] > 100 ? true : false );
            $count = $wpdb->get_results( "SELECT COUNT(*) FROM $wpdb->nrl_fields_rentals", ARRAY_A );
            $status['fields_rentals'] = ( $count[0]['COUNT(*)'] > 100 ? true : false );
            $status['field_formats_media'] = NRL_Common::get_nrl_setting('field_formats_media');
            // The following two settings should be set when the MLS fields list is imported
            $status['import_fields'] = NRL_Common::get_nrl_setting('import_fields');
            $status['field_formats'] = NRL_Common::get_nrl_setting('field_formats');

//            $status['fields'] =  $status['fields'] && ! empty($status['import_fields']) && ! empty($status['field_formats']);
?>          
            <p id="nimp_setup_fields_sales"><button type="button" id="nimp_setup_fields+sales" onclick="nimpSetup('fields_sales')">Import MLS sales field list</button>
                <span class="nrl_import_status"><?php echo self::import_status_messages('fields_sales', $status['fields_sales']); ?></span></p>
            <p id="nimp_setup_fields_rentals"><button type="button" id="nimp_setup_field_rentals" onclick="nimpSetup('fields_rentals')">Import MLS rentals field list</button>
                <span class="nrl_import_status"><?php echo self::import_status_messages('fields_rentals', $status['fields_rentals']); ?></span></p>
            <p id="nimp_setup_fields_media"><button type="button" id="nimp_setup_fields_media" onclick="nimpSetup('fields_media')">Import MLS media field list</button>
                <span class="nrl_import_status"><?php echo self::import_status_messages('fields_media', $status['field_formats_media']); ?></span></p>            
            <br/><p id="nimp_setup_tables2"><button type="button" onclick="nimpSetup('tables2')">Create Properties and Lookup Tables</button>
                <span class="nrl_import_status"><?php echo self::import_status_messages('tables', $status['tables2']); ?></span></p>              
<?php            
            // If the fields table exists, we can create the rest of the tables..
            if ( $status['tables2'] ) {
                $count = $wpdb->get_results( "SELECT COUNT(*) FROM $wpdb->nrl_lookup", ARRAY_A );
                $status['lookup_property'] = ( $count[0]['COUNT(*)'] > 100 ? true : false );
                $lookups_media = NRL_Common::get_nrl_setting('lookups_media');
                $status['lookup_media'] = ( ! empty( $lookups_media ) );
                $field_settings = NRL_Common::get_nrl_setting('field_settings');
                $status['field_settings'] = ( ! empty( $field_settings ) );
                $status['rental_zips'] = NRL_Common::get_nrl_setting('rental_zips');
                $status['polygons'] = NRL_Common::get_nrl_setting('polygons');
                $status['factoid_text'] = NRL_Common::get_nrl_setting('factoid_text');                
                $search_fields = NRL_Common::get_nrl_setting('search_fields');
                $status['search_fields'] = ( ! empty( $search_fields ) );
                $prop_type_text = NRL_Common::get_nrl_setting('prop_type_text');
                $status['prop_type_text'] = ( ! empty( $prop_type_text ) );
?>          
            <p id="nimp_setup_lookup_property"><button type="button" onclick="nimpSetup('lookup_property')">Import MLS Property lookup list (this will take several minutes)</button>
                <span class="nrl_import_status"><?php echo self::import_status_messages('lookup_property', $status['lookup_property']); ?></span></p>
            <p id="nimp_setup_lookup_media"><button type="button" onclick="nimpSetup('lookup_media')">Import MLS Media lookup list</button>
                <span class="nrl_import_status"><?php echo self::import_status_messages('lookup_media', $status['lookup_media']); ?></span></p>
            <p id="nimp_setup_field_settings"><button type="button" onclick="nimpSetup('field_settings')">Update Field Settings</button>
                <span class="nrl_import_status"><?php echo self::import_status_messages('field_settings', $status['field_settings']); ?></span></p>

            <p id="nimp_setup_rental_zips"><button type="button" onclick="nimpSetup('import_rental_zips')">Update Rental Zips</button>
                <span class="nrl_import_status"><?php echo self::import_status_messages('rental_zips', $status['rental_zips']); ?></span></p>
            <p id="nimp_setup_polygons"><button type="button" onclick="nimpSetup('import_polygons')">Update Polygons</button>
                <span class="nrl_import_polygons"><?php echo self::import_status_messages('polygons', $status['polygons']); ?></span></p>
            <p id="nimp_setup_factoid_text"><button type="button" onclick="nimpSetup('import_factoids')">Update Factoid Text</button>
                <span class="nrl_import_status"><?php echo self::import_status_messages('factoid_text', $status['factoid_text']); ?></span></p>
            
            <p id="nimp_setup_search_fields"><button type="button" onclick="nimpSetup('search_fields')">Update Search Fields</button>
                <span class="nrl_import_status"><?php echo self::import_status_messages('search_fields', $status['search_fields']); ?></span></p>
            <p id="nimp_setup_prop_type_text"><button type="button" onclick="nimpSetup('prop_type_text')">Update Property Type Text</button>
                <span class="nrl_import_status"><?php echo self::import_status_messages('prop_type_text', $status['prop_type_text']); ?></span></p>
<?php            
            }
        } else {
            echo "<br/>Create tables first, then reload this page to see the other options...<br/>";
        }
?>
            <br/>
            <h3>Response Messages:</h3>
            <div id="nrl_setup_details" class="nimp_admin_log"></div>
            <div id='nrl_progress_notice'>This is a hidden notice</div>
<?php            
        return ob_get_clean();
    }
    
    static function import_status_messages( $type, $result ) {
        // Returns the html for the appropriate status message for $type and $result
        // Used by import_status
        
        switch ( $type ) {
            case 'tables':
            case 'tables2':
                $message = ( $result ? 'Tables have been created' : 'Tables have not been created' );
                break;
            case 'fields_sales':
            case 'fields_rentals':
            case 'fields_media':
                $message = ( $result ? 'Field list has been imported' : 'Field list has not been imported' );
//                $message = 'This is a test';
                break;
            case 'lookup_property':
            case 'lookup_media':
                $message = ( $result ? 'Lookup list has been imported' : 'Lookup list has not been imported' );
                break;
            case 'field_settings':
            case 'rental_zips':
            case 'polygons':
            case 'factoid_text':
                $message = ( $result ? 'Settings have been imported' : 'Settings have not been imported' );
                break;
            case 'search_fields':
                $message = ( $result ? 'Search fields have been imported' : 'Search fields have not been imported' );
                break;
            case 'prop_type_text':
                $message = ( $result ? 'Property type title text has been imported' : 'Property type title text has not been imported' );
                break;
            default:
                $message = 'Invalid setup task';
        }
        $class = ( $result ? 'nrl_success' : 'nrl_failure' );
        $message = '<span class="' . $class . '">' . $message . '</span>';

        return $message;
    }
    
    
    static function settings_polygon_search() {

        echo "This fuction is not yet available";
        return;
        
        $poly_default = array( array() );
//            'name' => 'My Test Area',
//            'slug'  =>  'my-test-area',
//            'vertices' => '(38.81721401500388, -77.06598043441772),(38.80825220755225, -77.08984136581421),(38.77266137104663, -77.08417654037476),(38.78189528732685, -77.06237554550171),(38.80009196944943, -77.05791234970093)'
//        ) );
//        update_option(NIMP_POLYGON_OPTION_NAME, array());
        $polygons = get_option(NIMP_POLYGON_OPTION_NAME, $poly_default); 
        $polygons = NRL_Common::get_nrl_setting('polygons');
//        $polygons[] = array('name'=>'Add new polygon', 'vertices'=>'', 'slug'=>'');
        // Add element to front of array
        array_unshift($polygons, array('name'=>'Add new polygon', 'vertices'=>''));
        $slugs = array();
        ?>
<!--        <p><i>The Simple Real Estate Pack plugin interferes with the display of the maps on this tab, so that plugin should be temporarily disabled to add or edit a Polygon.</i></p>-->
        <h3>Polygon Area</h3>
        <div class="nrl_admin_left" id="nrl_poly_form">
        <?php echo self::format_polygon_form($polygons); ?>
        </div>
        <div id="NIMP_Admin_Map" class="nrl_map"></div>
        <?php
        foreach ( $polygons as $slug => $polygon ) {
            $slugs[] = $slug;
        }        
        self::load_map($polygons, $slugs);
    }   // end function settings_polygon_search()
    
    static function format_polygon_form( $polygons, $selected=0 ) {
        // Returns the form used to manage polygon areas
        // $selected is the slug of the option to be selected
        $temp=1;
        if ( ! isset($polygons[0])) {
            array_unshift($polygons, array('name'=>'Add new polygon', 'vertices'=>''));
        }
        if ( ! isset($polygons[$selected])) {
            $selected = 0;
        }
        ob_start();
        ?>
        <form method="post" accept-charset="utf-8" id="map_form">
            <select name="poly_name" id="nrl_poly_select" >
            <?php
            foreach ( $polygons as $slug => $polygon ) {
                echo "<option value='$slug' id='nrl-$slug'";
                if ( $slug === $selected ) {
                    echo ' selected=selected';
                }
                echo ">{$polygon['name']}</option>";
            }
            ?>
            </select><br/>
            Name: <input type="text" name="name" value="<?php echo ( $selected!==0 ? $polygons[$selected]['name'] : ''); ?>" id="nrl_name"  />
            <input type="hidden" name="slug" value="<?php echo $selected; ?>" id="nrl_slug"  />
            <input type="hidden" name="vertices" value="<?php //echo $polygons[$selected]['vertices']; ?>" id="nrl_vertices"  />
            <br/><button name="save" type="button" id="nrl_save_polygon" onclick="nrlSavePolygon()" >Save</button>
            <button name="clear" type="button" id="nrl-clear-polygon" onclick="nrlClearPolygon()">Clear Polygon</button>
            <button name="delete" type="button" id="nrl-delete-polygon" onclick="nrlDeletePolygon()">Delete</button>
            <?php if ( $selected !== 0 ) {
               // echo '<br/><br/>Shortcode: [nrl-listings polygon="' . $selected .'"]';
            } ?>
            
        </form>
        <div id="nrl_shortcode">
            <?php if ( $selected !== 0 ) {
                echo '<p>Shortcode: <br/>[nrl-listings polygon="' . $selected .'"]</p>'; 
            }?>
        </div>
        <?php
        $result = ob_get_clean();
        return $result;
    }   // end function format_polygon_form()
    
    static function load_map($polygons, $slugs) {
        // Load Google Maps API for drawing polygon searches
        
        // Don't let this run more than once
        if ( self::$poly_map_init ) {
            return;
        }
        self::$poly_map_init = true;
        
        // Note: some of this code overlaps NRL_Display::show_map()
        $map_name = 'NIMP_Admin_Map';
       ?>
        <script type="text/javascript">
        var AdminMap;
        var nrlDrawingManager;
        var polygonShapes = [];
        var polygons = <?php echo json_encode($polygons, JSON_UNESCAPED_SLASHES); ?>;        
        var slugs = <?php echo json_encode($slugs, JSON_UNESCAPED_SLASHES); ?>;     
        var nrlChanged = false;
        function initAdminMap() {
            console.log("Initializing Admin map ");
            var myLatLng = {lat: 38.791365000000 , lng: -77.049944000000};
            AdminMap = new google.maps.Map(document.getElementById("NIMP_Admin_Map"), {
                center: myLatLng,
                zoom: 13,
                scrollwheel: false
            });        
            nrlDrawingManager = new google.maps.drawing.DrawingManager({
                drawingMode: google.maps.drawing.OverlayType.POLYGON,
  				drawingControl: true,
  				drawingControlOptions: {
//  					position: google.maps.ControlPosition.TOP_CENTER,
					drawingModes: [google.maps.drawing.OverlayType.POLYGON]
				},
                polygonOptions: {
                    editable: true
                }    
            });
            nrlDrawingManager.setMap(AdminMap);
            google.maps.event.addListener(nrlDrawingManager, "overlaycomplete", function(event) {
				newShape = event.overlay;
				newShape.type = event.type;
                polygonShapes.push(newShape);
			});

            google.maps.event.addListener(nrlDrawingManager, "overlaycomplete", function(event){
                overlayClickListener(event.overlay);
                jQuery('#nrl_vertices').val(event.overlay.getPath().getArray());
                nrlChanged = true;

            });
        }
        
        function overlayClickListener(overlay) {
            google.maps.event.addListener(overlay, "mouseup", function(event){
                console.log("overlayClickListener triggered");
                jQuery('#nrl_vertices').val(overlay.getPath().getArray());
                nrlChanged = true;
            });
        }
//        initialize();

//        jQuery(function(){
//            jQuery('#save').click(function(){
//                console.log("Save pressed");
//                //iterate polygon vertices?
//            });
//        });
        
        function nrlClearPolygon() {
//            console.log('Clearing polygon');
            for (var i = 0; i < polygonShapes.length; i++) {
                polygonShapes[i].setMap(null);
            }
            polygonShapes = [];
        }
        
        function nrlSavePolygon() {
            // Check to see if the name and coordinates are filled in
            if ( jQuery('#nrl_name').val() === '' || ( jQuery('#nrl_vertices').val() === ''  && jQuery('#nrl_slug').val() === '0' ) ) {
                alert('You must provide a name and draw a polygon');
                return false;
            }
            var polyName = jQuery('#nrl_name').val();
            var polySlug = jQuery('#nrl_slug').val();
//            alert('Slug is ' + polySlug);
            center = AdminMap.getCenter();
//            alert('Polygon ' + poly_name + ' will be saved.  Center is ' + center );

            var data = {
                action: 'save_polygon',
                poly_name: jQuery('#nrl_name').val(),
                poly_slug: jQuery('#nrl_slug').val(),
                poly_vertices: jQuery('#nrl_vertices').val(),
                poly_center: center.lat() + ',' + center.lng() ,
                poly_zoom: AdminMap.getZoom(),
                poly_test: 'This is a test'
            };
            // the_ajax_script.ajaxurl is a variable that will contain the url to the ajax processing file
            console.log('Sending AJAX request to save polygon ' + polyName);
            jQuery.post(NIMP_admin_AJAX.ajaxurl, data, function(response) {
//                alert(response);
                response = JSON.parse(response);
                if ( response['msg'].length > 0 ){
                    alert(response['msg']);
                }
                if ( response['select'].length > 0 ) {
                    jQuery('#nrl_poly_form').html(response['select']);
                    nrlSetFormEvents();
                }
                polygons = response['polygons'];

            });
//            jQuery("#nrl_poly_select").val(polySlug);
            nrlChanged = false;
            return false;            
        }
        
        function nrlDeletePolygon() {
            polyName = jQuery('#nrl_name').val();
            resp = confirm("Are you sure that you want to delete polygon area " + polyName + '?');
            if ( resp === false ) {
                return false;
            }
            var data = {
                action: 'delete_polygon',
                poly_name: jQuery('#nrl_name').val(),
                poly_slug: jQuery('#nrl_slug').val(),
//                post_var: 'this will be echoed back'
            };
            // the_ajax_script.ajaxurl is a variable that will contain the url to the ajax processing file
            console.log('Sending AJAX request to delete polygon ' + polyName);
            // Clear the polygon from the map
            nrlClearPolygon();
            jQuery.post(NIMP_admin_AJAX.ajaxurl, data, function(response) {
//                console.log('Response is type ' + typeof response );
                response = JSON.parse(response);
                if ( response['msg'].length > 0 ){
                    alert(response['msg']);
                }
                if ( response['select'].length > 0 ) {
                    jQuery('#nrl_poly_form').html(response['select']);
                    nrlSetFormEvents();
                }
                polygons = response['polygons'];
            });
                
            // Refresh poly list...
            jQuery("#nrl_poly_select").val('0');
            return false;  
    }
        
        function resetMap(myMap, zoom, center) {
            // Reset the map
            // zoom and center are optional parameters
//            console.log("Reset map called");
            if ( ! zoom ) {
                zoom = myMap.getZoom();
            }
            if ( ! center ) {
            center = myMap.getCenter();
            }
//             alert('Map zoom is ' + z + ', Center is ' + c);
            console.log( "Resetting map at center " + center + ", zoom " + zoom );
             google.maps.event.trigger(myMap, 'resize');
             myMap.setZoom(zoom);
             myMap.setCenter(center);
       }
           
       function nrlSetFormEvents() {
           // Set up the events triggered by changes in the form on the Polygons page
        
            // Detect change of name
            jQuery(document).on('change','#nrl_name',function(){
                nrlChanged = true;
            });

            // Detect select of a new polygon
            jQuery(document).on('change','#nrl_poly_select',function(){
    //               alert( this.value );
                // Check to see if there are unsaved changes
                if ( nrlChanged ) {
    //                alert('Unsaved changes!');
                    resp = confirm('You have unsaved changes! Discard changes and proceed?');
                    if ( resp === false ) {
                        mySlug = jQuery('#nrl_slug').val();
                        jQuery("#nrl_poly_select").val(mySlug);
    //                    optId = 'nrl-' + jQuery('#nrl_slug').val();
    //                    jQuery('#optId').attr('selected','selected');
                        return false;
                    }
                    nrlChanged = false;
                }

               nrlClearPolygon();
               polySlug = this.value;
                if ( polySlug === '0') {
                    // This is a new polygon
                    jQuery('#nrl_name').val('');
                    jQuery('#nrl_slug').val('');
                    jQuery('#nrl_vertices').val('');
                    jQuery('#nrl_shortcode').html('');
                } else {
                    jQuery('#nrl_name').val( polygons[polySlug]['name'] );
                    jQuery('#nrl_slug').val( polySlug );
//                    jQuery('#nrl_vertices').val( polygons[polySlug]['vertices'] );
                    jQuery('#nrl_vertices').val('');
                    jQuery('#nrl_shortcode').html('<p>Shortcode: <br/>[nrl-listings polygon="' + polySlug +'"]</p>');
                    zoom = parseInt(polygons[polySlug]['zoom']);

    //            if ( !empty(polygons[polySlug]['center'])){
                    center = polygons[polySlug]['center'].split(',');
                    center = new google.maps.LatLng(center[0], center[1]);
    //            } else {
    //                center = '';
    //            }
                    resetMap(AdminMap,zoom,center);

                    // Parse the vertices and build the polygon
//                    polyVertices = polygons[polySlug]['vertices'].replace(/\),\(/g,")!(").split('!')
//                    var regionCoords = [];
//                    for(var i=0 ; i < polyVertices.length ; i++) {
//                        coord = polyVertices[i].substring(1,polyVertices[i].length-2).split(',');
//                        regionCoords.push( new google.maps.LatLng(coord[0], coord[1]) );
//                    }
                    
                    var regionCoords = [];
                    for(var i=0 ; i < polygons[polySlug]['vertices'].length ; i++) {
                        regionCoords.push( new google.maps.LatLng(polygons[polySlug]['vertices'][i][0], polygons[polySlug]['vertices'][i][1]) );
                    }

                    // Construct the polygon.
    //                console.log('Drawing polygon. Number of points = ' + regionCoords.length);
                    console.log('Drawing polygon. Number of points = ' + regionCoords.length);
                    var newShape = new google.maps.Polygon({
                        paths: regionCoords
    //                    strokeColor: '#FF0000',
    //                    strokeOpacity: 0.8,
    //                    strokeWeight: 3,
    //                    fillColor: '#FF0000',
    //                    fillOpacity: 0.35
                    });
                    newShape.setMap(AdminMap);
                    polygonShapes.push(newShape);
                }
                return false;
              });
        
       }    // end function nrlSetFormEvents()
        </script>
        <script type="text/javascript" async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCY4Ocjm5wrB-fz4e9kKPDtQRk5gVY8Blc&libraries=drawing&callback=initAdminMap"></script>
<?php //return; ?>        
        <script>
        jQuery( document ).ready(function() {
//        console.log( "ready!" );
//        var mTab = document.getElementById("nrl_map").parentElement.parentElement.parentElement;
        // Find out which tab content section the map is in
        var myTab = jQuery("#<?php echo $map_name; ?>").parent();
        var myTabIndex = myTab.index() - 1;
        // Find the tab for this tab content section
        var MapTab = jQuery("#kxc-tab-list > li:eq(" + myTabIndex + ")");
//        alert('Tab index is: ' + myTabIndex + '  Map tab is: ' + MapTab.prop("tagName"));
        // Set up a click function to reveal the map when that tab is clicked
        MapTab.click(function(){
            // Reset the map so that it appears correctly
//            alert('Map tab was clicked!');
    <?php // If Google Maps API was loaded by dsidxpress, initialize it
        if ( defined('DSIDXPRESS_OPTION_NAME')) { ?>
            console.log("Ready to initialize Admin map at tab click");
            initAdminMap();
//            initMap(nrlMaps[<?php //echo self::$google_maps_cnt; ?>]);
        <?php  } else { ?>
            resetMap(AdminMap);
        <?php } ?>            
        });

        // Setup form events
        nrlSetFormEvents();
        
        // NO LONGER USED On the Polygon tab, determine which defined polygon in the list is clicked:
        jQuery("ul[id='nrl_poly_list'] li").click(function () {
//            alert(jQuery(this).html()); // gets innerHTML of clicked li
//            alert(jQuery(this).text()); // gets text contents of clicked li
            polyIndex = jQuery(this).index(); // gets text contents of clicked li
            jQuery("ul[id='nrl_poly_list'] li").css("background-color","inherit");
            jQuery(this).css("background-color","#DDDDDD");
            nrlClearPolygon();
            polySlug = slugs[polyIndex];
//            alert('Index is ' + polyIndex + '   slug is ' + polySlug);
            if ( polygons[polySlug]['vertices'] === '' ) {
                // This is a new polygon
                jQuery('#nrl_name').val('');
                jQuery('#nrl_slug').val('');
                jQuery('#nrl_vertices').val('');
            } else {
                jQuery('#nrl_name').val( polygons[polySlug]['name'] );
                jQuery('#nrl_slug').val( polySlug );
                jQuery('#nrl_vertices').val( polygons[polySlug]['vertices'] );
                zoom = parseInt(polygons[polySlug]['zoom']);
                
//            if ( !empty(polygons[polySlug]['center'])){
                center = polygons[polySlug]['center'].split(',');
                center = new google.maps.LatLng(center[0], center[1]);
//            } else {
//                center = '';
//            }
                resetMap(AdminMap,zoom,center);

                // Parse the vertices and build the polygon
                polyVertices = polygons[polySlug]['vertices'].replace(/\),\(/g,")!(").split('!')
                var regionCoords = [];
                for(var i=0 ; i < polyVertices.length ; i++) {
                    coord = polyVertices[i].substring(1,polyVertices[i].length-2).split(',');
                    regionCoords.push( new google.maps.LatLng(coord[0], coord[1]) );
                }

                // Construct the polygon.
//                console.log('Drawing polygon. Number of points = ' + regionCoords.length);
                console.log('Drawing polygon. Number of points = ' + regionCoords.length);
                var newShape = new google.maps.Polygon({
                    paths: regionCoords
//                    strokeColor: '#FF0000',
//                    strokeOpacity: 0.8,
//                    strokeWeight: 3,
//                    fillColor: '#FF0000',
//                    fillOpacity: 0.35
                });
                newShape.setMap(AdminMap);
                polygonShapes.push(newShape);

            }
            
        });    

    });
        </script>

        <?php
       
    }
    
    static function ajax_polygon() {
        // Process the ajax call from the create polygon map
        if ( empty(self::$polygons) ) {
            self::$polygons = get_option(NIMP_POLYGON_OPTION_NAME);
        }
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        $response = '';
        if ( ! empty($post['action'])) {
            switch($post['action']) {
                case 'save_polygon':
                    $response = self::ajax_save_polygon($post);
                    break;
                case 'delete_polygon':
                    $response = self::ajax_delete_polygon($post);
                    break;
            }
        }
        echo json_encode($response, JSON_UNESCAPED_SLASHES);
        die();
        
    }   // end function ajax_polygon
    
    static function ajax_save_polygon($post) {
        // Make sure that the polygon is defined
        $temp = 1;
        if ( empty($post['poly_name'])  || ( ! isset(self::$polygons[$post['poly_slug']]) && empty($post['poly_vertices']) ) ) {
            return array('msg' => 'Polygon is not defined', 'select' => '', 'polygons' => self::$polygons);
        }
//        $response = '';
        // If the slug is empty, this is a new polygon
        if ( empty($post['poly_slug']) ) {
            // Set a new slug and make sure it is unique
            $slug = str_replace(' ', '-', strtolower($post['poly_name']));
            $slug = str_replace("&#39;",'',$slug);
            $temp = 1;
//            $temp2 = substr($slug,0,strlen($slug)-2);
            while ( ! empty (self::$polygons[$slug]) ) {
                $temp = 1;
                if ( is_numeric(substr($slug,strlen($slug)-2)) ) {
                    $numb = substr($slug,strlen($slug)-2);
                    $slug = substr($slug,0,strlen($slug)-2) . str_pad($numb+1,2,'0',STR_PAD_LEFT);
                } else {
                    $slug = $slug . '-01';
                }
            }
        } else {
            $slug = $post['poly_slug'];
        }
        
        // Save the polygon
        if ( ! isset(self::$polygons[$slug]) ) {
            self::$polygons[$slug] = array();
        }
        self::$polygons[$slug]['name'] = $post['poly_name'];
        self::$polygons[$slug]['center'] = $post['poly_center'];
        self::$polygons[$slug]['zoom'] = $post['poly_zoom'];
//            'center'    => $post['poly_center'],
//            'zoom'      => $post['poly_zoom'],
//            'lat-range' => $lat_minmax,
//            'long-range' => $long_minmax,
//        );
        
        // Save the vertices if they have changed or if this is a new polygon
        if ( ! empty($post['poly_vertices']) ) {
            // Parse the vertices and calculatew the min/max lat and long
            $lat_minmax = array('min' => 999999, 'max' => -999999);
            $long_minmax = array('min' => 999999, 'max' => -999999);
            $vert_list = substr( $post['poly_vertices'], 1, strlen($post['poly_vertices'])-2 );
            $vert_list = explode('),(', $vert_list);
            $vertices = array();
            foreach ($vert_list as $vertex ) {
                $parts = explode(',', $vertex);
                $vertices[] = $parts;
                if ( (float)$parts[0] < $lat_minmax['min'] )
                    $lat_minmax['min'] = (float)$parts[0];
                if ( (float)$parts[0] > $lat_minmax['max'] )
                    $lat_minmax['max'] = (float)$parts[0];
                if ( (float)$parts[1] < $long_minmax['min'] )
                    $long_minmax['min'] = (float)$parts[1];
                if ( (float)$parts[1] > $long_minmax['max'] )
                    $long_minmax['max'] = (float)$parts[1];            
            }
            self::$polygons[$slug]['lat-range'] = $lat_minmax;
            self::$polygons[$slug]['long-range'] = $long_minmax;
            self::$polygons[$slug]['vertices'] = $vertices;
        }
        $temp = 1;
        
//        // Save the polygon
//        $temp = 1;
//        self::$polygons[$slug] = array(
//            'name'      => $post['poly_name'],
//            'vertices'  => $post['poly_vertices'],
//            'center'    => $post['poly_center'],
//            'zoom'      => $post['poly_zoom'],
//            'lat-range' => $lat_minmax,
//            'long-range' => $long_minmax,
//        );
        
        // Sort the polygon array
        uasort(self::$polygons, NIMP_Admin::poly_sort('name') );
        $result = update_option(NIMP_POLYGON_OPTION_NAME, self::$polygons);
        if ( ! $result ) {
            NIMP_Util::error_log('Failed to update polygon options');
        }
        $temp = 1;
        
        // Update the select list
        $select = self::format_polygon_form(self::$polygons, $slug);
        $response = array('msg' => 'Polygon saved', 'select' => $select, 'polygons' => self::$polygons);
//        $response = 'Received data for polygon ' . $_POST['poly_name'];
        // send the response back to the front end
//        echo $response;
        return $response;
    }   // end function ajax_save_polygon()
    
    static function poly_sort($key) {
        // Used to sort $polygons array
        return function ($a, $b) use ($key) {
            return strnatcmp($a[$key], $b[$key]);        
        };
    }
    
    static function polygon_export() {
        // Export the defined polygons to a file
        // Returns number of bytes written or false on fail
        if ( empty(self::$polygons) ) {
            self::$polygons = get_option(NIMP_POLYGON_OPTION_NAME);
        }
        $data = serialize(self::$polygons);
        $temp = 1;
        if ($fd = @fopen(NIMP_POLYGON_FILE, "w")) {
            $result = fwrite($fd, $data);
            fclose($fd);
        } else {
            $result = false;
        }        
        return $result;
    }
    
    static function polygon_import($merge=true) {
        // Import polygon array
        // If $merge=true, merge the inported polygons with existing ones, replacing those with the same slug
        // Returns
        if ($fd = @fopen(NIMP_POLYGON_FILE, "r")) {
            $result = fread($fd, filesize(NIMP_POLYGON_FILE));
            fclose($fd);
            if ( $result !== false ) {
                $data = unserialize($result);
                $result = true;
                if ( $merge ) {
                    self::$polygons = array_merge(self::$polygons, $data);
                } else {
                    self::$polygons = $data;
                }
                update_option(NIMP_POLYGON_OPTION_NAME, self::$polygons);
            }
        } else {
            $result = false;
        }        
        return $result;
    }
    
    static function ajax_delete_polygon($post) {
        // Delete the polygon
        $temp = 1;
        if ( ! empty(self::$polygons[$post['poly_slug']])) {
            unset(self::$polygons[$post['poly_slug']]);
            $result = update_option(NIMP_POLYGON_OPTION_NAME, self::$polygons);
            if ( ! $result ) {
                NIMP_Util::error_log('Failed to update polygon options');
            }
            $msg = 'Polygon deleted';
            $select = self::format_polygon_form(self::$polygons);
        } else {
            $msg = 'Polygon not found';
            $select = '';
        }
        return array('msg' => $msg, 'select' => $select, 'polygons' => self::$polygons);
    }
    
    
    static function ajax_shortcode_builder() {
        // Outputs html for shortcode builder
        if ( empty(self::$polygons) ) {
            self::$polygons = get_option(NIMP_POLYGON_OPTION_NAME);
        }
        
//        ob_start();
        ?>
        <form id='nrl_shortcode_form' name='nrl_shortcode_form' action="nrlBuildShortcode()">
<?php    echo NIMP_Display::format_all_search(array(), true, true, true); ?>
        </form>

  <?php
//        $response = ob_get_clean();
//        echo json_encode($response, JSON_UNESCAPED_SLASHES);
        die();
    }
    
    static function shortcode_builder_html_NOT_USED() {
        // Return html for shortcode builder
//        if ( empty(self::$polygons) ) {
//            self::$polygons = get_option(NIMP_POLYGON_OPTION_NAME);
//        }
        
        ob_start();
        
    ?><div id="nrl_shortcode_builder" class="clearfix">
        <form id="nrl_shortcode_form" name="nrl_shortcode_form" action="nrlBuildShortcode()">
<?php    echo NIMP_Display::format_all_search(array(), true, true);
?>        </form>
    </div>
<?php
        return ob_get_clean();
    }
    
    

    static function settings_factoid_text() {
        
        $factoid_text = NRL_Common::get_nrl_setting('factoid_text');
?>
    <fieldset class="nrl_settings_group">
        <div class="nrl_tab_content">
            <h4>Display Text for Factoids</h4>
            
            <p><b>** These field settings are not editable yet! **</b></p>

<?php if ( 1 == 2 ) { // TEMP ?>
            
            <p>Edit the display text for factoids below. The factoid tag on the left is used to display the factoid data. 
                Other tags can also be used in the factoid text. The tags for factoid data are all in lower case, to distinguish them from the other tags.</p>
<?php   foreach ( $factoid_text as $key => $text ) { ?>
<!--            <span class="nrl_field_label"><?php //echo $key; ?></span>-->
            <div class="nrl_clear">
                <span class="nrl_field_label">
                <label for="nrl_factoid_<?php echo $key; ?>"><?php echo $key; ?></label>
                </span>
                <input id="nrl_factoid_<?php echo $key; ?>" name="factoid_text[<?php echo $key;?>]" value="<?php echo $text; ?>" size="80" />
            </div>
<?php } ?>
            <p class="submit">
<!--                <input id="submit-factoid-text" class="button-primary" type="submit" value="factoid_text" onclick="document.pressed = this.value" name="submit_button" />-->
                <button type="submit" id="submit-factoid-text" class="button-primary" name="submit_button" value="factoid_text" >Save Factoid Text</button>                
            </p>
            
<?php } // TEMP ?>
            
        </div>        
    </fieldset>           
<?php        
    }    
            
    static function settings_tools() {
        // Get most recent modification date
        global $wpdb;

        //Display Import status
        ?>
            <div class="clear"></div>
                <fieldset class="nrl_settings_group">
                    <div class="nrl_status">
<!--                        <h4>Import Status</h4>        -->
<?php
if ( 1 === 2 ) {
// Get record count
        $query = "SELECT COUNT(*) FROM $wpdb->nrl_properties";
        $records = $wpdb->get_results( $query, ARRAY_A );
        $record_cnt = $records[0]['COUNT(*)'];
        if ( $record_cnt > 0 ) {
            // Get last mod date
            $query = "SELECT * FROM $wpdb->nrl_properties ORDER BY ModificationTimestamp DESC LIMIT 1";
            $listings = $wpdb->get_results( $query, ARRAY_A ); 
            $mod_date = $listings[0]['ModificationTimestamp'];  
            $prev_start = ( ! empty(self::$mls_options['prev_start_time']) ? self::$mls_options['prev_start_time'] : 'none' );
        ?>
                        <p>
                            <?php echo (self::$mls_options['initial_import'] ? 'Initial import in progress.' : 'Initial import finished.' ) ?></br>
                            Previous Start Time: <?php echo $prev_start; ?></br>
                            New Start Time: <?php echo self::$mls_options['new_start_time']; ?></br>
                            Offset: <?php echo self::$mls_options['offset']; ?></br>
                            Import Limit: <?php echo self::$mls_options['import_limit']; ?></br>
                            Properties Remaining: <?php echo self::$mls_options['remaining']; ?></br>
                            Estimated time remaining: <?php 
                            if ( self::$mls_options['offset'] < 25 ) {
                                echo 'unknown';
                            } else {
                                $time_used = time() - strtotime(self::$mls_options['new_start_time']);
//                                    $time_remaining = $time_used * ( (int)(self::$mls_options['remaining']+self::$mls_options['offset'])  ) / ((int)self::$mls_options['offset']);
                                $time_remaining = ((int)self::$mls_options['remaining']) * $time_used / ((int)self::$mls_options['offset']);
                                $times = NRL_Common::elapsed_time_string($time_remaining);
//                                    $days = (int)($time_remaining / (24*3600));
//                                    $hours = (int) (($time_remaining - $days*24*3600) / 3600);
//                                    $minutes = (int)(($time_remaining - $days*24*3600 - $hours*3600) / 60);
//                                echo "{$times['days']} days, {$times['hours']} hours, {$times['minutes']} minutes";                                    
                            }
                            ?></br>
                            Most recent modification: <?php echo $mod_date; ?></br>
                            <br/>Number of records in database: <?php echo $record_cnt; ?><br/>
                            <?php //$time_limit = ini_get('max_execution_time');
                            //ini_set('max_execution_time', 91);  ?>
                            PHP Max Execution Time: <?php echo ini_get('max_execution_time'); ?><br/>
                            <?php //ini_set('max_execution_time', $time_limit);  ?>
                            <?php //ini_set('max_execution_time', 91);  ?>
<!--                            Number of subdivisions in cities table: <?php //echo $cities_cnt; ?><br/>-->
                        </p>
                        <?php 
                        
                        // Check for Maintenance task status
                        $defaults = array(
                            'finished' => true,
                        );
                        $maint_options = get_option( NIMP_MAINT_OPTION_NAME, $defaults );
                        $temp = 1;
//                        if ( ! empty($maint_options['next_id']) ) {
                            // Maintenance has been run at least once
                        if ( $maint_options['finished'] ) {  ?>
                        <p>Maintenance Task is finished</p>
<?php                     if ( $maint_options['custom'] ) { ?>
                        <p>Custom Maintenance Task has been scheduled</p>
<?php                     } ?>
<?php                  } else { ?>
                        <p>Maintenance Task is active<br/>
                            <?php 
                            if ( isset($maint_options['offset']) && isset($maint_options['rec_count']) ) {
                                echo "Offset: {$maint_options['offset']}, Record count: {$maint_options['rec_count']}"; 
                        }
                        ?>
                        </p>
                            
<?php  }
        } else {
            // There aren't any records
            echo "<p>No listings have been imported yet.</p>\n";
        }
}        
//                        }
?>
                    </div>

                    <div class="nrl_tab_content">
                    
                        <p id="nimp_tools_import_sales"><button type="button" onclick="nimpTools('imports_sales')">Import Sales Listings</button></p>
                        <p id="nimp_tools_import_sales"><button type="button" onclick="nimpTools('imports_rentals')">Import Rentals Listings</button></p></br>
                        <p id="nimp_tools_export_polygons"><button type="button" onclick="nimpTools('export_polygons')">Export Polygons</button></p><br/>
                        
<?php      if ( NIMP_DEV_USER ) { ?>
                        <p id="nimp_tools_maintenance"><button type="button" onclick="nimpTools('maintenance')">Enable Custom Maintenance Task</button></p>
                        <p id="nimp_tools_test"><button type="button" onclick="nimpTools('test')">Run Test Code</button></p><br/>

<?php         if ( class_exists("NIMP_Bright") ) {
               // Add stuff for Bright MLS conversion..
?>
                        <p id="nimp_tools_sales_table"><button type="button" onclick="nimpConvert('create_sales_table')">Split sales table from DB</button></p>
                        <p id="nimp_tools_rentals_table"><button type="button" onclick="nimpConvert('create_rentals_table')">Split rentals table from DB</button></p>
                        <p id="nimp_tools_sales_rename"><button type="button" onclick="nimpConvert('convert_sales')">Rename Sales DB fields for Bright MLS</button></p>
                        <p id="nimp_tools_rentals_rename"><button type="button" onclick="nimpConvert('convert_rentals')">Rename Rentals DB fields for Bright MLS</button></p>
                        <p id="nimp_tools_process_db_fields"><button type="button" onclick="nimpConvert('process_db_fields')">Process Bright MLS DB fields</button></p>
<?php          } ?>


<?php      } ?>
            <br/>
            <h3>Response Messages:</h3>
            <p>Status:</p>
            <div id="nimp_tools_status"></div>
            <p>Log:</p>
            <div id="nimp_tools_log"></div>
            <div id="nimp_tools_progress_notice" class="nrl_progress_notice">This is a hidden notice</div>
            
<?php if ( 1 === 2 ) { ?>
                    <p><button type="button" onclick="location.href='?page=nimp_settings&nrl_action=import-listings'">Import Listings</button></p><br/>
                    <p><button type="button" onclick="location.href='?page=nimp_settings&nrl_action=poly-export'">Export Polygons</button></p>
                    <p><button type="button" onclick="location.href='?page=nimp_settings&nrl_action=poly-import'">Import Polygons</button></p><br/>
<?php      if ( NIMP_DEV_USER ) { ?>
                    <p><button type="button" onclick="location.href='?page=nimp_settings&nrl_action=custom'">Enable Custom Maintenance Task</button></p>
                    <p><button type="button" onclick="location.href='?page=nimp_settings&nrl_action=test'">Run Test Code</button></p><br/><br/>
                    <p><button type="button" onclick="location.href='?page=nimp_settings&nrl_action=convert-columns'">Bright MLS: Convert Columns</button></p>
                    
<?php      } ?>
<?php } ?>                    
                    </div>
                </fieldset>
        <?php

    }   // function settings_tools
            
    static function settings_debug() {
        // Debug tab
        // List of settings to display
//        $nrl_settings = array(
//            'field_formats',
//            'field_settings',
//            'import_fields',
//            'import_status',
//            'search_fields',
//        );
        
// ### Need to finish select code...j
// Rewrite status and log to build the id from the tab name (tools, debug, etc.)
// ..use classes instead of id for css
// ..use id for updating contents        
        
?>        
            NRL Settings Field: <select id="nimp_debug_nrl_setting">
<?php
        global $wpdb;
        $table_name = $wpdb->nrl_settings; 
        $nrl_settings = $wpdb->get_col( "SELECT option_name FROM $table_name" );
        foreach ( $nrl_settings as $setting ) { ?>
                <option value="<?php echo $setting; ?>"><?php echo $setting; ?></option>
<?php   } ?>                
            </select>
            <p id="nimp_debug_get_setting"><button type="button" onclick="nimpDebug('get_setting')">Get Setting</button></p><br/>
            <p id="nimp_debug_get_options"><button type="button" onclick="nimpDebug('get_options')">Get plugin options</button></p>
            <p id="nimp_debug_get_maint_options"><button type="button" onclick="nimpDebug('get_maint_options')">Get maintenance options</button></p>
            <br/>
            <p>Output:</p>
            <div id="nimp_debug_log" class="nimp_admin_log"></div>            
            <div id="nimp_debug_progress_notice" class="nrl_progress_notice">This is a hidden notice</div>
        
<?php
        
    }

    static function validate_settings( $text ) {
        // Validate the imports plugin settings
        // Wordpress will call this function when the settings form is submitted
        // $text contains the POST options array from the form

$temp = 1;

        // See which submit button was clicked
        if ( filter_has_var( INPUT_POST, 'submit_button' ) ) {
            $submit = filter_input( INPUT_POST, 'submit_button', FILTER_SANITIZE_STRING );
            $temp = 1;
            
        }
        
        
// XXX How do I prevent saving the default option if we are on a different page?
//..I could replace $text with the old option value
        
        
        // Test for checkbox status
        if ( isset($text['suspend_imports']) ) {
            $text['suspend_imports'] = true;
        } else {
            $text['suspend_imports'] = false;
        }        
        if ( isset($text['allow_cron']) ) {
            $text['allow_cron'] = true;
        } else {
            $text['allow_cron'] = false;
        }
        
        // Cleanup the list of rental zips
        if ( isset($text['rental_zips']) ) {
            $zips = explode("\n",$text['rental_zips']);
            for ( $cnt=0; $cnt<count($zips); $cnt++ ) {
                $zips[$cnt] = trim($zips[$cnt]);
                if ( strlen($zips[$cnt])===0 ) {
                    unset($zips[$cnt]);
                }
            }
            sort($zips);
//            $text['rental_zips'] = $zips;
            NRL_Common::update_nrl_setting( 'rental_zips', $zips );
            unset($text['rental_zips']);
            
            // Merge in with other options (some options do not appear on the settings tab)
            $options = NIMP_Util::get_options();
            $text = array_merge( $options, $text );
        }        
        
        return $text;
    }
    
    static function duplicate_post_link( $actions, $post ) {
        if ($post->post_type=='nrl_articles' && current_user_can('edit_posts')) {
            $actions['duplicate'] = '<a href="admin.php?action=nrl_duplicate_post_as_draft&amp;post=' . $post->ID . '" title="Duplicate this item" rel="permalink">Duplicate</a>';
        }
        return $actions;
    }

    
    
    
    
    
    
// ***** Not needed for imports plugin *****
    

    
    
    static function nrl_schedule_option_page_capability( $capability ) {
        // Allow users with nrl_edit_schedule capability to manage the options on the schedule page
        return 'nrl_edit_schedule';
    }

    static function my_extra_user_fields( $user ) {
        // Display extra fields for author agents
         
        $agent_author = get_user_meta( $user->ID, 'nrl_agent_author', true );

// *** BEGIN Code for backwards compatibility
        if ( empty($agent_author) ) {
            $agent_author = get_user_meta( $user->ID, '_is_post_agent', true );
            if ( $agent_author == 'on' ) {
                $agent_author = 'true';
            }
        }
        $temp = 1;
// *** END code for backward compatibility        

?>
<?php   

        // Only admins can change Agent Author status
        if ( current_user_can('manage_options')) {
?>        <h3>Agent Author</h3>
        <table class="form-table">
            <tr>
                <th><label for="agent_author">Agent Author</label></th>
                <td>
                    <input type="checkbox" name="nrl_agent_author" value="true" <?php if($agent_author === 'true') echo 'checked="checked"';?> >
                </td>
            </tr>
        </table>
<?php             
        } else {
           // User can't change value of Agent Author checkbox
?>      <input name="nrl_agent_author" type="hidden" value="<?php echo $agent_author; ?>"/>       
<?php        }
        // Display random text input area if this is an author agent
        if ( $agent_author == 'true' ) { 
            $agent_text = get_user_meta ($user->ID, 'nrl_agent_text', true ); 
            if ( empty($agent_text) ) {
                $agent_text = array();
            }
?>
        <a name="agent-author-text"></a>
        <h3>Agent Author Random Text</h3>
        <div class="nrl_random">
<?php       //cnt = 1;
//            for ( $cnt=0; $cnt<2; $cnt++ ) {
//            foreach ( $agent_text as $item ) { 
//                $item = ( empty($agent_text[$cnt]) ? '' : $agent_text[$cnt] );
?>
            <p class="nrl_form_field">
            <label for="nrl_agent_text">Agent Author Text: </label>
            <textarea cols="65" rows="2" id="nrl_agent_text" name="nrl_agent_text[text]"><?php echo ( ! empty($agent_text['text']) ? $agent_text['text'] :
                '' ); ?></textarea><br/>            
            </p>
            <p class="nrl_form_field">
            <label for="nrl_agent_pic">Agent Author Pic: </label>
            <textarea cols="65" rows="2" id="nrl_agent_text" name="nrl_agent_text[pic]"><?php echo ( ! empty($agent_text['pic']) ? $agent_text['pic'] :
                '' ); ?></textarea><br/>            
            </p>
<?php           
//            }
 ?>
        </div>        
<?php }
        return;
    }

    static function save_my_extra_user_fields( $user_id ) {
        // Save user Agent Author status and random text
        if ( ! current_user_can( 'administrator', $user_id) && ! current_user_can( 'nrl_edit_schedule' ) )  {
            return false;
        }
        $agent_author = NRL_Common::get_post_var('nrl_agent_author' );
        $temp = 1;
        // If the user is an Agent Author, add the capacity to edit the Post Generator schedule
        $user = get_user_by( 'id', $user_id );
        if ( ! empty($agent_author) && $agent_author == 'true') {
            $user->add_cap('nrl_edit_schedule');
        } else {
            $user->remove_cap('nrl_edit_schedule');
        }
//        update_user_meta( $user_id, '_is_post_agent', $_POST['agent_author']);
        update_user_meta( $user_id, 'nrl_agent_author', $agent_author );
        
        $agent_text = NRL_Common::get_post_var('nrl_agent_text', true);
        $temp = 1;
//        foreach ( $agent_text as $key => $item ) {
//            $my_text = trim($item);
//            if ( empty($my_text) ) {
//                unset($agent_text[$key]);
//            }
//        }
        update_user_meta( $user_id, 'nrl_agent_text', $agent_text );

    }
    
    static function edit_post_views($views) {
        // Add a view to edit My Drafts
        $current_user = wp_get_current_user();
        $views['my_drafts'] = '<a href="edit.php?post_type=post&#038;author='. $current_user->ID . '&#038;post_status=draft">My Drafts</span></a>';
        return $views;
    }
    

    static function validate_schedule( $text ) {
        // Validates the input from post generation scheduling
        // Wordpress will call this function when the settings form is submitted
        // $text contains the POST options array from the form    

        // Get existing post generation schedule
        if ( empty(self::$post_schedule) ) {
            self::$post_schedule = NIMP_Util::get_post_schedule_options();
        }

        $temp = 1;
        $post_meta = array();
        // Process the schedule items
        foreach ( $text as $user_id => $user_schedule ) {
            if ( ! empty($user_schedule['schedule'])) {
                
                foreach ( $user_schedule['schedule'] as $key => $item ) {
    //                if ( $item['article_id'] < 0 ){     
                    // If the schedule item is empty (no article template selected), remove it
//                    if ( empty($item['article_id']) || ( ! is_array($item['article_id']) && $item['article_id'] < 0 ) ){
                    if ( empty($item['article_id']) || ( is_array($item['article_id']) && empty($item['article_id'][0] ) ) ){
                        unset( $text[$user_id]['schedule'][$key] );
                        $temp = 1;
                    } else {
                        // If the location value is blank, and a single template is chosen and the template has a default, fill it in
                        if ( empty($item['post_location_value']) && count($item['article_id']) == 1 &&  $item['article_id'][0] > 0 ) { // not for Random templates
                            $temp = 1;
                            if ( empty($post_meta[$item['article_id'][0]]) ) {
                                $post_meta[$item['article_id'][0]] = NIMP_Posts::get_meta_data( $item['article_id'][0] );
                            }
                            $text[$user_id]['schedule'][$key]['post_location_value'] = $post_meta[$item['article_id'][0]]['post_location_value'];
                            $text[$user_id]['schedule'][$key]['post_location_type'] = $post_meta[$item['article_id'][0]]['post_location_type'];
                        }
                    }
                }   // end inner foreach
                self::$post_schedule[$user_id]['schedule'] = array_values($text[$user_id]['schedule']);
                $temp = 1;                
            }
            // See if the enabled checkbox is unchecked
            if ( isset($text[$user_id]['enabled']) && $text[$user_id]['enabled'] == 'true'  ) {
                self::$post_schedule[$user_id]['enabled'] = 'true';
            } else {
                self::$post_schedule[$user_id]['enabled'] = 'false';
            }
            if ( ! empty($text[$user_id]['days']) ) {
                self::$post_schedule[$user_id]['days'] = $text[$user_id]['days'];
                $temp = 1;
            } else {
                self::$post_schedule[$user_id]['days'] = array();
            }
            if ( ! empty($text[$user_id]['show_users']) ) {
                self::$post_schedule[$user_id]['show_users'] = "true";
            } else {
                self::$post_schedule[$user_id]['show_users'] = "false";
            }
        }   // end outer foreach

        // Merge the two arrays, preserving the numeric keys = user ID (array_merge() won't work here)
//        self::$post_schedule = $text + self::$post_schedule;
        return self::$post_schedule;
    }
    

    static function display_post_scheduling() {
        // Schedules are stored in an array:
        // User ID => array( array( template=>ID, select=>select_type, value=>select_value) )
        
        // Get post generation schedule
        self::$post_schedule = NIMP_Util::get_post_schedule_options();

        $temp = 1;
?>
    <fieldset class="nrl_settings_group">
        <div class="nrl_tab_content">
<?php
        // Is the current user an editor?

       $user = wp_get_current_user();
        if ( NIMP_DEV_USER && current_user_can('manage_options') ) {
            // Dev user can see schedules for all agent authors
            if (  ! empty(self::$post_schedule[$user->data->ID]['show_users']) && self::$post_schedule[$user->data->ID]['show_users'] == 'true' ) {
                $checked = ' checked="checked"';
            } else {
                $checked = '';
            }
//            $checked = ( ! empty(self::$post_schedule[$user->data->ID]['show_users']) ? ' checked="checked"' : '' );
?>
            <div class="nrl_right"><input type="checkbox" name="<?php echo NIMP_POST_GENERATOR_OPTION_NAME . "[{$user->data->ID}][show_users]"; 
            ?>" value="true" <?php echo $checked; ?>/>Show all users </div>
<?php      if ( ! empty($checked) ) {
                // Show schedules for all users
                $args = array('role__in'=>array('author', 'editor','administrator'), 'orderby' => 'nicename' );
                $user_list = get_users( $args );
                foreach ( $user_list as $user ) {
                    if ( user_can( $user, 'nrl_edit_schedule') ) {
                        self::display_editor_schedule($user);
                        echo "<p>&nbsp;</p>\n";
                    }
                }      
            } else {
                // Show the schedule for only the current user
                self::display_editor_schedule($user);
            }
        } else if ( current_user_can( 'nrl_edit_schedule' ) ) {
            self::display_editor_schedule($user);
        }
?>         
            <p class="submit">
                <button type="submit" id="nrl_save_schedule" class="button-primary" name="Save" ><?php 
                esc_attr_e('Save', NIMP_TEXT_DOMAIN); ?></button>
<!--                <input id="submit1" class="button-primary" type="submit" value="<?php //esc_attr_e('Generate Now', NIMP_TEXT_DOMAIN); ?>" onclick="document.pressed = this.value" name="submit" />-->
            </p>            
        </div>        
    </fieldset>
<?php        
    }
    
    static function display_editor_schedule($user) {
        // Display the html form elements for the post generation schedule for a single editor
        
        if ( empty(self::$post_schedule[$user->ID]) ) {
            self::$post_schedule[$user->data->ID] = array(
                'enabled'   => 'true',
                'schedule' => array(),
                'days' => array('1','1','1','1','1','1','1')
            );
            $temp = 1;
        }
        
        $draft_posts = NIMP_Posts::count_user_posts_by_status( 'draft', $user->ID );
//        echo "This user has $draft_posts draft posts<br/>";
        $post_meta = array();
        
        echo "<h3>Scheduled Posts for {$user->data->display_name}:</h3>";
        $checked = ( self::$post_schedule[$user->data->ID]['enabled'] == 'true' ? ' checked="checked"' : '' );
        $post_drafts = NIMP_Posts::count_user_posts_by_status('draft', $user->data->ID );
        if ( $post_drafts >= NIMP_DRAFT_POST_LIMIT ) {
?>    <div class="nrl_notice"><p>WARNING: User has at least <?php echo NIMP_DRAFT_POST_LIMIT; ?> post drafts. No posts will be automatically generated. </p></div>
<?php  }
        if ( empty($checked) ) {
?>    <div class="nrl_notice"><p>WARNING: Post schedule for this user is disabled. No posts will be automatically generated. </p></div>
<?php   }
        if ( empty(self::$post_schedule[$user->data->ID]['days']) ) {
?>    <div class="nrl_notice"><p>WARNING: No days are checked for this user. No posts will be automatically generated. </p></div>
<?php   }
?>      <p><input type="checkbox" name="<?php echo NIMP_POST_GENERATOR_OPTION_NAME . "[{$user->data->ID}][enabled]"; 
          ?>" value="true" <?php echo $checked; ?>/> Enable Post Schedule
<!--            <span>&nbsp;&nbsp;&nbsp; <a href="edit.php?post_type=post&#038;author=<?php echo $user->ID; ?>&#038;post_status=draft">View My Post Drafts</a></span>-->
        </p>
        <p>Generate scheduled posts early in the morning on the following days:
<?php
            $day_names = array('Sun', 'Mon', 'Tue', 'Wed', 'Ths', 'Fri', 'Sat');
            for ( $cnt=0; $cnt<7; $cnt++ ) {
                $checked = ( ! empty(self::$post_schedule[$user->data->ID]['days'][$cnt]) ? ' checked="checked"'  : '' );
?>           <?php echo $day_names[$cnt]; ?> <input type="checkbox" name="<?php echo NIMP_POST_GENERATOR_OPTION_NAME . "[{$user->data->ID}][days][$cnt]"; 
                ?>"<?php echo $checked; ?> />
<?php       }
?>
         </p>
      <p><i>To select multiple article types, hold down ctrl (Windows) or cmd (Mac). When you save the schedule, if you choose a single template and 
              there is a default location for the template, that will be filled in. If you wish, you can override the 
             default location if the template allows it.</i></p>
<?php
        if ( ! empty(self::$post_schedule[$user->data->ID]['schedule'])) {
            $user_items = self::$post_schedule[$user->data->ID]['schedule'];
        } else {
            $user_items = array();
        }
          
        // Add some blank scheduled post slots
        // KPC Currently set to a max of 20 scheduled posts per agent author
        $new_items = min( max( 10 - count($user_items), 5 ), 20 );
        for ( $cnt = 1; $cnt <= $new_items; $cnt++ ) {
            $user_items[] = array();
        }
        // Add a Random option to the template list
        $templates = array('-1' => 'Random') + self::$article_templates;
        $temp = 1;
        
//        foreach ( $user_item as $item_key => $item ) {
        $cnt = 1;
        foreach ( $user_items as $item_key => $item ) {
            $sched_numb = substr( "0{$cnt}", -2 );
            if ( empty($item['article_id']) || ( is_array($item['article_id']) && empty($item['article_id'][0]) )  ) {
                $no_article = ' selected="selected"';
            } else {
                $no_article = '';
            }
?>                
            <div class="nrl_option_item">
            <span><?php echo $sched_numb; ?></span>
            <select multiple id="nrl_article_type" name="<?php echo NIMP_POST_GENERATOR_OPTION_NAME . "[{$user->data->ID}][schedule][$item_key]"; ?>[article_id][]">
                <option value=""<?php  echo $no_article;?>>Choose one:</option>
<?php      foreach ( $templates as $key => $art_title ) {  ?>
                <option value="<?php echo $key; ?>"<?php if ( ! empty($item['article_id']) && in_array($key, $item['article_id']) ) echo ' selected="selected"'; 
                ?>><?php echo $art_title; ?></option>
<?php       }   ?>
            </select>
            <select id="nrl_select_location" class="nrl_location" name="<?php echo NIMP_POST_GENERATOR_OPTION_NAME . "[{$user->data->ID}][schedule][$item_key]"; ?>[post_location_type]">
                <option value="">Any</option>
                <option value="County"<?php if ( ! empty($item['post_location_type']) && 'County' == $item['post_location_type']) echo ' selected="selected"'; 
                ?>>County</option>
                <option value="CityName"<?php if ( ! empty($item['post_location_type']) && 'CityName' == $item['post_location_type']) echo ' selected="selected"'; 
                ?>>City</option>                
                <option value="Subdivision"<?php if ( ! empty($item['post_location_type']) && 'Subdivision' == $item['post_location_type']) echo ' selected="selected"'; 
                ?>>Subdivision</option>
                <option value="PostalCode"<?php if ( ! empty($item['post_location_type']) && 'PostalCode' == $item['post_location_type']) echo ' selected="selected"'; 
                ?>>Zip</option>
            </select>
            <input type="text" id="nrl_select_value" class="nrl_location" name="<?php echo NIMP_POST_GENERATOR_OPTION_NAME . "[{$user->data->ID}][schedule][$item_key]"; ?>[post_location_value]" value="<?php
                echo ( ! empty($item['post_location_type']) && ! empty($item['post_location_value'])) ? $item['post_location_value'] : ''; ?>"  size="50" />
            </div>
<?php   
        $cnt++;
        }  // end foreach
        //}

    }
    
    
    static function duplicate_post_as_draft(){
        // Create a duplicate of a post and redirect to the edit post screen
        global $wpdb;
        if (! ( isset( $_GET['post']) || isset( $_POST['post'])  || ( isset($_REQUEST['action']) && 'nrl_duplicate_post_as_draft' == $_REQUEST['action'] ) ) ) {
            wp_die('No post to duplicate has been supplied!');
        }

        /*
         * get the original post id
         */
        $post_id = (isset($_GET['post']) ? absint( $_GET['post'] ) : absint( $_POST['post'] ) );
        /*
         * and all the original post data then
         */
        $post = get_post( $post_id );

        /*
         * if you don't want current user to be the new post author,
         * then change next couple of lines to this: $new_post_author = $post->post_author;
         */
        $current_user = wp_get_current_user();
        $new_post_author = $current_user->ID;

        /*
         * if post data exists, create the post duplicate
         */
        if (isset( $post ) && $post != null) {

            /*
             * new post data array
             */
            $args = array(
                'comment_status' => $post->comment_status,
                'ping_status'    => $post->ping_status,
                'post_author'    => $new_post_author,
                'post_content'   => $post->post_content,
                'post_excerpt'   => $post->post_excerpt,
                'post_name'      => $post->post_name,
                'post_parent'    => $post->post_parent,
                'post_password'  => $post->post_password,
                'post_status'    => 'draft',
                'post_title'     => $post->post_title,
                'post_type'      => $post->post_type,
                'to_ping'        => $post->to_ping,
                'menu_order'     => $post->menu_order
            );

            /*
             * insert the post by wp_insert_post() function
             */
            $new_post_id = wp_insert_post( $args );

            /*
             * get all current post terms ad set them to the new post draft
             */
            $taxonomies = get_object_taxonomies($post->post_type); // returns array of taxonomy names for post type, ex array("category", "post_tag");
            foreach ($taxonomies as $taxonomy) {
                $post_terms = wp_get_object_terms($post_id, $taxonomy, array('fields' => 'slugs'));
                wp_set_object_terms($new_post_id, $post_terms, $taxonomy, false);
            }

            /*
             * duplicate all post meta just in two SQL queries
             */
            $post_meta_infos = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$post_id");
            if (count($post_meta_infos)!=0) {
                $sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) ";
                foreach ($post_meta_infos as $meta_info) {
                    $meta_key = $meta_info->meta_key;
                    $meta_value = addslashes($meta_info->meta_value);
                    $sql_query_sel[]= "SELECT $new_post_id, '$meta_key', '$meta_value'";
                }
                $sql_query.= implode(" UNION ALL ", $sql_query_sel);
                $wpdb->query($sql_query);
            }

            /*
             * finally, redirect to the edit post screen for the new draft
             */
            wp_redirect( admin_url( 'post.php?action=edit&post=' . $new_post_id ) );
            exit;
        } else {
            wp_die('Post creation failed, could not find original post: ' . $post_id);
        }
    }    
    
    
    static function display_post_drafts() {
        // Display draft posts for the current user for edting or publishing
        
        global $current_user;
//        $user = wp_get_current_user();
        // Get draft posts for the user
        $args = array(
          'author'        =>  $current_user->ID, 
          'orderby'       =>  'post_modified',
          'order'         =>  'ASC',
          'post_status'   => 'draft',
          'posts_per_page' => -1 // no limit
        );
        $post_list = get_posts( $args );
        $draft_count = count($post_list);
        echo "<h3>Draft Posts for {$current_user->data->display_name} ($draft_count drafts):</h3>";
        
        if ( ! empty($post_list) ) {
            if ( count($post_list) >= NIMP_DRAFT_POST_LIMIT ) {
?>    <div class="nrl_notice"><p>WARNING: User has a least <?php echo NIMP_DRAFT_POST_LIMIT; ?> post drafts. No posts will be automatically generated. </p></div>
<?php       }
            
            self::display_post_list( $post_list );
        }
        $temp = 1;
    }
    
    static function display_post_list( $post_list ) {
        // Display a post list in a custom format
        
        $nonce = wp_create_nonce( 'bulk-posts' );
//        $referer_field = wp_referer_field();
        $temp = 1;
        // Close the current form and open a new form
        ?>  
    <form id="posts-filter" method="get">
        <input type="hidden" id="_wpnonce" name="_wpnonce" value="<?php echo $nonce; ?>">
        <?php  wp_referer_field(); ?>
        <input type="hidden" name="post_type" class="post_type_page" value="post">
        <input type="hidden" name="action" class="post_type_page" value="nrl_admin_action">
        <div class="tablenav top">
            <div class="alignleft actions bulkactions">
			<label for="bulk-action-selector-top" class="screen-reader-text">Select bulk action</label>
              <select name="nrl_action" id="bulk-action-selector-top">
                <option value="-1">Bulk Actions</option>
                    <option value="publish">Publish</option>
                    <option value="trash">Move to Trash</option>
              </select>
              <input type="submit" id="doaction" class="button action" value="Apply">
            </div>
        </div>            
            <table class="wp-list-table widefat fixed striped posts">
                <thead>
                <td id="cb" class="manage-column column-cb check-column">
                    <input id="nrl_select_all" type="checkbox" />
                </td>
                <th class="manage-column">Title</th>
                <th>Categories</th>
                <th>Tags</th>
                <th>Date</th>
                </thead>
                <tbody>
<?php   foreach ( $post_list as $post ) { 
            $cat_list = self::get_post_term_list( $post->ID, 'category' );
            $tag_list = self::get_post_term_list( $post->ID, 'post_tag' );
            $trash_nonce = wp_create_nonce( 'trash-post_' . $post->ID );
            $publish_nonce = wp_create_nonce( 'publish-post_' . $post->ID );
            $temp = 1;
?>                <tr>
                      <th scope="row" class="check-column">
                          <input id="cb-select-<?php echo $post->ID ?>" type="checkbox" name="post[]" value="<?php echo $post->ID ?>" />
                      </th>
                      <td><?php echo $post->post_title; ?>
                        <div class="row-actions">
                        <span class="edit">
                        <a href="<?php echo home_url(); ?>/wp-admin/post.php?post=<?php echo $post->ID ?>&amp;action=edit" aria-label="Edit">Edit</a> | 
                        </span>
                        <span class="trash"><a href="<?php echo home_url(); ?>/wp-admin/post.php?post=<?php echo $post->ID 
                                ?>&amp;action=trash&amp;_wpnonce=<?php echo $trash_nonce; ?>" class="submitdelete" >Trash</a> | </span>                            
                        <span class="view"><a href="<?php echo home_url(); ?>/?p=<?php echo $post->ID ?>&amp;preview=true" rel="bookmark">Preview | </a></span>                            
                        <span class="view"><a href="<?php echo home_url(); ?>/wp-admin/admin.php?post=<?php echo $post->ID 
                                ?>&action=nrl_admin_action&nrl_action=publish&_wpnonce=<?php echo $nonce; ?>">Publish</a></span>                            
<!--//                                ?>&amp;nrl_action=publish&amp;_wpnonce=<?php e//cho $publish_nonce; ?>">Publish</a></span>                            -->
                        </div>
                      
                      </td>
                      <td><?php echo $cat_list; ?></td>
                      <td><?php echo $tag_list; ?></td>
                      <td><?php echo "Post Modified<br/><abbr title='{$post->post_modified}'>" . substr($post->post_modified,0,10) . '</abbr>'; ?></td>
                  </tr>
<?php  }
?>              </tbody>
            </table>
        </form>       
<?php             
    }
    
    static function get_post_term_list( $post_id, $taxonomy='post_tag' ) {
        // wp_get_post_terms( $post_id, $taxonomy
        $terms = wp_get_post_terms( $post_id, $taxonomy );
        if ( ! empty($terms) ) {
            $term_list = '';
            $first = true;
            foreach ( $terms as $term ) {
                if ( $first ) {
                    $first = false;
                } else {
                    $term_list .= ', ';
                }
                $term_list .= $term->name;
            }
        } else {
            $term_list = '';
        }
        return $term_list;
    }

    static function display_post_generator() {
        // Displays the content of the Post Generator tab
?>
        <h2><?php _e(NIMP_PLUGIN_NAME, NIMP_TEXT_DOMAIN); ?></h2>
<?php  settings_errors();
?>
        <div id='nrl_post_generator_container' class="wrap">
<!--            <form id="nrl_post_generator" name="optionsform" class="" action="options.php" method="post" enctype="multipart/form-data">        -->
<?php // ##### KPC:  The following line generates the hidden nonce, etc. But the form does not open until later!
//         settings_fields( 'nrl_schedule' );
         do_settings_sections( 'post_generator' ); ?>
<!--            </form>  End of form -->
        </div>

<?php                
    }
    
    }    // end class NIMP_Admin

   // add_action( 'show_user_profile', array('NIMP_Admin', 'my_extra_user_fields')  );
   // add_action( 'edit_user_profile', array('NIMP_Admin','save_my_extra_user_fields') );
