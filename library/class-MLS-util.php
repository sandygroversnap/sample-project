<?php
/*
 * Utility functions to support MLS imports for Nesbitt Imports plugin
 * Used on the back end only
 * @author Ken Carlson (http://kencarlsonconsulting.com)
*/

class NIMP_MLS_Util {
//    static $maint_status;
    static $maint_options, $options, $mls;
    static $author_id; // ID for author of generated posts
    
    static function set_defaults() {
        // Set the default MLS options
        $defaults = array(
            'url'   =>  'http://csrets.mris.com:6103/platinum/login',
            'user'  =>  '3082558',
            'pwd'   =>  '855177',
            'agent'         =>  'RETS Test/1.0',
            'unique'        =>  'ListingKey',
            'allow_cron'    => true,
            'import_limit'  => 150,              // How many properties to attempt to import in one run
            'picture_limit' => 30,
            
            'initial_import'    => true,
            'new_pass'          => true,
            'added'             => 0,
            'updated'           => 0,
            'photos_added'      => 0,
            'remaining'         => 0,
            'rental_zips'       => array(),
//            'rental_picture_limit' => 10,
        );        
        return $defaults;
    }
    
// Replaced by NIMP_Util::get_options()
//     
//    static function get_options() {
//        // Load options
//        $options = get_option(NIMP_MLS_OPTIONS_NAME);
//        $defaults = self::set_defaults();
////$options['url'] = 'http://cornerstone.mris.com:6103/platinum/login';
//        if ( ! $options) {
//            // Options table entry does not exist, so create it
//            update_option(NIMP_MLS_OPTIONS_NAME, $defaults);
//            $options = $defaults;
//        } else if (count($options) < count($defaults)) {
//            // Add missing elements from the default form options array
//            $options = array_merge($defaults, $options);
//            update_option(NIMP_MLS_OPTIONS_NAME, $options);
//        }
//        return $options;
//    }    

//    static function update_options( $new_options, $context='' ) {
//        // $context gives info about the calling function, for debugging
////     static function update_options($new_options) {
//        // Clear the options cache so that the update won't fail (ref: https://codex.wordpress.org/Function_Reference/update_option)
//        $result = wp_cache_delete ( 'alloptions', 'options' );
//        $result = update_option( NIMP_MLS_OPTIONS_NAME, $new_options );
//        if ( ! $result ) {
//            $msg = 'Failed to update option: ' . NIMP_MLS_OPTIONS_NAME;
//            if ( ! empty($context) ) {
//                $msg .= " in $context";
//            }
//            NIMP_Util::error_log( $msg );
//        }
//        return $result;
//    }
    
// MOVED to NIMP_Utl    
//    static function get_post_schedule_options() {
//        // Get the post schedule options
//            $post_schedule = get_option(NIMP_POST_GENERATOR_OPTION_NAME, array());
//            
//            // Update (TEMP)
//            foreach ( $post_schedule as $user => $schedule_data ) {
//                // For backwards compatibility...
//                if ( ! empty($schedule_data) && ! isset($schedule_data['enabled'])) {
//                    $schedule = array();
//                    foreach ( $schedule_data as $item ) {
//                        $schedule[] = $item;
//                    }
//                    $post_schedule[$user] = array(
//                        'enabled'   => 'true',
//                        'schedule' => $schedule
//                        );
//                }
//                if ( ! isset($schedule_data['days'])) {
//                    $post_schedule[$user]['days'] = array('1','1','1','1','1','1','1');
//                }
//                
//                // More backwards compatibility 11-15-17 (Change article id to array
//                if ( ! empty($schedule_data['schedule'][0]) && ! is_array($schedule_data['schedule'][0]['article_id']) ) {
//                    foreach ( $schedule_data['schedule'] as $key => $item ) {
//                        if ( ! is_array($item['article_id']) ) {
//                            $post_schedule[$user]['schedule'][$key]['article_id'] = array($item['article_id']);
//                        }
//                    }
//                }
//            }   // end outer foreach
//        return $post_schedule;
//    }    

// *** Moved to NIMP_Install
    
//    static function enable_cron_jobs() {
////        $options = self::get_options();
//        $options = NIMP_Util::get_options();
//        $options['allow_cron'] = true;
////        self::update_options( $options, 'MLS_UTIL::enable_cron_jobs()' );
//        NIMP_Util::update_options( $options, 'MLS_UTIL::enable_cron_jobs()' );
//    }
//    
//    static function disable_cron_jobs() {
////        $options = self::get_options();
//        $options = NIMP_Util::get_options();
//        $options['allow_cron'] = false;
//        NIMP_Util::update_options( $options, 'MLS_UTIL::disable_cron_jobs()' );        
//    }
    
    static function get_maintenance_options() {
        $defaults = array(
            'first_run' => false,
            'finished'  => true,
            'custom'    => false,
            'type'      => ''
        );
        self::$maint_options = get_option(NIMP_MAINT_OPTION_NAME,$defaults);
        if ( count(self::$maint_options) < count($defaults) ) {
            self::$maint_options = array_merge($defaults, self::$maint_options);
        }
        return self::$maint_options;
    }

        static function update_nrl_options( $option_name, $new_options ) {
        // Clear the options cache so that the update won't fail (ref: https://codex.wordpress.org/Function_Reference/update_option)
        $result = wp_cache_delete ( 'alloptions', 'options' );
        $result = update_option($option_name, $new_options);
        if ( ! $result ) {
            NIMP_Util::error_log('Failed to update option: ' . $option_name);
        }
       return $result;
    }
    
//    static function update_maintenance_options( $new_options ) {
//        $result = update_option(NIMP_MAINT_OPTION_NAME, $new_options);
//        if ( ! $result ) {
//            NIMP_Util::error_log('Failed to update option: ' . NIMP_MAINT_OPTION_NAME);
//        }
//       return $result;
//    }
    
//    static function get_import_data($name) {
//        // Get the import option $name 
//        global $wpdb;
//        
//        $query = "SELECT option_value FROM $wpdb->nrl_settings WHERE option_name='$name'";
//        $option = $wpdb->get_results( $query, ARRAY_N );
//        if ( $option === false ) {
//            $temp = 1;
//        }
//        return unserialize($option[0][0]);
//    }
//    
//    static function update_import_data($name, $value) {
//        global $wpdb;
//        $fields = array(
//            'option_name'   => $name,
//            'option_value'  => serialize($value),
//        );
//        $result = $wpdb->replace( $wpdb->nrl_settings, $fields );
//        if ( $result === false ) {
//            $temp = 1;
//        }
//        return $result;
//    }
    
    static function run_cron_job() {
        // Run cron job indicated by the number value of nrl_cron_job (in queary string)
        // (Right now, the number value is ignored)
// TEMP for testing
//        echo $_SERVER['REMOTE_ADDR'];
//        NIMP_Util::error_log("Cron IP is {$_SERVER['REMOTE_ADDR']}");
//        exit;

        // Check for scheduled server maintenance
        // Suspend cron jobs for server maintenance update between 12am and 4 am (manually set dates in $maint_dates)
        // Server updates:
        //      Plesk upate: Mon 7/16/18 12 am to 4 am Eastern time
        //      Spectre and Meltdown vulnerabilities update: Tue 8/14 23:00 CDT (4 hour duration) = Wed 8/15 12am to 4 am Eastern
        
echo "Running cron job...\n";

        $maint_dates = array('07/16/2018', '08/15/2018');
//        $date = explode('/', date("m/d/Y"));
        date_default_timezone_set('America/New_York');
        $date = date("m/d/Y");
        $time = explode(':', date("H:i"));  // array( hours, minutes)
        if ( in_array( $date, $maint_dates ) && $time[0] <= 3 ) {
            exit;
        }

        self::$mls = new NIMP_MLS();
        
        // See if cron jobs are enabled
        if ( ! self::$mls->options['allow_cron'] ) {
            $log = "\n\n" . date('Y-m-d H:i:s') . " Cron jobs are disabled.\n";
//            NRL_Common::write_log($log, NIMP_IMPORT_LOG);            
            exit;
        }
        
        // Check the lock file to see if the cron job is already running
//        $fname = ABSPATH . 'wp-content/NRL/lock';
//        $fp = self::check_file_lock( $fname );
//        if ( $fp === false ) {
//            // The cron job is already running, so quit
//            exit(-1);
//        }

        // Set PHP maximum execution time if necessary
        $max_time = ini_get('max_execution_time');
//        if ( $max_time < (NIMP_IMPORT_TIME_LIMIT + 15) && $max_time !== 0 ) {
        if ( $max_time < (NIMP_IMPORT_TIME_LIMIT + 30) ) {
            $new_time = NIMP_IMPORT_TIME_LIMIT + 30;
            ini_set( 'max_execution_time', $new_time );
        } else {
            $new_time = $max_time;
        }
        
        // Check for import type
        $import_type = filter_input(INPUT_GET, 'import', FILTER_SANITIZE_STRING);

        // See if we need to run a maintenence job
        self::get_maintenance_options();
//        if ( ( date('G') == '2' && date('i' < 4) ) && ($maint_options$maint_options['first_run'] || ! $maint_options$maint_options['finished'] ) ) {

        if ( ! self::$maint_options['finished'] ) {
            // We have an active maintenance task
            self::maintenance();
//        } else if( $time[0] == 2 && $time[1] < 4 ) {
////        } else if( $time[0] <= 24 && $time[1] <= 59 ) {
//            // At 2 am start a custom cron job, if any
//            if ( ! empty(self::$maint_options['custom_photos']) ) {
//                self::maintenance_photos();
//            } 
            
        } else if( $time[0] == 2 && $time[1] < 4 ) {
//        } else if( $time[0] == 2 ) {
//        } else if( $time[0] <= 24 && $time[1] <= 59 ) {            
//            if ( $time[1] < 4 ) {
                // At 2 am start the history and deletions maintenance task

            
//                self::maintenance_start('history');                
            
            
//            } elseif ( $time[1] > 54 ) {
//                // At the end of the hour, rebuild the listing virtual page site maps
////                self::create_listings_sitemap('sales');
////                self::create_listings_sitemap('rentals');
//            }
                
        } else if( $time[0] == 3 && $time[1] < 4 ) {
//        } else if( $time[0] <= 24 && $time[1] <= 59 ) {
            // At 3 am start the generate posts task
//            self::maintenance_start('generate_posts');
            
        } else if( $time[0] == 4 && $time[1] < 4 ) {
//        } else if( $time[0] <= 24 && $time[1] <= 59 ) {
            // At 4 am start a database cron job, if any
            if ( ! empty(self::$maint_options['custom']) ) {
//                self::maintenance_start('database');
//                self::maintenance_start('custom');
            } else {
                // Build the virtual listngs sitemaps
//                self::create_listings_sitemap('sales');
//                self::create_listings_sitemap('rentals');                
            }
            
        } else {
            // Run the import function
//            $numb = filter_input(INPUT_GET, 'nrl_imports', FILTER_SANITIZE_STRING);
//            $mris = new NIMP_MLS();
//            switch ($numb) {
//                case '1':
                    // Import listings
            self::$mls->import_mls( $import_type );
//                    break;
//            }            
        }
        
        // After the cron job is run, exit
//        fclose($fp);  // Close and unlock the lock file
//        $fp = 0;     // This should release the lock (probably unnecessary)
        exit;
    }
    
    static function check_file_lock( $fname ) {
        // Check the lock file to see if the cron job is already running
        // Returns the file pointer or false the cron job is already running
        
        $fp = fopen( $fname, 'r+' );
        if ( empty($fp) ) {
            echo "Failed to open lock file: $fname<br/>";
        }
        
        // Is the file locked?
        if ( ! flock( $fp, LOCK_EX | LOCK_NB ) ) {     // Exclusive lock, don't block access
            // For backward compatibility...
            if ( empty(self::$mls->options['cron_running']) ) {
                self::$mls->options['cron_running'] = 0;
            }
            $cnt = self::$mls->options['cron_running'];
            echo "Cron job already running (count=$cnt)";
            $log = "\n" . date('Y-m-d H:i:s') . " Cron job already running (count=$cnt).\n";
            
            // See if the cron job code has been running too long
            if ( self::$mls->options['cron_running']++ >= 5 ) {
                // The cron job code is stuck.. stop it
                $result = flock( $fp, LOCK_UN );
                $log .= "\ncron code has been running too long--unlocking the lock file.";
                if ( $result === false ) {
                    $log .= "..FAILED!";
                }
                $log .= "\n";
                fclose($fp);  // Close and unlock the lock file
                $fp = 0;     // This should release the lock (probably unnecessary)                
                // Reset the cron is running count so that it will run the cron job
                self::$mls->options['cron_running'] = 0;
                // reopen the file
                $fp = fopen( $fname, 'r+' );                
            } else {
                // The lock file is locked, and the cron job hasn't been running too long, so quit
                $fp = false;                
            }
//  TEMP DISABLED           
//          NRL_Common::write_log($log, NIMP_IMPORT_LOG);
            // Update options
            NIMP_Util::update_options( self::$mls->options, 'MLS_UTIL::run_cron_job() update cron is running count' );
//            if ( self::$mls->options['cron_running'] > 0 ) {
//                // The lock hasn't been reset, so don't exit
////  TEMP DISABLED               exit(-1);
//            } 
        } else if ( ! empty(self::$mls->options['cron_running']) ) {
            // Safeguard in case this is incorrectly set
            self::$mls->options['cron_running'] = 0;
            NIMP_Util::update_options( self::$mls->options, 'MLS_UTIL::run_cron_job() reset cron is running count' );
        }
        
        //NRL_Common::write_log("No Cron job is running, so run the cron job\n", NIMP_IMPORT_LOG);
        return $fp;
    }
    
    static function enable_custom_task() {
        // Called by NIMP_Admin::display_options() to enable custom maintenance task
        self::get_maintenance_options();
        self::$maint_options['custom'] = true;
        $result = self::update_nrl_options( NIMP_MAINT_OPTION_NAME, self::$maint_options );
        if ( $result ) {
            echo 'Custom maintanence task is enabled.<br/>';
        }
        // If there is an error updating the option, update_nrl_optionx() will write to the error log
    }
    
    static function update_settings_csv_OLD($type) {
        // Reads in a csv file and return the settings
        // The csv file name is $type .csv
        // Returns an array with field name as key, or false if the file can't be opened

        $fields = false;
//        $mls_options = NIMP_MLS_Util::get_options();
        if ( $type == 'nrl_field_settings' ) {
            // We need to add the field formats from the field_formats nrl setting
            $field_formats_sales = NRL_Common::get_nrl_setting('field_formats_sales');
            $field_formats_rentals = NRL_Common::get_nrl_setting('field_formats_rentals');
            $field_formats = array_merge( $field_formats_sales, $field_formats_rentals );
            
            if ( empty($field_formats) ) {
                return false;
            }
        }
        
        $fname = NIMP_PATH . "setup/$type.csv";
//        $option_name = $type;
        // Get the column titles from the first row
        if ( ($handle = fopen($fname, "r") ) !== FALSE ) {
            if ( ($titles = fgetcsv($handle, 1000, ",")) === FALSE ) {
                // Error reading file
                fclose($handle);
                return(false);
            }
            $fields = array();
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                // The first column contains the field name.  Use it as the array key.
//                if ( ! empty($data[2]) ) {
                if ( count($data) > 2 ) {
                    // There are more than two colulmns, so store all but the first column in an array
                    $fields[$data[0]] = array();
                    // Fill in the array with the data from the other columns
                    foreach ( $data as $key => $val ) {
                        if ( $key > 0 && $titles[$key]!='order' ) {
                            $fields[$data[0]][$titles[$key]] = $val;
                        }
                    }
                    if ( $type == 'nrl_field_settings' ) {
                        // Add the field format from mls_options
                        if ( $data[0] == 'ListPrice' || $data[0] == 'ClosePrice' ) {
                            $fields[$data[0]]['format'] = '%d';
                        } else {
                            $fields[$data[0]]['format'] = $field_formats[$data[0]];
                        }
                    }                    
                } else {
                    // There are only two columns
                    if ( ! empty($data[1]) ) {
                        $fields[$data[0]] = $data[1];                    
                    }
                }   // end else
            }   // end while
            fclose($handle);
//            $result = update_option( $option_name, $fields );
        } else {
            // Failed to open file
            return false;
        }
        return $fields;        
    }
    
    static function update_search_fields_csv_OLD() {
        // Reads in a csv file and return the search settings
        // The csv file name is nrl_ . $type .csv
        // Returns an array with field name as key, or false if the file can't be opened
        $fields = false;
        $fname = NIMP_PATH . 'setup/' . NIMP_SEARCH_OPTION_NAME . ".csv";
        // Get the column titles from the first row
        if ( ($handle = fopen($fname, "r") ) !== FALSE ) {
            if ( ($titles = fgetcsv($handle, 1000, ",")) === FALSE ) {
                // Error reading file
                fclose($handle);
                return(false);
            }
            $fields = array();
            $fields2 = array();
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                // The first column contains the field name.  Use it as the array key.
                $fields[$data[0]] = array();
                // Fill in the array with the data from the other columns
                foreach ( $data as $key => $val ) {
                    if ( $key > 0 ) {
                        if ( $titles[$key] === 'default') {
                            // Correct captitalization added by Excel
                            $val = strtolower($val);
                        }
                        $fields[$data[0]][$titles[$key]] = $val;
                    }
                }
                // Set up the shortcode option array
                if ( $fields[$data[0]]['type'] === 'min-max' ) {
                    // This is a min-max field
                    $shortcode = $fields[$data[0]]['shortcode'];
                    $fields2["$shortcode-min"] = $data[0] . '-min';
                    $fields2["$shortcode-max"] = $data[0] . '-max';
                } else {
                    $shortcode = $fields[$data[0]]['shortcode'];
                    $fields2[$shortcode] = $data[0];                }
            }
            fclose($handle);
        } else {
            // Failed to open file
            return false;
        }
        return $fields;        
    }   // end function update_settings_search_csv() 

// *** The next two functions have similar counterparts in NIMP_Database for the main plugin
    
    static function maintenance_start( $type ) {
        // Initiate maintenance job
        // Called by run_cron_job()
        
        $maint_types = array( 'history', 'deletions', 'database', 'generate_posts', 'custom' );
        if ( ! in_array( $type, $maint_types )) {
            NIMP_Util::error_log( "Invalid maintenance type: $type" );
            if ( ! self::$maint_options['finished'] ) {
                self::$maint_options['finished'] = true;
                self::update_nrl_options( NIMP_MAINT_OPTION_NAME, self::$maint_options );
            }
            return;
        }
        
        if ( defined( 'NIMP_DEBUG') ) {
            $log = date('Y-m-d H:i:s') . " Starting $type maintenance task..\n";
            NRL_Common::write_log( $log, NIMP_DEBUG_LOG );            
        }
        self::$maint_options['first_run'] = true;
        self::$maint_options['finished'] = false;
        self::$maint_options['type'] = $type;
        self::update_nrl_options( NIMP_MAINT_OPTION_NAME, self::$maint_options );
//        $result = update_option('nrl_maintenance',self::$maint_options);
        if ( $type == 'database') {
            // Create the cities table if it does not exist
            self::create_cities_table();
        }
        self::maintenance();
    }
    
    static function maintenance() {
        // Run maintenance task determined by type
        // Called by run_cron_job and maintenance_start()
        
        switch ( self::$maint_options['type'] ) {
            case 'history':
                self::maintenance_history();
                break;
            case 'deletions':
                 self::maintenance_deletions();
                break;        
            case 'database':
                self::maintenance_database();
                break;
            case 'generate_posts':
                self::maintenance_generate_posts();
                break;
            case 'custom':
                self::maintenance_custom();
                break;
            default:
                // Error--invalid maintenance type
                self::$maint_options['finished'] = true;
                self::update_nrl_options( NIMP_MAINT_OPTION_NAME, self::$maint_options );             
        }
    }
    
    static function maintenance_history() {
        // Run maintenance task to scan the MRIS History resource
        // This will process Withdrawn, Expired, and TempOff listings

        $t_start = microtime(true);
        self::$options = NIMP_Util::get_options();
        
        if ( self::$maint_options['first_run'] ) {
            // Set the new start time
            $temp = 1; 
            if ( isset(self::$maint_options['hist_new_start_time']) ) {
                self::$maint_options['hist_prev_start_time'] = self::$maint_options['hist_new_start_time'];
                
//self::$maint_options['hist_prev_start_time'] = '2016-05-17T15:00:00';
                
            } else {
                $prev_time = time() - 60*60*24;
                self::$maint_options['hist_prev_start_time'] = date('Y-m-d\TH:i:s', $prev_time); //. 'T' . date('H:i:s', $prev_time);
            }
            $prev_start = strtotime(self::$maint_options['hist_prev_start_time']);
            if ( ($prev_start + 60*60*24*10) < time() ) {
                // Don't try to process more than 10 days at once
                self::$maint_options['hist_new_start_time'] = date('Y-m-d\TH:i:s', $prev_start + 60*60*24*14);
                $query_time = self::$maint_options['hist_prev_start_time'] . '-' . self::$maint_options['hist_new_start_time'];
            } else {
                self::$maint_options['hist_new_start_time'] = date('Y-m-d\TH:i:s'); //. 'T' . date('H:i:s');
                $query_time = self::$maint_options['hist_prev_start_time'] . '+';                
            }
  
// FOR TESTING            
//self::$maint_options['hist_new_start_time'] = date('Y-m-d\TH:i:s', $new_time);            
            
//        $query = "((PropHistChangeType=STATUS),(PropHistOriginalPickListValue=ACTIVE-CORE),(PropHistNewPickListValue=|TEMPOFF-CORE,WITHDRN-CORE,EXPIRED-CORE)"
            $query = "((PropHistChangeType=STATUS),(PropHistNewPickListValue=|TEMPOFF-CORE,WITHDRN-CORE,EXPIRED-CORE)"
//                    . ",(PropHistChangeTimestamp=" . self::$maint_options['hist_prev_start_time'] . "+))";
                    . ",(PropHistChangeTimestamp=" . $query_time . "))";
// FOR TESTING                    
//                    . ",(PropHistChangeTimestamp=" . self::$maint_options['hist_prev_start_time'] . '-' . self::$maint_options['hist_new_start_time'] . "))";
//            self::$maint_options['listings'] = self::get_mris_records("History","PROP_HIST",$query);
            self::$maint_options['offset'] = 0;
            self::$maint_options['history']['expired'] = 0;
            self::$maint_options['history']['withdrawn'] = 0;
            self::$maint_options['history']['offmarket'] = 0;
            self::$maint_options['photos_deleted'] = 0;
            self::$maint_options['first_run'] = false;
            
            // For initialiation
            if ( ! isset(self::$maint_options['history']['expired_addr']) ) {
                self::$maint_options['history']['expired_addr'] = '';
            }
            if ( ! isset(self::$maint_options['history']['withdrawn_addr']) ) {
                self::$maint_options['history']['withdrawn_addr'] = '';
            }            
            if ( ! isset(self::$maint_options['history']['withdrawn_total']) ) {
                self::$maint_options['history']['withdrawn_total'] = 0;
            }
            if ( ! isset(self::$maint_options['history']['report'])) {
                self::$maint_options['history']['report'] = array();
            }
            $listings = self::get_mris_records("History","PROP_HIST",$query);
        } else {
            // We are continuing to process a set -- get the records
            $listings = get_option(NIMP_HISTORY_OPTION_NAME);
            $temp = 1;
        }

        global $wpdb;
        while ( self::$maint_options['offset'] < count($listings)  ) {
            
            $mls_number = $listings[self::$maint_options['offset']]['mls'];
            $listing_key = $listings[self::$maint_options['offset']]['key'];
            $my_query = "SELECT * FROM $wpdb->nrl_properties WHERE ListingKey='$listing_key'";
            $db_listing = $wpdb->get_results( $my_query, ARRAY_A );
            if ( ! empty($db_listing) ) {
                // Listing found -- process the status change
// *** For testing..
//                $source_key = $listings[self::$maint_options['offset']]['key'];
//                $db_key = $db_listing[0]['ListingKey'];
//                $db_addr = $db_listing[0]['FullStreetAddress'];
//                $source_addr = $listings[self::$maint_options['offset']]['addr'];
//                $db_status = $db_listing[0]['ListingStatus'];
                
                if ( $db_listing[0]['ListingId'] != $mls_number ) {
                    // Something is wrong, the MLS numbers don't match
                    $msg = "ListingId $listing_key has MLS $mls_number in MRIS, but {$db_listing[0]['ListingId']} in our database";
                    NIMP_Util::error_log($msg);
                    $temp = 1;
                } else {
                    // Update the database
                    $new_status = $listings[self::$maint_options['offset']]['NewStatus'];
                    if ( $new_status === 'WITHDRN-CORE' || $new_status === 'EXPIRED-CORE' ) {
                        // Capture the address for email
                        $time_stamp = $listings[self::$maint_options['offset']]['Timestamp'];
                        $address = $db_listing[0]['FullStreetAddress'] . ', ' . $db_listing[0]['CityName'] . '  MLS#: ' . $db_listing[0]['ListingId'] 
                            . "  Date/time: $time_stamp \n";
                        if ( $new_status === 'WITHDRN-CORE' ) {
                            self::$maint_options['history']['withdrawn_addr'] .= $address;
                            self::$maint_options['history']['report'][$db_listing[0]['CityName']]['Withdrawn'][] = $address;
                            self::$maint_options['history']['withdrawn']++;
                        } else {
                            self::$maint_options['history']['expired_addr'] .= $address;
                            self::$maint_options['history']['report'][$db_listing[0]['CityName']]['Expired'][] = $address;
                            self::$maint_options['history']['expired']++;
                        }
                        
                        // For testing, just change the status to DELETED...
//                        $result = $wpdb->update( $wpdb->nrl_properties , ['ListingStatus' => 'DELETED'], ['id' => $db_listing[0]['id']] );
//                        $temp = 1;

                        // Remove the photos
                        self::$maint_options['photos_deleted'] += self::delete_photos($db_listing[0]['id'], 0);                        
                        
                        // Delete this listing
                        $post_id = $db_listing[0]['PostID'];
                        $result = $wpdb->delete( $wpdb->nrl_properties, array( 'id' => $db_listing[0]['id'] ) );
                        if ( $result === false ) {
                            NIMP_Util::error_log("History maintenance: Error deleting listing id " . $db_listing[0]['id'] );
                        }
                        
                        // Delete the listing page
//                        wp_delete_post( $post_id, true );
                        
                    } else if ( $new_status === 'TEMPOFF-CORE' ) {
                        // Change status fo OffMarket
                        $result = $wpdb->update( $wpdb->nrl_properties , array( 'ListingStatus' => 'OffMarket' ), array( 'id' => $db_listing[0]['id'] ) );
                        self::$maint_options['history']['offmarket']++;
                    }                    
                }

            }  // end if not empty $listing
            
            self::$maint_options['offset']++;
            // Is there time to continue?
            if ( (microtime(true) - $t_start) > NIMP_IMPORT_TIME_LIMIT ) {
                break;
            }            
        }   // end while
        
        //See if we are finished
        if ( self::$maint_options['offset'] >= count($listings) ) {
            // Next check for deletions
//            self::$maint_options['finished'] = true;
            self::$maint_options['first_run'] = true;
            self::$maint_options['type'] = 'deletions';
            self::$maint_options['listings'] = array();
            self::$maint_options['history']['withdrawn_total'] += self::$maint_options['history']['withdrawn'];
            $log = date('Y-m-d H:i:s') . " History maintenance: " . self::$maint_options['history']['withdrawn'] . " Withdrawn and " 
                    . self::$maint_options['history']['expired'] . ' Expired listing removed, ';
            if ( self::$maint_options['history']['offmarket'] > 0 ) {
                $log .= self::$maint_options['history']['offmarket'] . " marked as TempOff, ";
            }
            $log .= self::$maint_options['photos_deleted'] . " photos deleted.\n";
            NRL_Common::write_log($log, NIMP_MAINT_LOG);
            echo " History maintenance task: " . self::$maint_options['history']['withdrawn'] . ' withdrawn and ' . self::$maint_options['history']['expired'] 
                    . ' expired listings deleted.  ' . self::$maint_options['history']['offmarket'] . ' offmarket listings updated.<br/>';
            
            // Send some emails
//            if ( ! empty(self::$maint_options['history']['expired_addr']) || ! empty(self::$maint_options['history']['withdrawn_addr']) ) {
            if ( ! empty(self::$maint_options['history']['report']) ) {    
                if ( ! empty(self::$options['notification_email']) ) {
                    // Send emails, one per city
                    foreach ( self::$maint_options['history']['report'] as $cityname => $city ) {
                        $subject = "Expired/Withdrawn listings in $cityname removed on " .  date( 'Y-m-d' );
                        $msg = '';
                        // XXX Note that $listings is reused here!!!
                        foreach ( $city as $type => $listings ) {
                            // Types are Expired or Withdrawn
                            $msg .= "*** $type listings in $cityname removed on " . date('Y-m-d') . ":\n";
                            foreach ( $listings as $listing ) {
                                $msg .= $listing;
                            }
                            $msg .= "\n\n";
                        }
                        // Send an email for this city
                        $result = wp_mail( self::$options['notification_email'], $subject, $msg );                            
                    }
//                    $subject = "Expired/Withdrawn listings removed on " .  date( 'Y-m-d' );
//                    $msg = 'Expired listings removed on ' . date('Y-m-d') . ":\n\n" . self::$maint_options['history']['expired_addr'];
//                    $msg .= "\n\nWithdrawn listings removed on " . date('Y-m-d') . ":\n\n" . self::$maint_options['history']['withdrawn_addr'];
//                    $result = wp_mail( self::$options['notification_email'], $subject, $msg );
                } else {
                    // Don't send an email
                    $result = true;
                }

                if ( $result ) {
                    self::$maint_options['history']['expired_addr'] = '';
                    self::$maint_options['history']['withdrawn_addr'] = '';
                    self::$maint_options['history']['report'] = array();
                    self::$maint_options['history']['withdrawn_total'] = 0;
                } else {
                    // Error sending email
                    $msg = "Maintenance History: Error sending email to ". self::$options['notification_email'];
                    NIMP_Util::error_log($msg);
                }
            }

        } else {
            // We are not finished yet...  save the remaining listings
            $listings = array_slice($listings, self::$maint_options['offset']);
            self::update_nrl_options( NIMP_HISTORY_OPTION_NAME,$listings );
//            update_option( NIMP_HISTORY_OPTION_NAME,$listings);
            self::$maint_options['offset'] = 0;     // We have removed the elements already processed, so start at zero
            $temp = 1;
        }

        self::update_nrl_options( NIMP_MAINT_OPTION_NAME, self::$maint_options );
//        $result = update_option('nrl_maintenance',self::$maint_options);        
        $temp = 1;
    }   // end function maintenance_history()
    
    static function maintenance_deletions() {
        // Run maintenance task to scan the MRIS Deletions resource
        // This will remove any listings deleted by MRIS

        $t_start = microtime(true);
        if ( self::$maint_options['first_run'] ) {
            // Set the new start time
            if ( isset(self::$maint_options['del_new_start_time']) ) {
                self::$maint_options['del_prev_start_time'] = self::$maint_options['del_new_start_time'];
                
//$prev_time = time() - 60*60*24 * 20;
//self::$maint_options['del_prev_start_time'] = date('Y-m-d\TH:i:s', $prev_time); //. 'T' . date('H:i:s', $prev_time);                
                
            } else {
                $prev_time = time() - 60*60*24;
                self::$maint_options['del_prev_start_time'] = date('Y-m-d\TH:i:s', $prev_time); //. 'T' . date('H:i:s', $prev_time);
            }
            $prev_start = strtotime(self::$maint_options['del_prev_start_time']);
            if ( ($prev_start + 60*60*24*10) < time() ) {
                // Don't try to process more than 10 days at once
                self::$maint_options['del_new_start_time'] = date('Y-m-d\TH:i:s', $prev_start + 60*60*24*14);
            } else {
                self::$maint_options['del_new_start_time'] = date('Y-m-d\TH:i:s'); //. 'T' . date('H:i:s');
            }            
            
            self::$maint_options['del_new_start_time'] = date('Y-m-d\TH:i:s'); //. 'T' . date('H:i:s');
            $query = "((TableName=LISTINGS),(DeletionTimestamp=" . self::$maint_options['del_prev_start_time'] . "+))";
            self::$maint_options['listings'] = self::get_mris_records("Deletions","DELETIONS",$query);
            self::$maint_options['offset'] = 0;
            self::$maint_options['deletions']['deleted'] = 0;
            self::$maint_options['photos_deleted'] = 0;
            self::$maint_options['first_run'] = false;
        }
        global $wpdb;
        while ( self::$maint_options['offset'] < count(self::$maint_options['listings'])  ) {
            $listing_key = self::$maint_options['listings'][self::$maint_options['offset']];
            $my_query = "SELECT * FROM $wpdb->nrl_properties WHERE ListingKey='$listing_key'";
            $db_listing = $wpdb->get_results( $my_query, ARRAY_A );
            if ( ! empty($db_listing) ) {
                // Listing found -- process the status change
                
                // For testing, just change the status to DELETED...
//                $result = $wpdb->update( $wpdb->nrl_properties , ['ListingStatus' => 'DELETED'], ['id' => $db_listing[0]['id']] );
//                $temp = 1;
                
                // Remove the photos
                self::$maint_options['photos_deleted'] += self::delete_photos($db_listing[0]['id'], 0);
                
                // Delete this listing
                $post_id = $db_listing[0]['PostID'];
                $result = $wpdb->delete( $wpdb->nrl_properties, array( 'id' => $db_listing[0]['id'] ) );
                if ( $result === false ) {
                    NIMP_Util::error_log( "Deletions maintenance: Error deleting listing id " . $db_listing[0]['id'] );
                }
                
                // Delete the listing page
                wp_delete_post( $post_id, true );

                self::$maint_options['deletions']['deleted']++;
            }  
            self::$maint_options['offset']++;
            // Is there time to continue?
            if ( (microtime(true) - $t_start) > NIMP_IMPORT_TIME_LIMIT ) {
                break;
            }
        }   // end while
        
        //See if we are finished
        if ( self::$maint_options['offset'] >= count(self::$maint_options['listings']) ) {
            self::$maint_options['finished'] = true;
            self::$maint_options['listings'] = array();
            self::$maint_options['type'] = '';
            if ( self::$maint_options['deletions']['deleted'] > 0 ) {
                $log = date('Y-m-d H:i:s') . " Deletions maintenance task: " . self::$maint_options['deletions']['deleted'] . " listings deleted, "
                    . self::$maint_options['photos_deleted'] . " photos deleted.\n" ;
                NRL_Common::write_log($log, NIMP_MAINT_LOG);
            }
            echo " Deletions maintenance: " . self::$maint_options['deletions']['deleted'] . " listings deleted, "
                . self::$maint_options['photos_deleted'] . " photos deleted. \n" ;
        }
        self::update_nrl_options( NIMP_MAINT_OPTION_NAME, self::$maint_options );
//        $result = update_option( NIMP_MAINT_OPTION_NAME, self::$maint_options );
    }
    
    static function get_mris_records( $resource, $class, $query, $parms='' ) {
        // Get records from MRIS $resource using $query
        // Returns array of selected fields from the records
        
        if ( empty($parms) ) {
            $parms = array( "Count" => 1, "Format" => "COMPACT");
        }
        $search = self::$mls->rets->SearchQuery( $resource, $class, $query, $parms );
        $results = array();
        if ( ! empty($search) ) {
            // Save the list in case we run out of time
            $record_count1 = self::$mls->rets->TotalRecordsFound();   // This does not work if count=0        
//            $prop_hist_keys = array();
            while ($listing = self::$mls->rets->FetchRow($search)) {
//                $status_change[$listing['PropHistListingKey']] = $listing['PropHistNewPickListValue'];
                 switch ($resource) {
                    case "History":
                        $results[] = array( 
                            'mls' => $listing['ListingId'], 
                            'key' => $listing['PropHistListingKey'], 
                            'NewStatus' => $listing['PropHistNewPickListValue'],
                            'addr' => $listing['FullStreetAddress'],
//                            'PropHistKey' => $listing['PropHistKey'],
                            'Timestamp' => $listing['PropHistChangeTimestamp']);
//                        $prop_hist_keys[] = $listing['PropHistKey'];
                        break;
                    case "Deletions":
                        $results[] = $listing['UniversalKey'];
//                        $time_stamp = $listing['DeletionTimestamp'];
//                        if ( !in_array($listing['SchemaShortName'], $schema)) {
//                            $schema[] = $listing['SchemaShortName'];
//                        }
                        break;
                }               
                $temp = 1;
            }
            self::$mls->rets->FreeResult($search);  
//            if ( ! empty($prop_hist_keys) ) {
                // Save this list for use in case these can't be processed in a single pass
//                update_option( NIMP_HISTORY_OPTION_NAME,$prop_hist_keys );
//            }
        }
        return $results;
    }   // end function get_mris_records()
    
    static function check_photo_mods( $id, $prev_date='' ) {
        // See if there have been media updates for listing $id since $prev_date
        // Get history records for a given MLS number for which MEDIA have changed
//        $mls = 'FX9646684';
//        $query = "((PropHistKey=300134264245,300134273162,300134276272,300134277969,300134281856))";

        if ( empty($prev_date) ) {
            $prev_date = '1960-01-01T00:00:00';
        }
        $query = "((PropHistChangeType=MEDIA),(ListingKey=$id)"
                    . ",(PropHistChangeTimestamp=$prev_date+))";        
//                . ")";
//                . ",(PropHistChangeTimestamp=2016-06-12T00:00:00+))";        
        $search = NIMP_MLS::mls_search('History', 'PROP_HIST', $query,
                array( "Count" => 1, "Format" => "COMPACT", "Limit" => 100));
        $temp = 1;
        if ( ! empty($search) ) {
            $record_count1 = NIMP_MLS::mls_record_count();
            while ($listing = NIMP_MLS::mls_get_row($search) ) {
                $temp = 1;
                echo $listing['PropHistChangeTimestamp'] . "<br/>\n";
//                echo 'PropHistKey: ' . $listing['PropHistKey'] . '  MLS: ' . $listing['ListingId'] . "<br/>\n";
            }
            NIMP_MLS::mls_free_search($search);
        }
        return $record_count1;
    }
    
// *** COPIED to NIMP_Database, but still needed here for now    
    
    static function maintenance_generate_posts() {
        // Generate posts scheduled in the Post Generator

define( 'NIMP_DEBUG_POSTGEN', true );
        
        $t_start = microtime(true);
        self::$options = NIMP_Util::get_options();
        if ( empty(self::$maint_options) ) {
            self::$maint_options = self::get_maintenance_options();
        }
        if ( self::$maint_options['first_run'] ) {
            self::$maint_options['users'] = 0;
            self::$maint_options['posts'] = 0;
            self::$maint_options['errors'] = 0;
            self::$maint_options['no_seed'] = 0;
            self::$maint_options['users_over_limit'] = 0;
//            self::$maint_options['schedule_data'] = get_option(NIMP_POST_GENERATOR_OPTION_NAME, array());
            self::$maint_options['schedule'] = NIMP_Util::get_post_schedule_options();
//            self::$maint_options['user_ids'] = array_keys(self::$maint_options['schedule']);
//            self::$maint_options['index'] = 0;
            self::$maint_options['first_run'] = false;
//            if ( isset(self::$maint_options['user_ids']) ) {
//                unset(self::$maint_options['user_ids']);
//            }
            if ( defined( 'NIMP_DEBUG_POSTGEN' ) ) {
                $log = date('Y-m-d H:i:s') . " Post Generation started..\n";
                NRL_Common::write_log( $log, NIMP_DEBUG_LOG );                
            }
        }
        $post_schedule = self::$maint_options['schedule'];
        $dow = (int) date('w');
        
        $timeout = false;
        $users = $posts = $errors = 0;
        foreach ( $post_schedule as $user_id => $schedule_data ) {
            
            if ( $schedule_data['enabled'] == 'false' || empty($schedule_data['days'][$dow]) ) {
                // Post generation for this user is disabled or generation is off for the day of week
                continue;
            }
            
            // Get the number of draft posts for this user
            $draft_count = NIMP_Posts::count_user_posts_by_status( 'draft', $user_id);
            
// FOR TESTING
//$user = get_user_by('id', $user_id);
//echo "Schedule for {$user->data->display_name}: <br/>";
            $cnt = 1;
            foreach ( $schedule_data['schedule'] as $key => $schedule ) {
                // See if there are too many draft posts
                if ( $draft_count++ >= NIMP_DRAFT_POST_LIMIT ) {
                    self::$maint_options['users_over_limit'];
                    break;
                }
                
                // Generate a post
//                $schedule_items[$index]['post_select_type'] = 'default';
                $schedule['post_select_type'] = 'default';
                $result = NIMP_Posts::generate_scheduled_post( $user_id, $schedule );
//                if ( $result == 'success' ) {
                if ( is_int( $result ) ) {      // If successful, the generated Post_ID is returned
                    $posts++;
                } else {
                    // Error generating post
                    self::$maint_options[$result]++;
//                    $errors++;
                }
                unset( self::$maint_options['schedule'][$user_id]['schedule'][$key] );
                 
// FOR TESTING
//echo "..article ID: " . $schedule['article_id'] . " Select type: " . $schedule['post_select_type'] . "<br/>";
                
                // See if we have time to continue..
                if ( (microtime(true) - $t_start) > NIMP_IMPORT_TIME_LIMIT ) {
//                if ( (microtime(true) - $t_start) > 20 ) {
                    $timeout = true;
                    break;
                }
                $cnt++;
            }   // end inner foreach
            

            if ( $timeout ) {
                // We have just now finished generting all the posts for the current user
                if ( empty(self::$maint_options['schedule'][$user_id]) ) {
                    $users++;
                    unset( self::$maint_options['schedule'][$user_id] );
                }
                break;
            } else {
                // Get ready for the next user
                $users++;
                unset( self::$maint_options['schedule'][$user_id] );
            }
        }   // end outer foreach

        self::$maint_options['users'] += $users;
        self::$maint_options['posts'] += $posts;
        self::$maint_options['errors'] += $errors;
            
        if ( $timeout && ! empty(self::$maint_options['schedule']) ) {
            $temp = 1;
            if ( defined( 'NIMP_DEBUG_POSTGEN' ) ) {
                $log = date('Y-m-d H:i:s') . " Post Generation finish this group..\n";
                NRL_Common::write_log( $log, NIMP_DEBUG_LOG );                
            }
        } else {
            // We are finished processing the scheduled posts
            unset( self::$maint_options['schedule'] );
            self::$maint_options['finished'] = true;
            // Write a message to the log
            $log = date('Y-m-d H:i:s') . " Post Generator: " . self::$maint_options['posts'] . " posts generated for " . self::$maint_options['users'] . " users ";
            if ( self::$maint_options['users_over_limit'] > 0 ) {
                $log .=  ', ' . self::$maint_options['users_over_limit'] . ' users with more than ' . NIMP_DRAFT_POST_LIMIT . ' draft posts';
            }
            if ( self::$maint_options['no_seed'] > 0 ) {
                $log .=  ', ' . self::$maint_options['no_seed'] . ' posts could not get a seed';
            }
            if ( self::$maint_options['errors'] > 0 ) {
                $log .= ", *** " . self::$maint_options['errors'] . " errors ***";
            }
            $log .= "\n";
            NRL_Common::write_log($log, NIMP_MAINT_LOG);
            if ( defined( 'NIMP_DEBUG_POSTGEN' ) ) {
                $log = date('Y-m-d H:i:s') . " Post Generation completed\n\n";
                NRL_Common::write_log( $log, NIMP_DEBUG_LOG );                
            }            
        }
        // Update maintenance options
//        self::update_maintenance_options( self::$maint_options );
        self::update_nrl_options(NIMP_MAINT_OPTION_NAME, self::$maint_options);
        
    }   // end function maintenance_generate_posts()
    
    
    static function maintenance_database() {
        // This task will scan the database to perform a custom task
        // This is designed to be run each time the cron job runs until it is finished
        // The database should not be modified (by adding or removing listings) while this is running
        
        global $wpdb;
        $limit = 4000;
        $t_start = microtime(true);
            
        if ( self::$maint_options['first_run'] ) {
            // Get a count of the records in the database
            $query = "SELECT COUNT(*) FROM $wpdb->nrl_properties";
            $records = $wpdb->get_results( $query, ARRAY_A );
            self::$maint_options['rec_count'] = $records[0]['COUNT(*)'];
            self::$maint_options['offset'] = 0;
            self::$maint_options['added'] = 0;
//            self::$maint_options['photos_deleted'] = 0;
            self::$maint_options['first_run'] = false;
            self::$maint_options['custom'] = false;
        } 

        // Get next group of records from database
        $offset = self::$maint_options['offset'];
        $query = "SELECT County,CityName,Subdivision,PostalCode FROM $wpdb->nrl_properties ORDER BY id ASC LIMIT $limit OFFSET $offset";
        $records = $wpdb->get_results( $query, ARRAY_A );  

        // Process the records
        $rec_cnt = 0;
        $added = 0;
        $find_keys = array();
        $removed = 0;
        foreach ( $records as $record ) {
            $added += self::add_city( $record, $type );
            $rec_cnt++;
            // Is there time to continue?
            if ( (microtime(true) - $t_start) > NIMP_IMPORT_TIME_LIMIT ) {
                break;
            }             
        }   // End foreach

        // Update stats..
        self::$maint_options['offset'] += $rec_cnt;
        self::$maint_options['added'] += $added;
        echo "DB Scan pass: Processed $rec_cnt records, added $added records <br/>";

        // See if we are finished...
        if ( self::$maint_options['offset'] >= (self::$maint_options['rec_count']) ) {  // was self::$maint_options['rec_count']-1
            // We are finished -- delete the records marked for deletion
            self::$maint_options['finished'] = true;
            echo 'DB Scan task finished.  ' . self::$maint_options['rec_count'] . ' records processed, <br/>';
                $log = date('Y-m-d H:i:s') . " Database maintenance: " . self::$maint_options['rec_count'] . ' records processed,'  . 
                    self::$maint_options['added'] . " records added to cities table\n";
                NRL_Common::write_log( $log, NIMP_MAINT_LOG );

            $temp = 1;
        }        
        // Update options
//        $result = update_option( NIMP_MAINT_OPTION_NAME, self::$maint_options );
        self::update_nrl_options( NIMP_MAINT_OPTION_NAME, self::$maint_options );
        
    }   // end function maintenance_database()
    
    static function remove_orphans($find_keys) {
        // Check a group of ListingKeys against the MRIS database and delete orphan records
        // $find_keys = array( $id => $ListingKey )
        // Returns the number of listings marked at DELETED
        global $wpdb;
        $search_keys = implode(',',$find_keys);
        $query = "((ListingKey=$search_keys))";
        $search = self::$mls->rets->SearchQuery("Property","RES",$query, 
            array( "Count" => 1, "Format" => "COMPACT", "Select" => "ListingKey") );
        $found_keys = array();
        while ($listing = self::$mls->rets->FetchRow($search)) {
            $found_keys[] = $listing['ListingKey'];
        }
        $removed = 0;
        foreach ( $find_keys as $id => $key ) {
            if ( ! in_array($key, $found_keys) ) {
                // This listing is not in the MRIS database, so we need to delete it.
                // Change the status to DELETED.  Once we are finished, we will delete all these records.
                $result = $wpdb->update( $wpdb->nrl_properties , ['ListingStatus' => 'DELETED'], ['id' => $id] );

                // Remove the photos
                self::$maint_options['photos_deleted'] += self::delete_photos($id, 0);                        
                $removed++;
            }
        }
        return $removed;
    }
    
    static function maintenance_custom( $first_run=false ) {
        // This runs a custom maintenance job
        // May 2018: This version deletes WP listing pages, but not WP listing location pages
        
        ini_set( 'max_execution_time', 120 );
        $max_time = 105;
//        $max_time = NIMP_IMPORT_TIME_LIMIT - 5;
        
        $t_start = microtime(true);

        if ( empty(self::$maint_options) ) {
            self::$maint_options = self::get_maintenance_options();
        }        
        
        if ( self::$maint_options['first_run'] || $first_run ) {
            self::$maint_options['first_run'] = false;
            if ( empty( self::$maint_options['deleted_posts'] ) ) {
                self::$maint_options['deleted_posts'] = 0;
            }
            $log = date('Y-m-d H:i:s') . " Post deletion custom maintenance task started";
            NRL_Common::write_log( $log, NIMP_CUSTOM_LOG );            
        }
        
        // Set the author for the generated page
        $user  = get_user_by( 'slug', 'nesbittrealty' );
        if ( ! empty( $user ) ) {
            $author_id = $user->ID;
        } else {
            return;
        }
        echo "<br/>Deleting WP listing pages...<br/>";
        
        // Get the next batch of listing pages
        $args = [
            'post_type' => 'page',
            'fields' => 'ids',
        //    'nopaging' => true,
            'numberposts' => 100,
            'author'	=> $author_id,
            'meta_key' => '_wp_page_template',
            'meta_value' => 'page-templates/listing_page.php',
        ];
        
        $cnt = 0;
        $timeout = false;
        
// ***** TEMP FOR TESTING        
//        $site_url = get_site_url();
//        if ( strpos( $site_url, '/localhost') !== false ) {
//            $local_host = true;
//        }
//        $max_time = 2;
        $local_host = false;
        $temp = 1;
        
        while ( ! $timeout && ( microtime(true) - $t_start ) < $max_time ) {
            $pages = get_posts( $args );
            if ( empty($pages) ) {
                break;
            }
            foreach( $pages as $page_id ) {
                
// **** FOR TESTING ****                
                if ( $local_host ) {
                    wp_delete_post( $page_id );
                    $cnt++;
                } else {
                    wp_delete_post( $page_id, true );
                    $cnt++;
                }
                if ( ( microtime(true) - $t_start ) > $max_time ) {
                    $timeout = true;
                    break;
                } 
            }
        }
        
        $t_elapsed = round( (microtime(true) - $t_start), 4);
        echo "$t_elapsed seconds used to find and delete " . $cnt . " WP listing pages<br/>";

        self::$maint_options['deleted_posts'] += $cnt;
        
// *** TEMP FOR TESTING **
NRL_Common::write_log( '.', NIMP_CUSTOM_LOG );
        
        
        // Are we finished?
        if ( empty($pages) ) {
            $log = "\n" . date('Y-m-d H:i:s') . " Post deletion Finished: " . self::$maint_options['deleted_posts'] . " total posts deleted\n\n";
            NRL_Common::write_log( $log, NIMP_CUSTOM_LOG );
            self::$maint_options['finished'] = true;
            self::$maint_options['custom'] = false;
            unset( self::$maint_options['deleted_posts'] );
        } else {
            // See if we need to quit for the day
            $time = explode(':', date("H:i"));  // array( hours, minutes)
//            if ( 1 == 2 && $time[0] >= 6 ) {
            // The custom maintenance job can run on cron for up to 2 hours
            if ( $time[0] >= 6 ) {
                $log = "\n" .date('Y-m-d H:i:s') . " Post deletion finished for the day: " . self::$maint_options['deleted_posts'] . " total posts deleted so far.\n";
                NRL_Common::write_log( $log, NIMP_CUSTOM_LOG );            
                self::$maint_options['finished'] = true;
            }            
        }
        
        self::update_nrl_options( NIMP_MAINT_OPTION_NAME, self::$maint_options );
       
    }
    
    static function maintenance_photos() {
        // Maintenance function to remove orphan photos from listings no longer in the database
        // This function is designed to save the state and run once a day
        // It does NOT use maintenance_start() to initialize
        
        global $wpdb;
        
        if ( ! isset(self::$maint_options['custom']) ) {
            self::get_maintenance_options();
        }
        
        $t_start = microtime(true);
//        if ( self::$maint_options['first_run'] ) {
        if ( empty(self::$maint_options['photos']['photos_task']) ) {
            self::$maint_options['photos']['photos_task'] = true;   // The task is started..
            // Get the highest id in the database
            $query = "SELECT id FROM $wpdb->nrl_properties ORDER BY id DESC LIMIT 1";
            $records = $wpdb->get_results( $query, ARRAY_A );            
            self::$maint_options['photos']['last_id'] = $records[0]['id'];
            self::$maint_options['photos']['current_id'] = 0;
            self::$maint_options['photos']['deleted'] = 0;
        }
        
        $id = self::$maint_options['photos']['current_id'];
        $removed = 0;
        while ( $id++ < self::$maint_options['photos']['last_id'] ) {
            // See if there are any photos for this id
            $photos = NRL_Common::get_photos($id);
            if ( ! empty($photos) ) {
                // See if this id is in the database
                $query = "SELECT id,ListingStatus FROM $wpdb->nrl_properties WHERE id=$id";
                $records = $wpdb->get_results( $query, ARRAY_A ); 
                $delete_photos = false;
                if ( empty($records) ) {
                    // Listing does not exist in database. Delete the photos
                    $delete_photos = true;
                } else if ( $records[0]['ListingStatus'] == 'Closed' && count($photos) > 1 ) {
                    // Retain only 1 photo for closed listings
                    unset($photos[0]);
                    $delete_photos = true;
                }
                if ( $delete_photos ) {
                    // Delete the photos
                    foreach ( $photos as $photo ) {
                        if ( unlink($photo) ) {
                            $removed++;
                        } else {
                            // There was an error deleting the photo
                            $temp = 1;
                        }
                    }     
                    $temp = 1;
                }
            }
            // See if there is time to continue
            if ( (microtime(true) - $t_start) > NIMP_IMPORT_TIME_LIMIT ) {
                break;
            }             
        }   // end while
        
        self::$maint_options['photos']['current_id'] = $id;
        self::$maint_options['photos']['deleted'] += $removed;
        
        // See if we are finished
        if ( $id >= self::$maint_options['photos']['last_id'] ) {
            self::$maint_options['photos']['photos_task'] = false;  // Mark the task as not running
            $log = date('Y-m-d H:i:s') . " Photo maintenance complete: " . self::$maint_options['photos']['deleted'] . " total orphan photos removed.\n" ;
            NRL_Common::write_log( $log, NIMP_MAINT_LOG );
            unset(self::$maint_options['custom_photos']);
        } else {
            $log = date('Y-m-d H:i:s') . " Photo maintenance in progress: " . $removed . " orphan photos removed this session, current id: $id\n" ;
            NRL_Common::write_log( $log, NIMP_MAINT_LOG );
        }
        
        // Update options
//        $result = update_option( NIMP_MAINT_OPTION_NAME, self::$maint_options );
        self::update_nrl_options( NIMP_MAINT_OPTION_NAME, self::$maint_options );

    }
    
    static function delete_photos( $id, $retain=1 ) {
        // Delete the photos for property id
        // Retain the first $retain photos
        // Returns the number of photos deleted
        // Called by import_mls() to remove all but one photo from properties that have been sold
        
        // KPC: Later, modify this to delete thumbnails too (except for $retain)
        
        $photos = NRL_Common::get_photos($id);
        $cnt = 0;
        foreach ( $photos as $photo ) {
            if ( ++$cnt > $retain ) {
                unlink($photo);
            }
        }
        return max( array($cnt-$retain,0) );
    }
    
    static function create_photo_path($path) {
        if ( ! file_exists($path) ) {
            $loc = strrpos($path, '/', -2);
            $parent = substr($path, 0, $loc);
//            if ( ! file_exists($parent) ) {
            self::create_path($parent);
//            }
            self::create_path($path);
        }
    }

    static function create_path($path, $permission=0755) {
        // Create a directory if it does not exist
//        $result = true;
            if ( ! file_exists($path) ) {
                $temp = 1;
                if ( ! mkdir($path, $permission) ) {
//                    $result = false;
                    die('Failed to create folders...');
                }
            }    
    }

    static function get_photo_type( $listing_key, $photo_caption ) {
        // Gets a single photo for $listing_key of type $photo_caption (e.g. "Exterior (Front)")
        // KPC: Later, could be a random selection of photos that match the type


        if ( empty(self::$mls) ) {
            self::$mls = new NIMP_MLS();            
        }
        // The below works to get exterior photos:
        $query = "((PropObjectKey=$listing_key)," . '(PropMediaCaption="' . $photo_caption . '"))';
        $search = self::$mls->rets->SearchQuery("Media","PROP_MEDIA",$query, 
            array( "Count" => 1, "Format" => "COMPACT", "Limit" => 30)
//            array( "Count" => 1, "Format" => "COMPACT", "Select" => "ListingKey")
        );
        $results = array();
        if ( ! empty($search) ) {
            // Save the list in case we run out of time
            $record_count1 = self::$mls->rets->TotalRecordsFound();   // This does not work if count=0        
//            $prop_hist_keys = array();
            $listing = self::$mls->rets->FetchRow($search);
            self::$mls->rets->FreeResult($search);  
        } else {
            $listing = '';
        }
        return $listing;
    }
    
    static function test_photo_type( $listing_key, $photo_caption ) {
        
        // PropMimeType returned by MRIS Media PROP_MEDIA
        $mime_type = array(
            '10000000379'   => 'gif',
            '10000000378'   => 'jpg',
            '10000000380'   => 'png',
            '10000000381'   => 'tiff'
        );        
        $photo = self::get_photo_type($listing_key, $photo_caption);
        if ( ! empty($photo) ) {
            // Save the file
            if ( ! empty( $mime_type[$photo['PropMimeType']]) ) {
                $ext = $mime_type[$photo['PropMimeType']];
            } else {
                $ext = 'jpg';
            }
            $fname = get_home_path() . 'wp-content/uploads/test.' . $ext;
            $result = file_put_contents($fname, $photo['Data']);
            $temp = 1;

        }
        
        return;
        
    }
    
    static function send_admin_email( $msg ) {
        $to_addr = 'kenneth.carlson42@gmail.com';
        $from_addr = '';
        $subject = "NRL Error on " .  date( 'Y-m-d' );
        $result = wp_mail( $to_addr, $subject, $msg );
        
        return $result;
    }
    
    static function add_city( $fields, $type ) {
        // Add county/city/subdivision to cities table if it is not already there
        // Makes sure that the location pages exist (this part will be moved to the main plugin)
        // Returns number of records added (0 or 1)
        global $wpdb;
                
        // See if this is a new city
        $query = 'SELECT * FROM ' . $wpdb->nrl_cities . ' WHERE County="' . $fields['County'] . '" AND City="' . $fields['City'] .'"';
//            . '" AND Subdivision="' . $subdiv .'"';
        $record =  $wpdb->get_row( $query );
        $added = 0;
//        $type = ( $fields['ForSale'] == '1'? 'Sales' : 'Rental' );
        $listing_type = ( $type == 'sales'? 'Sales' : 'Rental' );
        $new = $listing_type . 'New';

        if ( $record === NULL ) {
            // Add the county/city to the list
            // Add this record
            $city = NRL_Common::name_case( $fields['City'] );
            $data = array(
                'County'    => $fields['County'],
                'City'      => $city,
                'Zips'      => $fields['PostalCode'],
                $listing_type       => True,    // Set Sales or Rental to True
//                $new        => True,    // Mark this as a new Sales or Rental city
                
            );
            $formats = array('%s','%s','%s', '%d');
            $result = $wpdb->insert( $wpdb->nrl_cities, $data, $formats);
            if ( ! $result ) {
                $temp = 1;
            } else {
                $added = 1;
            }

            // Create the location page for county/city if necessary
//            self::create_location_pages($fields);
        } else {
            // The city is already in the database
            $data = array();
            $formats = array();
            // See if the sales/rental city page exitss
            if ( ! $record->{$listing_type} ) {
                // New sales/rental city page
//                self::create_location_pages($fields);
                $data[$listing_type] = True;
                $formats[] = '%d';
                $data['LastMod'] = date("Y-m-d H:i:s");
                $formats[] = '%s';
//                $data[$new] = True;
            }
            // Update the zip list
            $zips = $record->Zips;
            if ( ! empty($fields['PostalCode']) && strpos($zips, $fields['PostalCode']) === false ) {
                $zips .= ' ' . $fields['PostalCode'];
                $data['Zips'] = $zips;
                $formats[] = '%s';
            }
            
            // Update the cities record if it has changed
            if ( ! empty($data) ) {
            // Usage: $wpdb->update( $table, $data, $where, $format = null, $where_format = null );
                $result2 = $wpdb->update( $wpdb->nrl_cities, $data, array('id'=>$record->id), $formats, '%d' );
                $temp = 1;
                if ( ! $result2 ) {
                    $temp = 1;
                }                
            }
        }
        return $added;
    }   // end function add_city()
    
    static function get_cities_list() {
        // For utility use only
        global $wpdb;
        $query = "SELECT * FROM $wpdb->nrl_cities ORDER BY County,City";
        $records =  $wpdb->get_results( $query, ARRAY_A );
        $temp = 1;
        $output = "County,City,Zips\n";
        foreach ( $records as $record ) {
//            echo "{$record['County']} --- {$record['City']} --- {$record['Subdivision']}<br/>\n";
            // Creating a .csv file, so be sure value does not contain a comma
//            $record['Subdivision'] = str_replace(',', '', $record['Subdivision']);
            $output .=  "{$record['County']},{$record['City']},{$record['Zips']}\n";
        }
        // Find the last id in the cities table
        $query = "SELECT id FROM $wpdb->nrl_cities ORDER BY id DESC LIMIT 1";
        $result =  $wpdb->get_results( $query, ARRAY_A );        
        $last_id = $result[0]['id'];
        $filename = NIMP_PATH . "/cities_list-$last_id.csv";
        $result2 = NRL_Common::write_log($output, $filename, 'w');
        $temp = 1;
        if ( ! $result2['status'] ) {
            // Error writing file
            $temp = 1;
        }        
    }
    
    static function custom_test() {
        // Used for testing the code
//        $log = date('Y-m-d H:i:s') . " Database maintenance: This would have run the custom maintenance function.\n";
//        NRL_Common::write_log($log, NIMP_MAINT_LOG);        
        
return;        
            // Now delete the records marked for deletion
            $delete_start = microtime(true);
            $result = $wpdb->delete( $wpdb->nrl_properties, array('ListingStatus' => 'DELETED') );  
            $delete_time = microtime(true) - $delete_start;   
            
            if ( $result === false ) {
                // Error deleting listings
                $log = date('Y-m-d H:i:s') . " Database maintenance: Error deleting listings.\n";
                NRL_Common::write_log( $log, NIMP_MAINT_LOG );
            } else {
                $log = date('Y-m-d H:i:s') . " Database maintenance: $result records deleted in $delete_time seconds,\n"; 
                NRL_Common::write_log( $log, NIMP_MAINT_LOG );
            }        
        
    }
    
    /* ***** Functions to create a listing page for a listing -- MOVED to NIMP_Database (for main plugin) ***** */

    
    /* **** The following three functions have been moved to NRL_Common
     * ..these were duplicated in NIMP_Listings_Page **** */


// *** Moved to NIMP_Util    
//    static function set_featured_image($post_id, $url) {

// Moved to NIMP_Util
//    static function thumbnail_url_field( $html ) {
//
//    static function thumbnail_external_replace( $html, $post_id ) {
//  
// Moved to NIMP_Util    
//     static function url_is_image( $url ) {

    /* *** The following function does not appear to be used *** */
//    static function thumbnail_url_field_save( $pid, $post ) {
//        $cap = $post->post_type === 'page' ? 'edit_page' : 'edit_post';
//        if (
//            ! current_user_can( $cap, $pid )
//            || ! post_type_supports( $post->post_type, 'thumbnail' )
//            || defined( 'DOING_AUTOSAVE' )
//        ) {
//            return;
//        }
//        $action = 'thumbnail_ext_url_' . $pid . get_current_blog_id();
//        $nonce = filter_input( INPUT_POST, 'thumbnail_ext_url_nonce', FILTER_SANITIZE_STRING );
//        $url = filter_input( INPUT_POST,  'thumbnail_ext_url', FILTER_VALIDATE_URL );
//        if (
//            empty( $nonce )
//            || ! wp_verify_nonce( $nonce, $action )
//            || ( ! empty( $url ) && ! url_is_image( $url ) )
//        ) {
//            return;
//        }
//        if ( ! empty( $url ) ) {
//            update_post_meta( $pid, '_thumbnail_ext_url', esc_url($url) );
//            if ( ! get_post_meta( $pid, '_thumbnail_id', TRUE ) ) {
//                update_post_meta( $pid, '_thumbnail_id', 'by_url' );
//            }
//        } elseif ( get_post_meta( $pid, '_thumbnail_ext_url', TRUE ) ) {
//            delete_post_meta( $pid, '_thumbnail_ext_url' );
//            if ( get_post_meta( $pid, '_thumbnail_id', TRUE ) === 'by_url' ) {
//                delete_post_meta( $pid, '_thumbnail_id' );
//            }
//        }
//    }
       
//       static function wpautop_fix() {
//
//    $meta_test = get_post_meta( get_the_ID(), 'is_listing_page', true );
//     if ($meta_test === 'true') {
//             remove_filter( 'the_content', 'wpautop' ); 
//                remove_filter( 'the_excerpt', 'wpautop' );
//
//     }
//       }

    
}
