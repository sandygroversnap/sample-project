<?php

/**
 * Class to handle import of MLS records from MLS database
 * For Nesbitt Imports plugin 
 * @author Ken Carlson (http://kencarlsonconsulting.com)
 */

// *** This should include only code not required on the front end! ***
// Load PHRETS library
//require_once( NIMP_PATH . "vendor/autoload.php");      // For PHRETS v. 2.x
require_once( NIMP_PATH . "library/phrets.php" );        // For PHRETS v. 1.x


class NIMP_MLS {
    var $rets, $connected, $mls_class;  //  $mls_options, 
    var $options, $status;
    var $media_formats, $media_lookup_fields, $media_lookups;

    function __construct( $bright_mls = true ) {
        // For PHRETS v. 1.x
        // If $bright_mls is true, use the Bright MLS server instead of MRIS
         
        // Get options
//        $this->mls_options = NIMP_MLS_Util::get_options();
        
        $this->options = NIMP_Util::get_options();
//        $this->status = NRL_Common::get_nrl_setting( 'import_status' );
        
        if ( $bright_mls ) {
            // Change the login info to use Bright MLS
            $this->options['url'] = 'http://csrets.mris.com:6103/platinum/login';
            $this->options['user'] = '3237119';
            $this->options['pwd'] = 'vap2ubpSuswevaCr6cravpChu';
            $this->options['agent'] = 'Bright RETS Application/1.0';
            $this->options['rets-version'] = 'RETS/1.8';
//            $this->options['unique'] = '';
        }
        
//        $this->mls_options = array(
//            'url'   =>  'http://csrets.mris.com:6103/cornerstone/login',
//            'user'  =>  '3082558',
//            'pwd'   =>  '855177',
//            'agent' =>  'RETS Test/1.0',
//            'unique'    =>  'ListingKey',
//        );
        $this->connected = false;

        // Login to MRIS
        $this->connect();

        $temp = 1;
        
    }
    
    function __destruct() {
        $this->disconnect();
    }
    
    function connect() {
        // See if we are already connected
        if ( $this->connected ) {
            return;
        }
        // start rets connection
        $this->rets = new phRETS;

        // Uncomment and change the following if you're connecting
        // to a server that supports a version other than RETS 1.5
        //$rets->AddHeader("RETS-Version", "RETS/1.7.2");
//echo "url is " . $this->mls_options['url'] . '<br/>';
//echo $this->mls_options['user'], $this->mls_options['pwd'] . ' ' . $this->mls_options['user'], $this->mls_options['pwd'] . ' ' . $this->mls_options['agent'] . '<br/>';
//        $mls_settings = array(
//            'url'   =>  'http://csrets.mris.com:6103/cornerstone/login',
//            'user'  =>  '3082558',
//            'pwd'   =>  '855177',
//            'agent' =>  'RETS Test/1.0',
//            'unique'    =>  'ListingKey',
//        );    

// **** TEMP to restore Dev site access        
//        $this->mls_options['url']  = 'http://cornerstone.mris.com:6103/platinum/login';
//        $this->mls_options['user'] = '3082558';
//        $this->mls_options['user'] = '3082558';
//        $this->mls_options['pwd']  = '855177';
//        $this->mls_options['agent'] = 'RETS Test/1.0';
//        $this->mls_options['unique']  = 'ListingKey';

//        $this->rets->AddHeader("User-Agent", $this->mls_options['agent']);
        $this->rets->AddHeader("User-Agent", $this->options['agent']);
        if ( ! empty($this->options['rets-version']) ) {
            $this->rets->AddHeader("RETS-Version", $this->options['rets-version']);
        }
        
// Turn on PHRETS debug log        
//$this->rets->SetParam( 'debug_mode', true);

//        echo "+ Connecting to MLS server...  ";
        $connect = $this->rets->Connect( $this->options['url'], $this->options['user'], $this->options['pwd'] );

        // check for errors
        if ( $connect ) {
//                echo "Connected<br/>\n";
            $this -> connected = true;
        } else {
            echo "connect is $connect <br/>";
            echo "Error connecting to MLS server:<br>\n";
            print_r($this->rets->Error());
            echo "<br/>\n";
//            $log = "";
            exit;
        }
        $temp = 1;

        return;
    }       // end connect()
    
    
    function __construct_2() {
        // For PHRETS v. 2.x
        // Get options
        $this->mls_options = array(
            'url'   =>  'http://csrets.mris.com:6103/cornerstone/login',
            'user'  =>  '3082558',
            'pwd'   =>  '855177',
            'agent' =>  'RETS Test/1.0',
            'unique'    =>  'ListingKey',
        );


//        $config = new \PHRETS\Configuration;        
        $config = new \PHRETS\Configuration;
        $config->setLoginUrl($this->mls_options['url']);
        $config->setUsername($this->mls_options['user']);
        $config->setPassword($this->mls_options['pwd']);

        $config->setUserAgent($this->mls_options['agent']);
        $this->rets = new \PHRETS\Session($config);
        
        // For debugging -- DOESN'T WORK
//        $this->rets->SetParam("debug_mode", true);
//        $this->rets->SetParam("debug_file", "debug_log.txt");        
        
        $bulletin = $this->rets->Login();
        
        $temp = 1;
    }
    
    function get_mls_info() {
        echo "<br/>MRIS Database Information<br/>";
        $resources = $this->rets->GetMetadataResources();
        foreach ($resources as $resource) {
                echo "<b>Resource {$resource['ResourceID']}</b><br/>\n";
                $classes = $this->rets->GetMetadataClasses($resource['ResourceID']);
                foreach ($classes as $class) {
                        echo "   + Class {$class['ClassName']} described as " . $class['Description'] . "<br/>\n";
                }
        }        
        
    }
    
    function mls_search( $resource, $class, $query, $parms='' ) {
        // Search mls database
        // Returns $search
        if ( empty($parms) ) {
            $parms = array( "Count" => 1, "Format" => "COMPACT", "Limit" => 100);
        }
        $search = $this->rets->SearchQuery( $resource, $class ,$query, $parms);
        return $search;
    }
    
    function mls_record_count() {
        $record_count = $this->rets->TotalRecordsFound();   // This does not work if count=0
        return $record_count;
    }
    
    function mls_get_row($search) {
        // Get the next row in an mls search
        $result = $this->rets->FetchRow($search);
        return $result;
    }
    
    function mls_free_search($search) {
        // Free the results of a search
        $this->rets->FreeResult($search);        
    }

    function import_fields( $type='sales' ) {
        // Imports field list from MRIS database        
        // Uses mris_fields.csv to determine which fields to import
        // Updates the nrl_fields table and the 'field_formats' nrl setting
        // Based on code from PHRETS Wiki
        global $wpdb;

        $resource = "Property";
        if ( $type == 'sales') {
            $class = 'RESI';
            $match_col = 1;
        } else if ( $type == 'rentals' ) {
            $class = 'RLSE';
            $match_col = 2;
        } else {
            // Error
            return "+ Invalid field import type: $type";
        }
        $table = "nrl_fields_$type";
        $table_name = $wpdb->$table;
        
//        $rets_resource_info = $this->rets->GetMetadataInfo();
        
        // pull field format information for this class
        $rets_fields = $this->rets->GetMetadata( $resource, $class );
        
        // Get list of fields to import
//        $import_fields = $this->get_csv_fields(NIMP_PATH . "mris_fields.csv",true);     
        $import_fields = NIMP_Util::csv_file_get( NIMP_PATH . 'setup/' . "mls_import_fields.csv",true);     
        $temp = 1;
        
        // Import the field names
        $field_values = array(
            'SystemName' => 0,
            'LongName' => 0,
            'MaximumLength' => 0,
            'DataType' => 0,
            'Precision1' => 0,
            'Interpretation' => 0,
            'UseSeparator' => 0,
            'EditMaskID' => 0,
            'LookupName' => 0,
            'MaxSelect' => 0,
        );
        
        $field_formats = array();
        $field_settings = array();
        $lookup_fields = array();
        $fields_added = 0;
        $fields_replaced = 0;
        foreach ( $rets_fields as $field ) {
//            if ( in_array($field['SystemName'], $import_fields) ) {
            if ( ! empty($import_fields[$field['SystemName']]) && $import_fields[$field['SystemName']][0] == 'X' && $import_fields[$field['SystemName']][$match_col] == 'X' ) {
                $temp = 1;
                foreach ( $field_values as $fname => $fvalue ) {
                    // One column has a different name in our table
                    $col_name = ( $fname != 'Precision1' ? $fname : 'Precision' );
                    $field_values[$fname] = ( isset($field[$col_name]) ? $field[$col_name] : '' );
                }
                
                // PHP doesn't have long int, so change long numbers to varchar
                if ( $field_values['Interpretation']=='Number' && $field_values['DataType']=='Long'  ) {
                    $field_values['Interpretation'] = '';
                    $field_values['DataType'] = 'Character';
                    $temp = 1;
                }
                
                // Set the field format (used for MySQL import/replace)
                if ( $field_values['Interpretation'] == 'Number' ) {
                    if ( $field_values['DataType'] == 'Decimal' ) {
                        $field_formats[$field_values['SystemName']] = '%f';
                    } else {
                        $field_formats[$field_values['SystemName']] = '%d';
                    }
                } else {
                    $field_formats[$field_values['SystemName']] = '%s';
                }
                
                // Gather a list of Lookup fields
                if ( $field_values['Interpretation'] == 'Lookup' || $field_values['Interpretation'] == 'LookupMulti' ) {
//                    $lookup_fields[] = $field_values['SystemName'];
                    $lookup_fields[$field_values['SystemName']] = $field_values['LookupName'];
                }
                
                $field_settings[$field_values['SystemName']] = array(
                    'Show'  =>  1,
                    'Empty' =>  1,
                    'Format'    => $field_formats[$field_values['SystemName']],
                );
                

                
                $temp = 1;                
                $result = 0;
                
                // The fields table is used to create the listings database
                $result = $wpdb->replace( $table_name, $field_values );    
                if ( $result===1 ) {
                    $fields_added++;
                } elseif ( $result > 1 ) {
                    $fields_replaced += $result - 1;
                }
                
            } else {
                // For testing..
                $temp = 1;
            }
            $temp = 1;
        }  // end foreach
        
        // Update options
//        $mls_options = NIMP_MLS_Util::get_options();
//        $mls_options['import_fields'] = $import_fields;
//        $mls_options['field_formats'] = $field_formats;
//        NIMP_MLS_Util::update_options( $mls_options, 'NIMP_MLS::import_fields()' );
//        update_option( NIMP_FIELDS_OPTION_NAME, $field_settings );
        
// ### Do I even need to save $import_fields?        
//        NRL_Common::update_nrl_setting( 'import_fields', $import_fields );  // This is an array of MLS fields to import from the .csv file
        ksort( $lookup_fields );
        NRL_Common::update_nrl_setting( "field_formats_$type", $field_formats );  // This is an array of field formats, using the field name as array key
        NRL_Common::update_nrl_setting( "lookup_fields_$type", $lookup_fields );

        $msg =  "+ Listing $type fields imported: $fields_added fields added, $fields_replaced fields replaced";
        return $msg;

    }       // end import_fields()

    function get_field_format ( $field_values ) {
        // Return the field format used for mySQL data formatting
        if ( $field_values['Interpretation'] == 'Number' ) {
            if ( $field_values['DataType'] == 'Decimal' ) {
                $format = '%f';
            } else if ( $field_values['DataType'] == 'Long' ) {
                // PHP does not have Long Int, so we store it as character
                $format = '%s';
            } else {
                $format = '%d';
            }
        } else {
            $format = '%s';
        }  
        return $format;
    }
    
    function import_media_fields() {
        // Set up the list of Media resource fields to import, along with the field formats
        // This is used to create up the media table
        
        $media_import_fields = NIMP_Util::csv_file_get_assoc( NIMP_PATH . "setup/media_import_fields.csv" );
        $media_fields = array();
        $media_formats = array();
        $lookup_fields = array();
        foreach ( $media_import_fields as $field ) {
            if ( $field['Import'] == 'X' ) {
                $media_formats[$field['SystemName']] = $this->get_field_format( $field ); // $field['Format'];
                unset( $field['Import'] );
                // For backwards compatibility...
                if ( isset($field['Format']) ) {
                    unset( $field['Format'] );
                }
                $media_fields[] = $field;
                // Save a list of lookup fields
                if ( $field['Interpretation'] == 'Lookup' || $field['Interpretation'] == 'LookupMulti' ) {
//                    $lookup_fields[] = $field_values['SystemName'];
                    $lookup_fields[$field['SystemName']] = $field['LookupName'];
                }                
            }
        }
        
        NRL_Common::update_nrl_setting( "media_fields", $media_fields );
        NRL_Common::update_nrl_setting( "field_formats_media", $media_formats );
        NRL_Common::update_nrl_setting( "lookup_fields_media", $lookup_fields );
        $msg =  "+ Media fields imported: " . count($media_fields) . " fields imported";
//        return $media_formats;
        return $msg;
        
    }
    
    
    function search($query) {
        // KPC *** Class 'MF' is Multi-family properties.  Do I need to incluude that, as well as RES?
//        $fields = implode(",", $this->get_csv_fields(NIMP_PATH . "mris_fields.csv",true));
//        $fields = $this->get_csv_fields(NIMP_PATH . "mris_fields.csv",true);
//        $args = array( "Count" => 1, "Format" => "COMPACT", "Limit" => "20", "Select" => "FullStreetAddress,StreetNumber,StreetName,CityName,PostalCode,ListPrice"); 
//        $search = $this->rets->SearchQuery("Property","RES","(ListDate=2015-06-27+)",
//          $query = '((StandardStatus=|10000069142),(ForSale=1),(ListDate=1980-01-01+),(County=|10000005756,10000005839,85044057103,10000005773,10000005850,10000005851,10000005860,10000005861,10000005816))';
        $query = '((County=|10000005756,10000005839,85044057103,10000005773,10000005850,10000005851,10000005860,10000005861,10000005816))';
//          $query = '((ListDate=1980-01-01+),(County=|10000005756,10000005839,85044057103,10000005773,10000005850,10000005851,10000005860,10000005861,10000005816))';
        $query = '((StandardStatus=|10000069142),(ForSale=1),(ListDate=2015-08-18+),(County=|10000005756,10000005839,85044057103,10000005773,10000005850,10000005851,10000005860,10000005861,10000005816))';

        $search = $this->rets->SearchQuery("Property", $this->mls_class,$query, 
//                $args
//        array( "Count" => 1, "Format" => "COMPACT", "Limit" => "20", "Select" => "FullStreetAddress,StreetNumber,StreetName,CityName,PostalCode,ListPrice")
//        array( "Count" => 1, "Format" => "COMPACT", "Limit" => "20")
        array( "Count" => 2, "Format" => "COMPACT")
        );
        $record_count = $this->rets->TotalRecordsFound();
        $temp = 1;
        return;
        while ($listing = $this->rets->FetchRow($search)) {
            echo '<p>';
            if ( ! empty($listing['FullStreetAddress']) ) {
                echo "Address: <i>{$listing['FullStreetAddress']}</i>, ";
            } else {
                echo "Address: {$listing['StreetNumber']} {$listing['StreetName']}, ";
            }
            echo "{$listing['CityName']}, ";
//            echo "{$listing['State']} {$listing['PostalCode']} listed for ";
            echo " {$listing['PostalCode']} listed for ";
            echo "\$".number_format($listing['ListPrice'])."\n";
            echo '</p>';
        }
        $this->rets->FreeResult($search);        
        return;       
        
    }
    
    function import_mls( $type='sales' ) {
        // Get mls listings and store them in the database
        global $wpdb;
        define('NIMP_LOG_TIME', false);
        define('NIMP_DETAILED_LOG', true);
        
//        if ( ! empty($this->options['suspend_imports']) && $this->options['suspend_imports'] == 'true' ) {
        if ( $this->options['suspend_imports'] ) {
            echo "<br/><b>Imports are suspended!</b><br/>";
            return;
        }
        
        switch ( $type ) {
            case 'sales':
                $this->mls_class = 'RESI';
                $table_name = $wpdb->nrl_properties_sales;
                $this->status = NRL_Common::get_nrl_setting( 'import_status_sales' );
                break;
            case 'rentals':
                $this->mls_class = 'RLSE';
                $table_name = $wpdb->nrl_properties_rentals;
                $this->status = NRL_Common::get_nrl_setting( 'import_status_rentals' );
                break;
            default:
                return;
        }
        
        if ( $this->status['initial_import'] ) {
            $this->options['import_limit'] = 150;            
        } else {
            $this->options['import_limit'] = 150;
        }
        
        $t = array();
        $t['start'] = microtime(true);
        
        // Is this a new pass?
        if ( $this->status['new_pass'] ) {
            $result = $this->get_mls_listings( $type );
            // Are there listings and is there time to process some listings?
            if ( ! $result || (microtime(true) - $t['start']) > NIMP_IMPORT_TIME_LIMIT/2 ) {            
                return;
            }
        }

        // Start log entry
        $log = "\n" . date('Y-m-d H:i:s') . ' Import started,  offset = ' . $this->status['offset'] . ', Limit = ' . $this->options['import_limit'];
        echo $log . "<br/><br/>\n\n";
        if ( NIMP_DETAILED_LOG ) {
    //        $log = "\n\nImport started " . date('Y-m-d H:i:s') . ',  offset = ' . $this->mls_options['offset'] . ', Limit = ' . $this->mls_options['import_limit'];
            // Write the log now, so we can detect it if the php execution time limits shuts this down
            NRL_Common::write_log($log, NIMP_IMPORT_LOG . "_$type.txt");
        }

        // Build query to get the next set of listings based on the Key Value list
//        $listing_keys = get_option( NIMP_LISTINGS_OPTION_NAME );
        $listing_keys = NRL_Common::get_nrl_setting( "mls_listings_$type" );
        if ( empty( $listing_keys) ) {
            // We are stuck (this shouldn't happen)
            $this->status['new_pass'] = true;
            NRL_Common::update_nrl_setting( "import_status_$type", $this->status );
            return;
        }

        $keys_to_get = array_slice( $listing_keys, $this->status['offset'], $this->options['import_limit'] );
        
        if ( empty($keys_to_get) ) {
            // This should not happen
            $this->status['new_pass'] = true;
            NRL_Common::update_nrl_setting( "import_status_$type", $this->status );
            return;
        }
        
        $query = '((ListingKey=' . implode( ',', $keys_to_get ) . '))';
        $temp = 1;

        // Get the requested listings
        $search = $this->rets->SearchQuery("Property", $this->mls_class, $query, 
            array( "Count" => 1, "Format" => "COMPACT")
            );
        if ( $search === false ) {
            // No listings were returned
            $log = "\n*** No listings returned.  Query=$query\n";
            NIMP_Util::error_log($log);
            // Fix problem with getting hung up with 1 listing remaining
            // KPC This needs further study!
            if ( $this->status['remaining'] == 1 ) {
                $this->status['remaining'] = 0;
            }
//            $this->status['remaining'] = 0;
            $temp = 1;
        }
        
        
        $record_count = $this->rets->TotalRecordsFound();   // This does not work if count=0        
        $t['listings'] = microtime(true) - $t['start'];
        
        // Load settings
//        $import_fields = NRL_Common::get_nrl_setting('import_fields');
        $field_formats = NRL_Common::get_nrl_setting("field_formats_$type");
        $lookup_fields = NRL_Common::get_nrl_setting("lookup_fields_$type");
        $lookups = NRL_Common::get_nrl_setting( "lookups_property" );
        $this->media_formats = NRL_Common::get_nrl_setting( "field_formats_media" );
//        $rental_zips = NRL_Common::get_nrl_setting('rental_zips');


// ### Fail if $field_formats is not defined
 
        // Add/Update the listings in the database
        $result_added = 0;
        $rentals_added = 0;
        $result_replaced = 0;
        $results_skipped = 0;
        $results_deleted = 0;
        $photos_added = 0;
        $photos_updated = 0;
        $photos_deleted = 0;
        $t['records'] = 0;
        $t['pictures'] = 0;
        $cnt = 0;
        $time_break = false;    // Did we stop importing due to the time limit?
        
        // Do name case correction on the following fields
        $case_correction = array('City','FullStreetAddress','BuilderModel', 'SubdivisionName', 'TaxSubdivision',
            'ElementarySchool','MiddleOrJuniorSchool','HighSchool', 'SchoolDistrictName', 'BuilderName','IncorporatedCityName');

//echo 'Missing Fields: <br/>';
        
        while ( $listing = $this->rets->FetchRow($search) ) {
            $cnt++;
            
            // See if this property is already in the database
            $query = "SELECT id,StandardStatus,TotalPhotos,ModificationTimestamp,ListingId,ListingKey,PhotosChangeTimestamp FROM $table_name WHERE ListingKey={$listing['ListingKey']}";
//            $status = $listing['StandardStatus'];
            $current_record = $wpdb->get_row( $query );

            // Import only Actvie records or records already in the DB. For rentals, only with certain zip codes
            
            if ( ( ! $this->status['initial_import'] && $listing['StandardStatus']!='10000069142' && $current_record===NULL ) ) { // || ( $type == 'rentals'
//                    && ( ! in_array($listing['PostalCode'], $rental_zips) ) && strpos($listing['ListOfficeName'], NIMP_OUR_AGENCY)===false ) ) {  
//                    && strpos($listing['ListOfficeName'], NIMP_OUR_AGENCY)===false ) ) {  
                // The property is not active and it is not already in our database    //or it is a rental not in the zip list
                $results_skipped++;                
                continue;
            }

            // ***** Prepare fields for adding to database *****
            $t_start = microtime(true);
            $formats = array();
            $fields = array();
            $missing_fields = 0;
            $rewrite_url = false;
            
            foreach ( $field_formats as $key => $format ) {            
//            foreach ( $import_fields as $key ) {
                
                if ( isset($listing[$key]) ) {
                    
                    $fields[$key] = $this->format_import_field( $listing[$key], $format );
                    $formats[] = $format;

                    // Replace Lookup code with actual text
                    if ( ! empty($listing[$key]) && ! empty($lookup_fields[$key]) && ! empty($lookups[$lookup_fields[$key]]) ) {
                        $fields[$key] = $this->format_lookup_field( $fields[$key], $lookups[$lookup_fields[$key]] );
                        $temp = 1;
                    }   
                    
                    // Correct name fields in all upper case
                    if ( ! empty($fields[$key]) && in_array($key, $case_correction) ) {
                        $temp = $fields[$key];
                        $fields[$key] = NRL_Common::name_case($fields[$key]);
//                        $temp2 = $fields[$key];
                    }
                    
                    // Modify the photo urls (we will use rewrite to use our own host and path)
                    if ( ! empty($fields[$key]) && strpos( $key, 'ListPicture') !== false  ) {
                        if ( strpos( $fields[$key], BRIGHT_IMAGE_HOST) !== false ) {
                            $len = strlen( BRIGHT_IMAGE_HOST );
                            $fields[$key] = substr( $fields[$key], $len );
                            $rewrite_url = true;
                        }
                    }
                    
                } else {
                    // Field not present in listing
                    $missing_fields++;
// XXX TEMP                    
if ( $cnt === 1 ) {
    echo '- ' . $key . '<br/>';
}

                }
            }   // foreach

            if ( $rewrite_url ) {
                $fields['RewriteURLs'] = '1';
                $formats[] = '%d';
            }            
            
            // See if this property is already in the database
//            $table_name = $wpdb->prefix . 'nrl_properties';    
            if ( $current_record === NULL ) {
                
                // Not found, add a new record
                $update = false;
                $result = $wpdb->replace( $table_name , $fields, $formats );
                $this_id = $wpdb->insert_id;
                $fields['id'] = $this_id;
//                $num_photos = 0;
                $photo_result = $this->media_data_import( $fields['ListingKey'], true );
                $result_added++;
                
                if ( NIMP_DEV_USER ) {
//                    $url = home_url('/') . NIMP_LISTINGS_DIR . '/mls/' . $listing['ListingId'];

// ## The following function needs to be updated before being called                    
//                    $url = NRL_Common::format_listing_page_url( $fields );
//                    echo "<a href='$url'>{$listing['FullStreetAddress']} \${$fields['ListPrice']}</a> -- $type<br/>";
//                    echo " ( $fields['ForSale']==1 ? '' : ' rental' ) . '<br/>';
                }
            } else {
                
                // Replace existing record
                if ( NIMP_DEV_SITE ) {
                    echo 'Replacing mls: ' . $current_record->ListingId . ' ListingKey: ' . $current_record->ListingKey . 
                        ' Modified: ' . $current_record->ModificationTimestamp . "<br/>\n";
                }
                $update = true;
//                $old_mod_date = $current_record->ModificationTimestamp;
                
                // UPDATE: We will never delete the rental listings
//                if ( $fields['StandardStatus'] != 'Active' && $fields['StandardStatus'] != 'Pending' && $type == 'rentals' ) {
//                    // This is a rental that is no longer on the marked. Delete record and the media data
//                    $result = $wpdb->delete( $table_name , array( 'id' => $current_record->id ),  array( '%d' ) );
//                    if ( $result != 1 ) {
//                        // Error deleting record
//                        NIMP_Util::error_log("Error deleting record with id=$current_record->id, Result=$result");
//                        $temp = 1;
//                    } else {
//                        $results_deleted++;
//                    }                    
//                    // Delete media data
//                    $result = $wpdb->delete( $wpdb->nrl_media , array( 'ResourceRecordKey' => $fields['ListingKey'] ),  array( '%s' ) );
//                    if ( ! empty($result) ) {
//                        $photos_deleted += $result;
//                    }
//                } else {
                
                    // Update the photos if necessary
                    if ( strtotime($current_record->PhotosChangeTimestamp) < strtotime($fields['PhotosChangeTimestamp'])  ) {
                        $photo_result = $this->media_data_import( $fields['ListingKey'], $new = false );
                    }
                    // Update the record
                    $fields['id'] = $current_record->id;
                    $formats[] = '%d';
                    $result = $wpdb->replace( $table_name , $fields, $formats );
                    if ( $result < 1 ) {
                        $temp = 1;
                    }
//                    $this_id = $current_record->id;
    //                $temp = $wpdb->insert_id;     // This works as well
                    $result_replaced++;                    
//                }

            }   // end if ( $current_record == NULL ) .. else ..
            // To find an error, use $wpdb->last_error
            $temp = 1;
            
            if ( ! empty($photo_result) ) {
                $photos_added += $photo_result['added'];
                $photos_updated  += $photo_result['updated'];
                $photos_deleted += $photo_result['deleted'];
            }
            
            $t['records'] += microtime(true) - $t_start;

            // If the Listing Status is Active, import photos. [** MOVED **]
            $t_start2 = microtime(true);

            $t['pictures'] += microtime(true) - $t_start2;
            
            // Add city to cities table
            NIMP_MLS_Util::add_city( $fields, $type );
//                NIMP_MLS_Util::create_listing_page($fields);
             
            // Is there time to continue?
            if ( (microtime(true) - $t['start']) > NIMP_IMPORT_TIME_LIMIT ) {
                $time_break = true;
                break;
            }
        }   // end while $listing   
        
        // See if we are stuck
        // 
// #### See code above loop.. is this ever okay?        
        
        if ( $cnt === 0 ) {
            if ( $this->status['error_cnt']++ === 2 ) {
                // Send an email notification on the third consecutive error
                $msg = "Imports $type not working: No listngs returned.\nOffset: {$this->status['offset']}, "
                ."Remaining: {$this->status['remaining']}, Record count: $record_count<br/>";
                NIMP_MLS_Util::send_admin_email($msg);
            }
        } else {
            $this->status['error_cnt'] = 0;
        }
        
        $this->status['offset'] += $cnt;
//            $this->status['remaining'] = ((int) $record_count) - $this->status['offset'] + 1;
        $this->status['remaining'] -= $cnt;
        $this->status['added'] += $result_added;
//        $this->status['rentals_added'] += $rentals_added;
        $this->status['updated'] += $result_replaced;
        $this->status['deleted'] += $results_deleted;
        $this->status['photos_added'] += $photos_added + $photos_updated;
        $this->status['photos_deleted'] += $photos_deleted;
        
        // Release some memory space
        $this->rets->FreeResult($search);        
//        unset($listing_keys);
        unset($lookups);
//        unset($field_interp);
        
        // Are we finished with this pass?
//        if (  ! $time_break && $cnt < $this->status['import_limit'] ) {
        if (  $this->status['remaining'] <= 0 ) {
            // We are finished with this pass (it should never be < 0)
            $this->status['new_pass'] = true;
        }
        NRL_Common::update_nrl_setting( "import_status_$type", $this->status );
        
        $time_elapsed =  round( (microtime(true) - $t['start']), 4);
        if ( NIMP_DETAILED_LOG ) {
            // Write to log file
            $log = ',  Total time: ' . $time_elapsed . ' sec';
            if ( $this->status['initial_import']) {
                $log .= "  *** Initial Import Phase {$this->status['initial_import_phase']} ***";
            }
//            $log .= "\nResults: " . $result_added . ' listings added, ' . $rentals_added . ' rentals added, ' .  $result_replaced . ' replaced, ' 
            $log .= "\nResults: " . $result_added . ' listings added, ' . $result_replaced . ' replaced, ' 
                    . $results_skipped . ' skipped, ' . $photos_added . ' media records added, ' . 
                    "$photos_deleted media records deleted, $results_deleted rental listings deleted";
            if ( $results_deleted > 0 ) {
                $log .= ' #####';
            }
            $log .= "\n";
            if ( $this->status['offset'] > 1 ) {
    //            $this->mls_options['remaining'] = ((int) $record_count) - $this->mls_options['offset'] + 1;
                $log .= $this->status['remaining'] . " listings left in set.\n";
            } else {
                $log .= "Set is finished.\n";
    //            $this->mls_options['remaining'] = 0;
            }

    //        $log .= "\n";
            if ( NIMP_LOG_TIME ) {
                $log .= "\nTime: Get listings: " . round($t['listings'], 4) . ' sec,  Save to DB: ' . round($t['records'], 4). ' sec,  Get pictures: ' 
                        . round($t['pictures'], 4) . ' sec';            
            }
    //        $log .= "\n";

            NRL_Common::write_log( $log, NIMP_IMPORT_LOG . "_$type.txt" );
        }
        echo '<p>' . $result_added . " listings added</p>\n";
//        echo '<p>' . $rentals_added . " rentals added</p>";
        echo '<p>' . $result_replaced . " listings replaced</p>\n";
        echo '<p>' . $results_deleted . " listings deleted (rentals)</p>\n";
        echo '<p>' . $results_skipped . " listings skipped</p>\n";
        echo '<p>' . $photos_added . " photos added</p>\n";
        echo '<p>' . $photos_deleted . " photos deleted</p>\n";
        if ( $this->status['new_pass'] ) {
            echo "<p>Finished importing this pass</p>\n";
        }
        echo "<p>Total time: $time_elapsed sec</p>\n";
        
        return true;
    }   // end function import_mls()
    
    function get_mls_listings( $type ) {
        // Get a list of Key Values to process for the next import pass
        
        $t_start = microtime(true);

        $query = '(';
        if ( $type == 'rentals' ) {
            $rental_zips = NRL_Common::get_nrl_setting('rental_zips');
            if ( ! empty($rental_zips) ) {
                $query = '((PostalCode=|' . implode( ',', $rental_zips ) . '),';
                
// XXX This also needs to include record like this: strpos($listing['ListOfficeName'], NIMP_OUR_AGENCY)===false ) for rentals (or sales too?)                
                
            } 
        }
        
        echo "Getting mls listings for new pass..\n";
        
        // Ready to begin a new pass
        if ( ( ! $this->status['initial_import'] || ! $this->status['initial_import_phase'] == 0 ) && $this->status['offset'] > 0  ) {
            // If this is not the very first pass of initial import, write the log from the previous pass
            $total_time = time() - $this->status['pass_start_datetime'];
            $pass_start_time = date('Y-m-d',$this->status['pass_start_datetime']). ' ' . date('H:i:s',$this->status['pass_start_datetime']);
            $times = NRL_Common::elapsed_time_string($total_time);
            $log = ($this->status['initial_import'] ? "** Initial Import $type phase {$this->status['initial_import_phase']} Pass Finished " : 'Import Pass Finished ');
            $log .=  date('Y-m-d'). ' ' . date('H:i:s') . " (Started: $pass_start_time)\n";
            $log .= "{$this->status['offset']} listings processed, {$this->status['added']} added, " .
                    "{$this->status['updated']} updated, {$this->status['photos_added']} media records added, " .
                    "{$this->status['photos_deleted']} media records deleted, {$this->status['deleted']} listings deleted\n";
            if ( ! $this->status['initial_import'] ) {
                $log .= "Total time: {$times['days']} days, {$times['hours']} hours, {$times['minutes']} minutes.\n";
            }
            $log .= "\n";
            NRL_Common::write_log( $log, NIMP_IMPORT_SUMMARY_LOG . "_$type.txt" );
        }

        // Reset options for new pass
        $this->status['prev_start_time'] = $this->status['new_start_time'];
        $this->status['pass_start_datetime'] = time();
//            $this->status['new_pass'] = true;
        $this->status['offset'] = 0;
        $this->status['remaining'] = 0;
        $this->status['added'] = 0;
//            $this->status['rentals_added'] = 0;
        $this->status['updated'] = 0;
        $this->status['deleted'] = 0;
        $this->status['photos_added'] = 0;
        $this->status['photos_deleted'] = 0;
        $this->status['error_cnt'] = 0;

        // *** Initial Import ***
            
        if ( $this->status['initial_import'] ) {
            if ( $this->status['initial_import_phase'] == 0 ) {
                // This is the first time running the initial import
                // Save the date-time so that we can check for modifications after initial import
//                $this->status['prev_start_time'] = date('Y-m-d'). 'T' . date('H:i:s');
                // Set the date to use for initial import passes (not needed after initial imports)
                $start_date = time() - 30 * 30 * 24 * 60 * 60;
                $this->status['prev_start_time'] = date('Y-m-d', $start_date);      // For imports, use the date only, not the datetime
                $this->status['initial_start_date'] = date('Y-m-d', $start_date); 
                $this->status['initial_import_phase'] = 1;
                $temp = 1;
            } else {
                $start_date = strtotime($this->status['prev_start_time']); // + 30 * 24 * 60 * 60;
            }
            
            // See if this phase is complete
            if ( $start_date > time() ) {
                // We are finished with this phase
                if ( ++$this->status['initial_import_phase'] > 2 || $type == 'rentals' ) {
                    // Initial imports are finished (for rentals, import only active listings, so no phase 2 is needed
                    $this->status['initial_import'] = false;
//                    $next_start_date = strtotime($this->status['initial_start_date']); // + 30 * 24 * 60 * 60;
//                    $this->status['prev_start_time'] = date('Y-m-d', $next_start_date). 'T' . date('H:i:s', $next_start_date);
                    $start_date = strtotime($this->status['initial_start_date']);
                    $this->status['prev_start_time'] = date('Y-m-d', $start_date). 'T' . date('H:i:s', $start_date);
                    $log = "\n\n********** Initial Imports: $type Finished.\n\n";
                    NRL_Common::write_log( $log, NIMP_IMPORT_SUMMARY_LOG . "_$type.txt" );
                    NRL_Common::update_nrl_setting( "import_status_$type", $this->status );                    // Return to start regular imports on the next pass
                    return false;
                } else {
                    // Reset the start date = the past 18 months for phase 3
                    $start_date = time() - 18 * 30 * 24 * 60 * 60;
                    $this->status['prev_start_time'] = date('Y-m-d', $start_date);      // For imports, use the date only, not the datetime
                    $log = "\n********** Initial Imports: $type Phase {$this->status['initial_import_phase']} Started **********  \nNew start date: " 
                        . $this->status['prev_start_time'] . "\n";
                    // Write the log now, so we can detect it if the php execution time limits shuts this down
                    NRL_Common::write_log( $log, NIMP_IMPORT_SUMMARY_LOG . "_$type.txt" );
                    // Save settings and return.. ready to begin next phase or regular imports
                    NRL_Common::update_nrl_setting( "import_status_$type", $this->status );
                }
                $temp = 1;
//                return false;   

            }
            //else {
            // Continue this phase..
            $new_start_time = date('Y-m-d', $start_date + 30 * 24 * 60 * 60);
            if ( $this->status['initial_import_phase'] == 1 ) {
                $query .= "(StandardStatus=|10000069142),(MLSListDate={$this->status['prev_start_time']}-$new_start_time),(County=|10000005756,10000005839,85044057103,10000005773,10000005850,10000005851,10000005860,10000005861,10000005816))";
            } else {
                $query .= "(StandardStatus=~10000069142),(MLSListDate={$this->status['prev_start_time']}-$new_start_time),(County=|10000005756,10000005839,85044057103,10000005773,10000005850,10000005851,10000005860,10000005861,10000005816))";
            }
//            }            
        } else {
            
        // *** Regular imports ***
            
            // If it has been a long time since the last pass, limit the time to one week
            if ( ! empty($this->status['prev_start_time']) ) {
                $prev_datetime = strtotime($this->status['prev_start_time']);
                if ( (time() - $prev_datetime) > 7*24*3600 ) {
                    $new_datetime = $prev_datetime + 7*24*3600;
                } else {
                    $new_datetime = time();
                }
            } else {
                $new_datetime = time();
            }

            $new_start_time = date('Y-m-d', $new_datetime). 'T' . date('H:i:s', $new_datetime);
            $prev_start_time = ( ! empty($this->status['prev_start_time']) ? $this->status['prev_start_time'] : 'none' );
            $log = "\n\n********** New Pass Started **********  \nPrev Start Time: $prev_start_time New Start Time: $new_start_time";
            // Write the log now, so we can detect it if the php execution time limits shuts this down
            NRL_Common::write_log( $log, NIMP_IMPORT_LOG . "_$type.txt" );
        
//            $query = "((ModificationTimestamp={$this->status['prev_start_time']}+),(County=|10000005756,10000005839,85044057103,10000005773,10000005850,10000005851,10000005860,10000005861,10000005816))";
            $query .= "(ModificationTimestamp={$this->status['prev_start_time']}-{$new_start_time}),(County=|10000005756,10000005839,85044057103,10000005773,10000005850,10000005851,10000005860,10000005861,10000005816))";
        }
        
        // *** Get import ListingKey fields
        // Get the requested properties.  They will be retured sorted by ascending values of the Key Field
        $search = $this->rets->SearchQuery("Property", $this->mls_class ,$query, 
            array( "Count" =>1, "Format" => "COMPACT", "Select" => "ListingKey")
            );
        if ( $search === false ) {
            // No listings were returned
            $temp = 1;
//            $this->status['prev_start_time'] = $new_start_time;
            $this->status['new_start_time'] = $new_start_time;
            NRL_Common::update_nrl_setting( "import_status_$type", $this->status );
            echo "get_mls_listings() no listings found.\n";
            return false;
        }
        $record_count = $this->rets->TotalRecordsFound();   // This does not work if count=0
        
        $new_listings = array();
        while ( $listing = $this->rets->FetchRow($search) ) {
            $new_listings[] = $listing['ListingKey'];
            $temp = 1;
        }
        
        echo "get_mls_listings($type): " . count($new_listings) . " found.\n";
        NRL_Common::update_nrl_setting( "mls_listings_$type", $new_listings );
        
        $this->rets->FreeResult($search);        

        // Update MLS options for the new pass
        $count = count($new_listings);
        $this->status['new_start_time'] = $new_start_time;
        $this->status['new_pass'] = false;
        $this->status['offset'] = 0;
        $this->status['remaining'] = $count;
//        NIMP_MLS_Util::update_options( $this->mls_options, 'NIMP_MLS::get_mls_listings()' );
        
        NRL_Common::update_nrl_setting( "import_status_$type", $this->status );
        
        // Write to log
//        $log = "\n$count listings in set, Total time: " . round( (microtime(true) - $t_start), 4) . ' sec';
//        NRL_Common::write_log ($log, NIMP_IMPORT_LOG . "_$type.txt" );
        
        return true;
    }   // function get_mls_listings()

    
    function format_import_field( $field_value, $format ) {
        // Formats import field for mySQL database
        
        if ( isset($format) ) {
            switch ($format) {
                case '%d':
                    $value = (int) $field_value;
                    break;
                case '%f':
                    $value = (float) $field_value;
                    break;
                default:
                    $value = $field_value;
            }
//                $formats[] = $format;
        } else {
            // Error: missing field format, assume string
            $value = $field_value;
//                $formats[] = "%s";
            $format = "%s";
        } 

        return $value;
    }
    
    function format_lookup_field( $field_value, $lookups ) {
        // Substitute the lookup name value(s) for the lookup code(s)

        $temp = 1;
//            if ( ! empty($field) && ! empty($lookups) ) {
//                $lookup_name = $lookups;
            $codes = explode(',',$field_value);
            $firstcode = true;
            $newvalue = '';
            foreach ( $codes as $code ) {
                $mycode = trim($code);
                if ( isset($lookups[$mycode]) ) {
                    if ( $firstcode ) {
                        $firstcode = false;
                    } else {
                        $newvalue .= ', ';
                    }
                    $newvalue .= $lookups[$mycode];
                }                               
            }
            if ( ! empty($newvalue) ) {
                $field_value = $newvalue;
            } else {
                // Lookup code not found!
                $temp = 1;
            }
//            }          

            return $field_value;
    }
    
    function media_data_import( $listing_key, $new=false ) {
        // Import media records for $listing_key
        // If $new is true, this is a new listing and we don't need to check for existing media records
        // If records exist, look for modified records modified records
        // Returns the number of media records added, updated, and deleted
        global $wpdb;

        if ( empty($this->media_formats) ) {
            $this->media_formats = NRL_Common::get_nrl_setting( "field_formats_media" );
        }
        if ( empty($this->media_lookup_fields)) {
            $this->media_lookup_fields = NRL_Common::get_nrl_setting('lookup_fields_media');
        }
        if ( empty($this->media_lookups) ) {
            $this->media_lookups = NRL_Common::get_nrl_setting('lookups_media');
        }
        
        $temp = 1;
        
        if ( ! $new ) {
            // Check for existing media records
            $query = "SELECT id,MediaKey,MediaModificationTimestamp FROM $wpdb->nrl_media WHERE ResourceRecordKey=$listing_key";
            $result = $wpdb->get_results( $query, ARRAY_A );
            $existing_media = array();
            if ( ! empty( $result) ) {
                foreach ( $result as $record ) {
                    $existing_media[$record['MediaKey']] = array(
                        'id' => $record['id'], 
                        'MediaModificationTimestamp' => $record['MediaModificationTimestamp']
                        );
                    $temp = 1;
                }
            }
        }
        
        $query = "((ResourceRecordKey=$listing_key),(MediaCategory=|50000967225),(MediaVisibility=~200005792385))"; // Get only photos, not agent-only
        $search = $this->rets->SearchQuery("Media", "PROP_MEDIA",$query, 
            array( "Count" =>1, "Format" => "COMPACT", "Limit"=>"30") );
        if ( $search === false ) {
            // No listings were returned
            return false;
        }
        
        $record_count = $this->rets->TotalRecordsFound();   // This does not work if count=0        
        $added = $updated = $deleted = 0;
        $media_keys = array();
        while ( $media_record = $this->rets->FetchRow($search) ) {
                $media_keys[] = $media_record['MediaKey'];
                
            if ( $new || empty($existing_media[$media_record['MediaKey']]) ) {
                // Add new media record
                $result = $this->media_record_update( $media_record );
                $temp = 1;
                if ( $result ) {
                    $added++;
                }
            } else {
                // See if we need to update the existing record
                $existing_mod = strtotime( $existing_media[$media_record['MediaKey']]['MediaModificationTimestamp'] );
                $new_mod = strtotime( $media_record['MediaModificationTimestamp'] );
//                    if ( strcmp( $existing_media[$media_record['MediaKey']]['MediaModificationTimestamp'], $media_record['MediaModificationTimestamp'] ) !== 0 ) {
                if ( $existing_mod !== $new_mod ) {
                    $result = $this->media_record_update( $media_record, $existing_media[$media_record['MediaKey']]['id'] );
                    $temp = 1;
                    if ( $result ) {
                        $updated++;
                    }
                }
            }
            $temp = 1;
        }   // end while
        
        // See if we need to delete any existing media records
        if ( ! $new ) {
            foreach ( $existing_media as $key => $record ) {
                if ( ! in_array( $key, $media_keys ) ) {
                    // This record no longer exists on the Bright MLS server
                    $result = $wpdb->delete( $wpdb->nrl_media , array( 'id' => $record['id'] ),  array( '%d' ) );
                    $temp = 1;
                    if ( $result ) {
                        $deleted++;
                    }
                }
            }
        }        
        
        return array( 'added'=>$added, 'updated'=>$updated, 'deleted'=>$deleted );
    }
    
    function media_record_update( $media_record, $id=0 ) {
        // Update db media record with data in $media_record
        // If $id is > 0, update existing record
        // Return 1 for success or false for faliure
        global $wpdb;
        $data = array();
        $formats = array();
        $rewrite = false;
        $len = strlen( BRIGHT_IMAGE_HOST );
        $temp = 1;
        foreach ( $this->media_formats as $field => $format ) {
            if ( ! empty($media_record[$field]) ) {
                // Modify the photo urls (we will use rewrite to use our own host and path)
                if ( strpos( $field, 'URL') !== false  ) {
                    if ( strpos( $media_record[$field], BRIGHT_IMAGE_HOST) !== false ) {
                        $media_record[$field] = substr( $media_record[$field], $len );
                        $rewrite = true;
                    }
                }
                // Is this a lookup field?
                if ( ! empty($this->media_lookup_fields[$field]) ) {
                    $media_record[$field] = $this->media_lookups[$this->media_lookup_fields[$field]][$media_record[$field]];
                    $temp = 1;
                }
                $data[$field] = $this->format_import_field( $media_record[$field], $format );
                $formats[] = $format;                
            }
        }   // end foreach
        if ( $rewrite ) {
            $data['RewriteURLs'] = '1';
            $formats[] = '%d';
        }
        if ( $id > 0 ) {
            // Update the current record
            $data['id'] = $id;
            $formats[] = '%d';
        }
        $result = $wpdb->replace( $wpdb->nrl_media , $data, $formats );        
        return $result;
    }
    
    function media_data_delete( $listingkey, $keep=0 ) {
        // Delete media data records for $listingkey
        // Returns the number of records deleted
        // $keep is currently not needed and is not implemented

        $result = $wpdb->delete( $wpdb->nrl_media , array( 'ResourceRecordKey' => $listingkey ),  array( '%s' ) );
        
        return $result;

       
        
    }
    
    function photos_check_updates_OLD( $listing_key, $prev_date='' ) {
        // See if there have been media updates for listing $id since $prev_date

        $temp = 1;
        // For the search to work, the timestamp needs a 'T' between the date and time, not a space        
        if ( empty($prev_date) ) {
            $prev_date = '1960-01-01T00:00:00';
        } else {
            $prev_date = str_replace(' ', 'T', $prev_date);
        }
        $query = "((PropHistChangeType=MEDIA),(PropHistListingKey=$listing_key)"
                    . ",(PropHistChangeTimestamp=$prev_date+))";        
        $t_start = microtime(true);
        $search = $this->rets->SearchQuery("History","PROP_HIST",$query, 
            array( "Count" => 1, "Format" => "COMPACT", "Limit" => 100)   
        );
        $search_time = microtime(true) - $t_start;
        $temp = 1;
        $record_count1 = 0;
        if ( ! empty($search) ) {
            $record_count1 = $this->rets->TotalRecordsFound();   // This does not work if count=0
            echo "Media updates for ListingKey $listing_key : $record_count1 updates found<br/>\n";
            while ( $listing = $this->rets->FetchRow($search) ) {
                $temp = 1;
//                echo $listing['PropHistChangeTimestamp'] . "<br/>\n";
            }
            $this->rets->FreeResult($search);          
        }
        return $record_count1;
    }   // end function check_photo_updates()    
    
    function import_photos_OLD($listing_key, $id, $update, $num_photos) {
        // $listing_key is the listing key value
        // $id is the id fields of the property table record
        // $update is true if the record already exists, and is being updated
        // $num_photos is the number of photos for the current record (only used if $update=true)
        // Use glob() to find the photos for a specific id in the directory
        // Based on code in PHRETS wiki

        // If the record is being updated, check to see any photos need to be updated
        // Get the urls of the photos to get a count of photos (much faster than downloading the jpg data)
//        if ( $update ) {
//            $photos = $this->rets->GetObject("Property", "Photo", $listing_key, '*', 1);
//            if ( empty($photos) || count($photos) <= $num_photos ) {
//                return(0);
//            }            
//        }
        
        if ( NIMP_DEV_SITE ) {
            $pic_limit = 4;
        } else {
            $pic_limit = $this->options['picture_limit'];
        }
        // Get the photos for this property
        $photos = $this->rets->GetObject("Property", "Photo", $listing_key);
        $temp = 1;
        $cnt = 0;
        if ( ! empty( $photos) ) {
            // Create photo group directory if it does not exist
            $photo_dir = NRL_Common::get_photo_path($id);
            NIMP_MLS_Util::create_photo_path($photo_dir);

            $pad_id = str_pad($id, 2, "0", STR_PAD_LEFT); // If single digit, left pad with zero
            $photo_ids = array();
            $temp = 1;
            foreach ($photos as $photo) {
//                $listing = $photo['Content-ID'];
                $number = str_pad($photo['Object-ID'], 2, "0", STR_PAD_LEFT);

                if ($photo['Success'] == true) {
                    // Create the file if it does not exist
                    $fname = "{$photo_dir}image-{$pad_id}-{$number}.jpg";
                    if ( ! file_exists($fname) ) {
                        $result = file_put_contents($fname, $photo['Data']);
                        $cnt++;
//                        $filesize = filesize($fname);
                        $temp = 1;
                    } else if ( $update ) {
                        // The file already exists -- should we replace it?
//                        $filesize = filesize($fname);
                        $photo_ids[] = $photo['Object-ID'];
                        if ( filesize($fname) !== $photo['Content-Length'] ) {
                            // The file has changed--replace it
                            $result = file_put_contents($fname, $photo['Data']);
                            $cnt++;                            
                            $temp = 1;
                        }
                        $temp = 1;
                    } else {
                        // This shouldn't happen...
                    }
                } else {
                    echo "Error importing photo ({$id}-{$number}): {$photo['ReplyCode']} = {$photo['ReplyText']}<br/>\n";
                }
//                if ( $cnt >= $this->mls_options['picture_limit'] ) {
                if ( $cnt >= $pic_limit ) {
                    break;
                }
            }
            
            // Do I need to remove any photos?
            if ( $update && $num_photos > count($photos) ) {
                // If there are more photos than in the MLS database, delete the extras
                // I'm not sure that I can assume that $photo['Object-ID'] values are sequential without gaps
                $cnt2 = $cnt;
                while ( $cnt2++ < $num_photos ) {
                    // Be sure that this isn't a photo we need (safeguard)
                    if ( ! in_array($cnt2, $photo_ids)) {
                        $number = str_pad($cnt2, 2, "0", STR_PAD_LEFT);
                        $fname = "{$photo_dir}image-{$pad_id}-{$number}.jpg";
                        if ( file_exists($fname) ) {
                            $result = unlink($fname);
                            $temp = 1;
                        }
                    }
                }
                
            }
            
            $temp = 1;            
        }
        return($cnt);

    }   // function import_photos()
    
    function photos_get_data( $ListingKey ) {
        // Returns the list of photos for $ListingKey, but not the photos themselves
        if ( empty($ListingKey) ) {
            return '';
        }

        $query = "((ResourceRecordKey=$ListingKey),(MediaCategory=|50000967225),(MediaVisibility=~200005792385))"; // Get only photos, not agent-only
//        $query = "((MediaModificationTimestamp=2017-11-01 00:00:00-2018-11-21 00:00:00))";
//        $query = "((PropertyType=10000001073))";  // Residential
        $search = $this->rets->SearchQuery("Media", "PROP_MEDIA",$query, 
//            array( "Count" =>1, "Format" => "COMPACT", "Select" => "MediaKey,MediaModificationTimestamp,MediaFileName,MediaType,MediaCategory,MediaVisibility,MediaImageOf,MediaShortDescription,MediaSizeDescription,MediaBytes,MediaDisplayOrder,PreferredPhotoYN,MlsStatus",
//            array( "Count" =>1, "Format" => "COMPACT", "Select" => "MediaKey",
            array( "Count" =>1, "Format" => "COMPACT", 
                "Limit"=>"50")
            );
        if ( $search === false ) {
            // No listings were returned
            $temp = 1;
            return false;
        }
        
        echo "<br/>Media for ListingKey $ListingKey:<br/>";

        $record_count = $this->rets->TotalRecordsFound();   // This does not work if count=0        
        $new_media = array();
        $sample_image = '';
        while ( $listing = $this->rets->FetchRow($search) ) {
            $new_media[] = $listing; //['ListingKey'];
            
            echo "Order: " . $listing['MediaDisplayOrder'] . " " . $listing['MediaURLFull'] . '<br/>';
//            echo "Category: " . $listing['MediaCategory'] . "  Size: " . $listing['MediaSizeDescription'] . " Bytes: " . $listing['MediaBytes'] . " Image of: ". $listing['MediaImageOf'] . ' Short Description: '
            echo "..Image of: " . $listing['MediaImageOf'] 
                 . '<br/>..Short Description: ' . $listing['MediaShortDescription'] 
                 . '<br/>..Long Description: ' . $listing['MediaLongDescription']. '<br/>' ;
            
            $temp = 1;
            if ( empty($sample_image) && $listing['MediaBytes'] > 80000 ) {
                $sample_image = $listing['MediaKey'];
            }
        }
//        echo "<br/>Photo Objects: <br/>";
//        $photos = $this->rets->GetObject("Property", "Full", $ListingKey, '*', 1);
//        foreach ( $photos as $photo ) {
//            echo 'ID: ' . $photo['Object-ID'] . " " . $photo['Location'] . '<br/>';
//        }
        $temp = 1;
        
        if ( ! empty($sample_image) ) {
            $query = "((MediaKey=$sample_image))"; // Get only photos
            $search = $this->rets->SearchQuery("Media", "PROP_MEDIA", $query, 
                array( "Count" =>1, "Format" => "COMPACT", // "Select" => "MediaKey,MediaModificationTimestamp,MediaFileName,MediaType,MediaCategory,MediaVisibility,MediaImageOf,MediaShortDescription,MediaSizeDescription,MediaBytes,MediaDisplayOrder,PreferredPhotoYN,MlsStatus",
                    "Limit"=>"10")
                );
            if ( $search !==false ) {
                $photo = $this->rets->FetchRow($search);
//                $fname = "D:\xampp\htdocs\nesbitt\wp-content\NRL\$ListingKey.jpg";
//                $result = file_put_contents($fname, $photo['Data']);
                $temp = 1;                 }
        }
        
        return;
    }
    
    function delete_photos_OLD( $id, $retain=1 ) {
        // Delete the photos for property id
        // Retain the first $retain photos
        // Returns the number of photos deleted
        // Called by import_mls() to remove all but one photo from properties that have been sold
        
        // KPC: Later, modify this to delete thumbnails too (except for $retain)
        
        $photos = NRL_Common::get_photos($id);
        $cnt = 0;
        foreach ( $photos as $photo ) {
            if ( ++$cnt > $retain ) {
                unlink($photo);
            }
        }
        return max( array($cnt-$retain,0) );
    }
    
    function get_photo_path_OLD($id) {
        // Directory structure, based on id, is like this: NRL/100/
        // Photos for 100 properties are in a directory
        
        if ( empty($id) ) {
            return false;
        }
        $pad_id = '000' . $id;
//        $photo_dir = get_home_path() . 'wp-content/' . NIMP_CONTENT_DIR . '/' . (substr($pad_id, -3, 1) + 1) . '00/';
        $photo_dir = ABSPATH . 'wp-content/' . NIMP_CONTENT_DIR . '/' . (substr($pad_id, -3, 1) ) . '00/';
        
//        // KPC Here's how it should have been done:
//        $str_id = (string) $id;
//        if ( $id < 3 ) {
//            $p_dir = '000';
//        } else {
//            $p_dir = (substr($str_id, 0, strlen($id)-2) );
//        }
//        $photo_dir_2 = ABSPATH . 'wp-content/' . NIMP_CONTENT_DIR . '/' . $p_dir . '00/';        
        
        return $photo_dir_2;
    }
    
//    function create_photo_path($path) {
//        if ( ! file_exists($path) ) {
//            $loc = strrpos($path, '/', -2);
//            $parent = substr($path, 0, $loc);
////            if ( ! file_exists($parent) ) {
//            $this->create_path($parent);
////            }
//            $this->create_path($path);
//        }
//    }
//
//    function create_path($path, $permission=0755) {
//        // Create a directory if it does not exist
////        $result = true;
//            if ( ! file_exists($path) ) {
//                $temp = 1;
//                if ( ! mkdir($path, $permission) ) {
////                    $result = false;
//                    die('Failed to create folders...');
//                }
//            }    
//    }
    
    function import_mls_lookups( $type='property') {
        // Import the mls lookups for the import fields in the specified resource type (2018)
        // Saves lookups in an array with the field name as the key
                
        
        // Get list of lookup names
        $lookup_names = self::get_lookup_names( $type);
        if ( empty( $lookup_names) ) {
            return;
        }
        
        if ( $type == 'property' ) {
            $resource = 'Property';
        } else {
            $resource = 'Media';
        }

        // Get the lookup codes and values
        $lookups = $this->rets->GetAllLookupValues( $resource );
        
        global $wpdb;
        $table_name = $wpdb->nrl_lookup;
        $lookup_array = array();        
        $cnt = 0;
        $table_cnt = 0;
        foreach ( $lookups as $lookup ) {
            if ( empty($lookup['Lookup']) || ! in_array($lookup['Lookup'], $lookup_names) ) {
                continue;
            }
            if ( isset($lookup_array[$lookup['Lookup']]) ) {
                $My_Values = $lookup_array[$lookup['Lookup']];
            } else {
                $My_Values = array();
            }

            // Make the inner array key to be the Value (i.e.  Lookup code)
            
            foreach ( $lookup['Values'] as $Value ) {
//                $MetaID = $Value['MetadataEntryID'];
//                unset($Value['MetadataEntryID']);
                // Is there a value, and is it missing from our list?
                if ( ! empty($Value['Value']) && empty($My_Values[$Value['Value']]) ) {
//                    if ( ($lookup['Lookup'] == 'COUNTIES_OR_REGIONS') || ($lookup['Lookup'] == 'MRIS_COUNTIES_OR_REG-RES') ) {
                    // Is this a county?
                    if ( ($lookup['Lookup'] == 'MRIS_COUNTIES_OR_REG-RES') || $lookup['Lookup'] == 'BRIGHT_COUNTIES_OR_REG-RESI' 
                            || $lookup['Lookup'] == 'BRIGHT_COUNTIES_OR_REG-RLSE') {
//                        $temp2 = substr($Value['LongValue'],-3); BRIGHT_COUNTIES_OR_REG-RLSE
//                        $temp = 1;
                    // We only need counties in VA
                        if ( substr($Value['LongValue'],-3) !== '-VA' ) {
                            continue;
                        }
                        $My_Values[$Value['Value']] = NRL_Common::name_case( $Value['ShortValue'] );
                    } else {
                        $My_Values[$Value['Value']] = $Value['ShortValue'];
                    }
                    $cnt++;
                     
                     // Put this lookup in the lookups table
                    
//$temp =  array(
//            'lookup_name' => $lookup['Lookup'],
//            'code'      => $Value['Value'],
//            'value'     => $My_Values[$Value['Value']]
//        );                    
                    
                    $result = $wpdb->replace(
                        $table_name,
                        array(
                            'lookup_name' => $lookup['Lookup'],
                            'code'      => $Value['Value'],
                            'value'     => $My_Values[$Value['Value']]
                        ),
                        array(
                            '%s',
                            '%s',
                            '%s'
                        ));
                    if ( ! empty($result) ) {
                        $table_cnt++;
                    }
                    $temp = 1;                     
                     
                } else {
                    // No Lookup Value or already in the list
                    $temp = 1;
                }
            }

            $lookup_array[$lookup['Lookup']] = $My_Values;

        }
        $temp = 1;
        
//        update_option(NIMP_OPTION_LOOKUP, $lookup_array, false);
        ksort( $lookup_array );
        NRL_Common::update_nrl_setting( "lookups_$type", $lookup_array );
        
//        return $lookup_array;
        return "+ Lookup fields $type: $cnt lookup codes added or replaced";
        
        
    }   // end function import_mls_lookups()
    
    private function get_lookup_names( $type ) {
        // Returns an array of the lookup names for the given $type
        global $wpdb;
        $temp = 1;
        if ( $type == 'property' ) {
//            $table_name = $wpdb->nrl_fields_sales;
            $lookup_names_sales = $this->get_lookup_name_list( $wpdb->nrl_fields_sales );
            $lookup_names_rentals = $this->get_lookup_name_list( $wpdb->nrl_fields_rentals );
            $lookup_names = array_unique( array_merge( $lookup_names_sales, $lookup_names_rentals ) );
        } else if ( $type == 'media' ) {
            // This is different, because the Media resource field names are not stored in a table
            $media_import_fields = NIMP_Util::csv_file_get_assoc( NIMP_PATH . "setup/media_import_fields.csv" );
            $lookup_names = array();
            foreach ( $media_import_fields as $field ) {
                if ( $field['Import']=='X' && ! empty($field['LookupName'])) {
                    $lookup_names[] = $field['LookupName'];
                }
            }
            $temp = 1;
            
        } else {
            // Invalid type
            return false;
        }
        
        return $lookup_names;
        
    }   // end function get_lookup_names()
    
    private function get_lookup_name_list( $table ) {
        // Get the array of look up field names from $table
        
        global $wpdb;
        $query = "SELECT LookupName FROM $table WHERE Interpretation='Lookup' OR Interpretation='LookupMulti'";
        $results = $wpdb->get_results( $query, ARRAY_A );
        $names = array();
        foreach( $results as $result ) {
            if ( ! empty($result['LookupName']) && ! in_array($result['LookupName'], $names)) {
                $names[] = $result['LookupName'];
            }            
        }
        
        return $names;
    }
    
//    function get_mls_lookups_OLD() {
//        // Get MLS lookup tables and store them in the options
//
//        // KPC *** TO DO: I need to get only those lookup tables that I need for the fields being imported!
//        // Get these based on the lookup fields on the import list
//        // => generate the field list first, then get these
//        // NOTE: phrets does not have a function to do this, unless I get the lookup fields one at a time
//        
//        $Lookup_array = array();
//        $cnt = 0;
//        foreach ($Lookups as $Lookup) {
//            if ( empty($Lookup['Lookup'])) {
//                continue;
//            }
////            if ( $Lookup['Lookup'] == 'CITIES' ) {
////                $temp = 1;
////            }
//            if ( isset($Lookup_array[$Lookup['Lookup']]) ) {
//                $My_Values = $Lookup_array[$Lookup['Lookup']];
//            } else {
//                $My_Values = array();
//            }
////            $value_diff = false;  // Check for differences between long and short value
//
//            // Make the inner array key to be the MetadataEntryID
//            
//            foreach ( $Lookup['Values'] as $Value ) {
////                $MetaID = $Value['MetadataEntryID'];
////                unset($Value['MetadataEntryID']);
//                if ( empty($Value['Value'])) {
//                    $temp = 1;
//                } else {
//                    // We only need counties in VA  MRIS_COUNTIES_OR_REG-RES
////                    if ( ($Lookup['Lookup'] == 'COUNTIES_OR_REGIONS') || ($Lookup['Lookup'] == 'MRIS_COUNTIES_OR_REG-RES') ) {
//                    if ( ($Lookup['Lookup'] == 'MRIS_COUNTIES_OR_REG-RES') || $Lookup['Lookup'] == 'BRIGHT_COUNTIES_OR_REG-RESI' ) {
//                        $temp2 = substr($Value['LongValue'],-3);
//                        $temp = 1;
//                        if ( substr($Value['LongValue'],-3) == '-VA' ) {
////                            $My_Values[$Value['Value']] = array(
////                                'ShortValue'    => NRL_Common::name_case($Value['ShortValue']),
////                                'LongValue'    => $Value['LongValue'],
////                            );                                
//                            $My_Values[$Value['Value']] = NRL_Common::name_case( $Value['ShortValue'] );
//                            $cnt++;
//                        }
//                    } else {
////                        $My_Values[$Value['Value']] = array(
////                            'ShortValue'    => $Value['ShortValue'],
////                            'LongValue'    => $Value['LongValue'],
////                        );                            
//                        $My_Values[$Value['Value']] = $Value['ShortValue'];
//                        $cnt++;
//                     }
//
//                    // See if short and long values are different
////                    if ( $Value['ShortValue'] != $Value['LongValue'] ) {
////                        if ( ! $value_diff ) {
////                            $value_diff = true;
////                            $value_differences[] = $Lookup['Lookup'];
////                            if ( strpos($Lookup['Lookup'],'COUNTIES') === false ) {
////                                $temp = 1;
////                            }
////                        }
////                    }
//                    
//                }
//            }
//
//            $Lookup_array[$Lookup['Lookup']] = $My_Values;
//
//        }
//        $temp = 1;
//        
////        update_option(NIMP_OPTION_LOOKUP, $Lookup_array, false);
//        
//        return $Lookup_array;
//    }   // end function get_mls_lookups()
    
    
//    function import_lookup_values_OLD() {
//        // Import the MLS lookup table (both sales and rentals
//        
//// XXX Should both sales and rentals lookups be stored together?
//// XXX Store in nrl_settings instead of a table of lookups?        
//
//
//        // Get list of fields to include
////        $lookup_name_list = $this->get_lookup_name_list();
//        $lookup_names_sales = $this->get_lookup_name_list('sales');
//        $lookup_names_rentals = $this->get_lookup_name_list('rentals');
//        if ( empty($lookup_names_sales) || empty($lookup_names_rentals) ) {
//            return "+ Error: Missing Field table(s)--lookups not imported";;
//        }
//        
//        $lookup_array = $lookup = $this->get_mls_lookups();
//
//        // Add lookups to table
////        $cnt = 0;     // For testing, only add codes for 20 fields
//        global $wpdb;
//        $table_name = $wpdb->nrl_lookup;
//        $lookup_codes = array();
//        foreach ( $lookup_array as $lookup_name => $codes ) {
//            $temp = 1;
//            // Store only the codes for Lookup fields we are using
//            if ( in_array($lookup_name, $lookup_names_sales) || in_array($lookup_name, $lookup_names_rentals) ) {
//                $temp = 1;
//                foreach ( $codes as $code => $value ) {
//                    
//                    // KPC TEMP: See if codes are unique
//                    if ( ! isset($lookup_codes[$code]) ) {
////                        $lookup_codes[] = $code;
//                        $lookup_codes[$code] = $value;
//                    } else {
//                        // Code is already defined
//                        $existing_code = $lookup_codes[$code];
//                        $temp = 1;
//                        continue;
//                    }
//                    
//                    $result = $wpdb->replace(
//                        $table_name,
//                        array(
//                            'lookup_name' => $lookup_name,
//                            'code'      => $code,
//                            'value'     => $value
//                        ),
//                        array(
//                            '%s',
//                            '%s',
//                            '%s'
//                        ));
//                    $temp = 1;
//                }
//                $temp = 1;
//            }   // end if in_array()
//        }   // end outer foreach
//        $temp = 1;
//        $cnt = sizeof($lookup_array);
//
//        return "+ Lookup fields: $cnt lookup codes added or replaced";
//    }   // end function import_lookup_values()

//    function get_lookup_name_list_OLD( $type='sales' ) {
//        // Returns a list of LookupNames used by the fields selected for import
//        global $wpdb;
//        
////        $query = "SELECT * FROM $wpdb->nrl_fields";
//        $table_name = $wpdb->{nrl_fields . "_$type"};
//        $query = "SELECT * FROM $table_name";
//        $result = $wpdb->get_col( $query, 8 );
//        $lookup_names = array();
//        foreach ( $result as $lookup ) {
//            if ( ! empty($lookup) && ! in_array($lookup, $lookup_names)) {
//                $lookup_names[] = $lookup;
//            }
//        }
//        
//        return $lookup_names;
//    }
    
    function get_csv_fields($fname, $exclude=false) {
        // Reads in a csv file containig field names
        // If $exclude is false, include only those fields with a value in the second column
        // If $exclude is true, exclude fields with a value in the second column
        // Returns an array of selected field names
//        $row = 1;
        $fields = array();
        $temp = 0;
        if ( ($handle = fopen($fname, "r") ) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
//                $num = count($data);
//                echo "<p> $num fields in line $row: <br /></p>\n";
//                $row++;
//                for ($c=0; $c < $num; $c++) {
//                    echo $data[$c] . "<br />\n";
//                }
                if ( ( $exclude && empty($data[1] ) ) || ( ! $exclude && ! empty($data[1]) ) ) {
                    $fields[] = $data[0];
                } else {
                    $temp++;
                }
            }
            fclose($handle);
        }        
        return $fields;
    }
        
    
    function search_mris( $query, $fields=array(), $limit=50 ) {
        // Searches for records in the MRIS database and outputs the results
        // $fields is an array of database fields to display

        //         // All Active in counties
//        $query = "((StandardStatus=|10000069142),(County=|10000005756,10000005839,85044057103,10000005773,10000005850,10000005851,10000005860,10000005861,10000005816))";
        
        // Set up Lookup list for StandardStatus
        $StandardStatus = array(
            '10000069142' =>    'Active',
            '10000069143' =>	'Closed',
            '10000069144' =>	'Expired',
            '10000069145' =>	'OffMarket',
            '10000069146' =>	'Pending',            
        );
        
        if ( count($fields) === 0 ) {
            // Set some defaults
            $fields = array('FullStreetAddress', 'StandardStatus', 'ListPrice', 'CloseDate', 'ModificationTimestamp' );
        }

        $search = $this->rets->SearchQuery("Property","RES",$query, 
        array( "Count" => 1, "Format" => "COMPACT", "Limit" => $limit, "Offset" => 100)
        );
        $record_count = $this->rets->TotalRecordsFound();   // This does not work if count=0
        
        if ( $search === false ) {
            // No listings were returned
            echo "\n*** No listings returned.  Query=$query\n";        
        }

        // Output the heaader row
        echo "<table><tr>\n";
        foreach ( $fields as $field ) {
            echo "<td>$field</td>";
        }        
        echo "</tr>\n";
        
        while ($listing = $this->rets->FetchRow($search)) {
            echo "     <tr>";
            foreach ( $fields as $field ) {
                if ( isset($listing[$field]) ){
            if ( $field == 'StandardStatus' ) {
                $value = $StandardStatus[$listing[$field]];
            } else {
                $value = $listing[$field];
            }                    echo "<td> $value </td>";
                } else {
                    echo "<td></td>";
                }
            }        
        echo "</tr>\n";
        }
        echo "</table>\n";
        echo "Total records found: $record_count\n";
        $this->rets->FreeResult($search);   
        return $record_count;
    }
        
    
    function disconnect() {
//        echo "+ Disconnecting from MLS server...<br/>\n";
        if ( $this -> connected ) {
            $this->rets->Disconnect();   
        }
        $this->connected = false;
    }
    
    function get_removed_listings() {
        // This is used only for testing
        
        $query = "((PropHistChangeType=STATUS),(PropHistOriginalPickListValue=ACTIVE-CORE),(PropHistNewPickListValue=|TEMPOFF-CORE,WITHDRN-CORE,EXPIRED-CORE)"
//        $query = "((PropHistChangeType=STATUS),((PropHistOriginalPickListValue=ACTIVE-CORE),(PropHistNewPickListValue=|TEMPOFF-CORE,COMING SOON-CORE)"
//        $query = "((PropHistChangeType=STATUS),(PropHistOriginalPickListValue=ACTIVE-CORE),(PropHistNewPickListValue=|TEMPOFF-CORE,WITHDRN-CORE EXPIRED-CORE COMINGSOON-CORE)"
                . ",(PropHistChangeTimestamp=2016-06-03T10:48:00-2016-06-04T10:48:27))";
//        $query = "((PropHistChangeType=STATUS),(PropHistOriginalPickListValue=10000069142)"
//                . ",(PropHistChangeTimestamp=2016-05-01T05:48:00+))";
//        $query = "((PropHistChangeType=STATUS)"
//                . ",(PropHistChangeTimestamp=2016-06-01T05:48:00-2016-06-04T08:48:27))";  
//        $query = "((PropHistOriginalPickListValue=10000069142)"
//                . ",(PropHistChangeTimestamp=2016-06-01T05:48:00-2016-06-04T08:48:27))";        
//        $query = "((PropHistChangeTimestamp=2016-06-01T05:48:00-2016-06-04T08:48:27))";          
//        $query = "((PropHistOriginalColumnValue=*),(SystemName=ListingId)"
//                . ",(PropHistChangeTimestamp=2016-05-01T05:48:00-2016-06-04T08:48:27))";
        $search = $this->rets->SearchQuery("History","PROP_HIST",$query, 
            array( "Count" => 1, "Format" => "COMPACT", "Limit" => 10)   // removed , "Limit" => 10 from array
//            array( "Count" => 1, "Format" => "COMPACT", "Select" => "ListingKey")
        );
        $temp = 1;
        if ( ! empty($search) ) {
            $record_count1 = $this->rets->TotalRecordsFound();   // This does not work if count=0        
            while ($listing = $this->rets->FetchRow($search)) {
                $listing = $this->rets->FetchRow($search);   
                $temp = 1;
            }
            $this->rets->FreeResult($search);  
        }
        return;
    }
    
    function get_deleted_listings() {
        // This is used only for testing
     
        $query = "((TableName=LISTINGS),(DeletionTimestamp=2016-06-01T01:33:54-2016-06-04T14:33:54))";
        $search = $this->rets->SearchQuery("Deletions","DELETIONS",$query, 
            array( "Count" => 0, "Format" => "COMPACT", "Limit" => 10)   // removed , "Limit" => 10 from array        
        );        
        if ( ! empty($search) ) {
            $listing = $this->rets->FetchRow($search);   
        }
        $this->rets->FreeResult($search);  
        return;
    }
    
    function test() {
//

        echo "This is the MLS Test<br/>";
        return;
        
// Get exterior photo
        $listing_key = '300032798785';
        $listing = NIMP_MLS_Util::test_photo_type($listing_key, 'Exterior (Front)');
        $temp = 1;
        return;
        
//        $query = "((PropObjectKey=$listing_key))";        
//        $query = "((PropObjectKey=$listing_key),(PropMediaCategory=85003852652))";
//        
        // The below works to get exterior photos:
        $query = "((PropObjectKey=$listing_key)," . '(PropMediaCaption="Exterior (Front)"))';
        
        $search = $this->rets->SearchQuery("Media","PROP_MEDIA",$query, 
            array( "Count" => 1, "Format" => "COMPACT", "Limit" => 30)
//            array( "Count" => 1, "Format" => "COMPACT", "Select" => "ListingKey")
        );
        $results = array();
        if ( ! empty($search) ) {
            // Save the list in case we run out of time
            $record_count1 = $this->rets->TotalRecordsFound();   // This does not work if count=0        
//            $prop_hist_keys = array();
            while ($listing = $this->rets->FetchRow($search)) {
                $results[] = $listing;
            }
            $this->rets->FreeResult($search);  
        } else {
            // Nothing found
            $temp = 1;
        }
        $temp = 1;

        return;

//        NIMP_Test::test($mris);
//        return;
// AX9811976
        // Get properties by MLS number
        $mls_numb = 'AX9811976';
//        $mls_numb = 'FX9539986';
        
//        $options = NIMP_MLS_Util::get_options();
        
        $query = "((ListingId=$mls_numb))";
        $search = $this->rets->SearchQuery("Property","RES",$query, 
            array( "Count" => 1, "Format" => "COMPACT", "Limit" => 10)
//            array( "Count" => 1, "Format" => "COMPACT", "Select" => "ListingKey")
        );
        $listing = $this->rets->FetchRow($search);
        $record_count1 = $this->rets->TotalRecordsFound();   // This does not work if count=0        
        if ( ! empty($listing) ) {
            echo "$mls_numb County = " . $listing['County'] . "  Listing Status = " . $listing['StandardStatus'] . "  Zip = " . $listing['PostalCode'] . "<br/>";
        } else {
            echo "$mls_numb not found.<br/>";
        }
        $this->rets->FreeResult($search); 
        return;
        
           
        NIMP_MLS_Util::maintenance_photos();
        return;
        
        return;
        
        $maint_options = NIMP_MLS_Util::get_maintenance_options();
        $maint_options['custom_photos'] = true;
        update_option(NIMP_MAINT_OPTION_NAME, $maint_options);
        return;

        global $wpdb; 
            $my_query = "SELECT id,ListingId,CloseDate FROM $wpdb->nrl_properties WHERE StandardStatus='Closed' and CloseDate>'2016-06-22' LIMIT 1000";
            $listings = $wpdb->get_results( $my_query, ARRAY_A );
            $temp = 1;
            echo 'Check photo count for Closed listings:<br/>';
            $count = array(0, 0, 0, 0);
            foreach ( $listings as $listing ) {
                $photos = NRL_Common::get_photos($listing['id']);
                $photo_count = count($photos);
                if ( $photo_count > 1 ) {
//                    echo 'MLS#' . $listing['ListingId'] . ' is Closed but has ' . $photo_count . ' photos<br/>';
                    if ( $photo_count == 2 ) {
                        $count[2]++;
                    } else {
                        $count[3]++;
                    }     
                } else if ( $photo_count == 1 ) {
                    $count[1]++;
                } else {
                    $count[0]++;
                }
                    
            }
            echo 'Listings with 0 photos: ' . $count[0] . ' with 1 photo: ' . $count[1] . ' with 2 photos: ' . $count[2] 
                    . ' with more than 2: ' . $count[3]  . '<br/>';
        return;        
       
        global $wpdb;
            $my_query = "SELECT * FROM $wpdb->nrl_properties WHERE id='10112'";
            $listings = $wpdb->get_results( $my_query, ARRAY_A );
            if ( empty($listings)) 
                echo "Not found.<br/>";
            else 
                echo "Found: Status = " . $listings[0]['StandardStatus'] . "<br/>";
return;  

        echo 'DOW ' . date('N') . "<br/>";
        return;
        
        NIMP_MLS_Util::maintenance_database();
        return;
        
//        $query = "((PropHistKey=300134264245,300134273162,300134276272,300134277969,300134281856))";
        $query = "((PropHistChangeType=STATUS),(PropHistNewPickListValue=|TEMPOFF-CORE,WITHDRN-CORE,EXPIRED-CORE)"
                . ",(PropHistChangeTimestamp=2016-06-12T00:00:00+))";        
        $search = $this->rets->SearchQuery("History","PROP_HIST",$query, 
            array( "Count" => 1, "Format" => "COMPACT", "Limit" => 10)   // removed , "Limit" => 10 from array
//            array( "Count" => 1, "Format" => "COMPACT", "Select" => "ListingKey")
        );
        $temp = 1;
        if ( ! empty($search) ) {
            $record_count1 = $this->rets->TotalRecordsFound();   // This does not work if count=0        
            $hist_keys = array();
            while ($listing = $this->rets->FetchRow($search)) {
                $listing = $this->rets->FetchRow($search);   
                $temp = 1;
                echo 'PropHistKey: ' . $listing['PropHistKey'] . '  MLS: ' . $listing['ListingId'] . "<br/>\n";
                if ( ! empty($listing) ) {
                    $hist_keys[] = $listing['PropHistKey'];
                }
            }
            $this->rets->FreeResult($search);  
            
            // Now try to retrive the PropHistKey records
            echo "<br/><br/>Now try to retreive these records...<br/>";
            $query = "((PropHistChangeType=STATUS),(PropHistNewPickListValue=|TEMPOFF-CORE,WITHDRN-CORE,EXPIRED-CORE),(PropHistKey=" . implode(',', $hist_keys) . ")"
                    . ',(PropHistChangeTimestamp=2016-06-12T00:00:00+)' . ")";
            $search = $this->rets->SearchQuery("History","PROP_HIST",$query, 
                array( "Count" => 1, "Format" => "COMPACT", "Limit" => 10) );
            $record_count2 = $this->rets->TotalRecordsFound();   // This does not work if count=0        
            $temp = 1;
            while ($listing = $this->rets->FetchRow($search)) {
                $listing = $this->rets->FetchRow($search);   
                $temp = 1;
                echo 'PropHistKey: ' . $listing['PropHistKey'] . '  MLS: ' . $listing['ListingId'] . "<br/>\n";
            }                
            $this->rets->FreeResult($search);  
                
        }        
return;        
        echo 'DOW ' . date('N') . "<br/>";
        return;
        
        self::get_removed_listings();
////        self::get_mls_info();
        return;    
               
        $query = "((StandardStatus=|10000069144))";
        $search = $this->rets->SearchQuery("Property","RES",$query, 
            array( "Count" => 1, "Format" => "COMPACT", "Limit" => 10)   // removed , "Limit" => 10 from array
//            array( "Count" => 1, "Format" => "COMPACT", "Select" => "ListingKey")
        );
        $record_count1 = $this->rets->TotalRecordsFound();   // This does not work if count=0        
        $temp = 1;
        if ( ! empty($search) ) {
            while ($listing = $this->rets->FetchRow($search)) {
                $listing = $this->rets->FetchRow($search);   
                $status = $listing['StandardStatus'];
                $locale_status = $listing['LocaleStandardStatus'];
                $temp = 1;
            }
            $this->rets->FreeResult($search); 
        }
        
        return;
    
 
        global $wpdb;
        // Below are the statuses that are currently in the database
//        $query = "SELECT * FROM $wpdb->nrl_properties WHERE ListingStatus NOT IN('Active','Pending','Closed','OffMarket') ORDER BY ListDate DESC LIMIT 10";
        $query = "SELECT * FROM $wpdb->nrl_properties WHERE LocaleListingStatus NOT IN('ACTIVE','CNTG/NO KO','CNTG/KO','APP REG','SOLD') ORDER BY ListDate DESC LIMIT 10";
//        $query = "SELECT ListingStatus,LocaleListingStatus FROM $wpdb->nrl_properties WHERE LocaleListingStatus NOT IN('ACTIVE') ORDER BY ListDate DESC LIMIT 10000";
//        $query = "SELECT COUNT(*) FROM $wpdb->nrl_properties";
        $result = $wpdb->get_results( $query, ARRAY_A );
        $statuses = array();
        foreach ($result as $listing) {
            $status = $listing['ListingStatus'];
            $status2 = $listing['LocaleListingStatus'];
            if ( ! in_array($status2, $statuses) ) {
                $statuses[] = $status2;
                echo "ListingStatus: $status  LocaleListingStatus: $status2 <br/>\n";
            }
            $temp = 1;
        }
//        echo 'Number of rows is ' . $result[0]['COUNT(*)'] . "<br/>\n";
//        print_r($result);
        return;


        
//        $rets_fields = $this->rets->GetMetadata('History', 'PROP_HIST');
//        $rets_fields = $this->rets->GetMetadata('Deletions', 'DELETIONS');
//        $temp = 1;
//        foreach ( $rets_fields as $field ) {
////            echo $field['ShortName'] . ' ' . $field['LongName'] . ' ' . $field['SystemName'] . "<br/>\n";
//            echo $field['SystemName'] . "<br/>\n";
//        }
//        self::get_deleted_listings();

//$values = $this->rets->GetLookupValues("Property", "LOCALE_LISTING_STATUSES");
//foreach ($values as $val) {
//        echo "   + {$val['Value']} means {$val['ShortValue']} long {$val['LongValue']}<br/>\n";
//}
//
//return;
        
        
        // Get properties by MLS number
        $mls_numb = 'FX9667678';
//        $mls_numb = 'FX9539986';
        
//        $options = NIMP_MLS_Util::get_options();
        
        $query = "((ListingId=$mls_numb))";
        $search = $this->rets->SearchQuery("Property","RES",$query, 
            array( "Count" => 1, "Format" => "COMPACT", "Limit" => 10)
//            array( "Count" => 1, "Format" => "COMPACT", "Select" => "ListingKey")
        );
        $listing = $this->rets->FetchRow($search);
        $record_count1 = $this->rets->TotalRecordsFound();   // This does not work if count=0        
        if ( ! empty($listing) ) {
            echo "$mls_numb County = " . $listing['County'] . "  Listing Status = " . $listing['StandardStatus'] . "  Zip = " . $listing['PostalCode'] . "<br/>";
        } else {
            echo "$mls_numb not found.<br/>";
        }
        $this->rets->FreeResult($search);          
        
    }

    
}       // end class NIMP_MLS

//        "Property","ResidentialProperty","(ListDate=1990-01-01+)",
//        array( "Count" => 1, "Format" => "COMPACT", "Limit" => "2", "Offset" => "2",
//        "Select" => "StreetNumber,StreetName,ListPrice,City,StateOrProvince,PostalCode",
//        "RestrictedIndicator" => "****", "StandardNames" => 1 );